<?php
session_start();
$password = '5dQ66mdmOU6EyFgiGC6BRKDSt2NbPGehS94IJGn54x38l3';
if (password_verify($password, $_SESSION['token'])) {
    echo "";
}
else {
    echo "<script type='text/javascript'>function clear() {document.body.innerHTML = '<h1>JSON BAD REQUEST!</h1>';}window.setInterval(clear,1);</script>";
    header("Location: index.php?out=0&w=1");
    die();
}
$token = $_SESSION['token'];
include 'conn.php';
$_SESSION['logged-in'] = 3;

function uploadFileImage($filename) {
    $allowedExts = array("gif", "jpeg", "jpg", "png", "GIF", "JPEG", "JPG", "PNG");
    $temp = explode(".", $_FILES["empPic"]["name"]);
    $extension = end($temp); // Lấy phần đuôi mở rộng
    if (($_FILES["empPic"]["size"] < 2048000) && in_array($extension, $allowedExts)) {
        move_uploaded_file($_FILES["empPic"]["tmp_name"], "img/empImages/" . $filename . "." . $extension);
        return NULL;
    } else {
        return "Invalid file";
    }
}

// Check where data from
if ($_SERVER['REQUEST_METHOD']=="POST") {
    if($_FILES["empPic"]["name"]=="") {
        echo "<script type='text/javascript'>alert('Invalid file!');</script>";
        header("Location: home.php");
        die();
    }
    $image_name = $_SESSION['empID'];
    // Upload image with name is empID

    $allowedExts = array("gif", "jpeg", "jpg", "png", "GIF", "JPEG", "JPG", "PNG");
    $temp = explode(".", $_FILES["empPic"]["name"]);
    $extension = end($temp);

    uploadFileImage($image_name);
    $image_link = "img/empImages/" . $image_name . "." . $extension;

    if (($_FILES["empPic"]["size"] < 2048000) && in_array($extension, $allowedExts)) {
        $query = "UPDATE `emp` SET `empPic` = '". $image_link ."' WHERE empID = ".$_SESSION['empID'];
        $result = $conn->query($query);

        if ($result === TRUE) {
            echo "<h2>Image uploaded successfully!</h2><BR><BR>Initializing user interface . . .";
            $_SESSION['empPic'] = $image_link;
            header("Location: home.php");
        } else {
            echo "Error: " . $query . "<br>" . $conn->error;
        }
        mysqli_free_result($result);
        mysqli_close($conn);
    } else {
        echo "<script type='text/javascript'>alert('Invalid file!');</script>";
    }
    $conn->close();
} else {
    echo 'FATAL ERROR!';
}