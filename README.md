﻿# THÔNG TIN PROJECT ECONNECT # 

Bộ ứng dụng và tiện ích cho các nhân viên và quản lí thuộc một công ty/tổ chức.
Giúp việc giao tiếp giữa các nhân viên, giữa quản lí và nhân viên dễ dàng hơn. Bên cạnh đó là những công cụ hữu ích: lịch, danh bạ, check-list... Hỗ trợ công việc và tiết kiệm thời gian.

Project 1 - Group 2 - C1410L - Aprotrain Aptech (aahn02)

## TỔNG QUAN ##

### Tính năng chính: ###
**Người dùng:**
Trao đổi
- Lịch công việc
- Danh bạ
- Yêu cầu hỗ trợ
- Check-list
- Sửa thông tin cá nhân

**Quản lí:** (có thêm các tính năng) 
- Đăng thông báo
- Phân công công việc
- Xem thống kê
- Quản lí hệ thống
- Quản lí nhân sự
- Phân quyền

### Phiên bản ###
* WebApp Version: **1.0.0.5** 
* Database version: **1.0.4.4**

### Demo ###
* Website: https://www.econnect.cf
* Demo: (Comming soon) - Dự kiến: **25/04/2015**


## Liên hệ ##
* Leader: Giang Nguyen - Mob:     01272555555
* Project email: econnect@C1410L.tk || admin@econnect.cf