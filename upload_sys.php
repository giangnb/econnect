<?php
session_start();
$password = '5dQ66mdmOU6EyFgiGC6BRKDSt2NbPGehS94IJGn54x38l3';
if (password_verify($password, $_SESSION['token'])) {
    echo "";
}
else {
    echo "<script type='text/javascript'>function clear() {document.body.innerHTML = '<h1>JSON BAD REQUEST!</h1>';}window.setInterval(clear,1);</script>";
    header("Location: admin/index.php?out=0&w=1");
    die();
}
$token = $_SESSION['token'];
include 'conn.php';
$_SESSION['logged-in'] = 3;

function uploadFileImage() {
    $allowedExts = array("gif", "jpeg", "jpg", "png", "GIF", "JPEG", "JPG", "PNG");
    $temp = explode(".", $_FILES["logo"]["name"]);
    $extension = end($temp); // Get extension
    if (($_FILES["logo"]["size"] < 2048000) && in_array($extension, $allowedExts)) {
        move_uploaded_file($_FILES["logo"]["tmp_name"], "img/logo." . $extension);
        return NULL;
    } else {
        return "Invalid file";
    }
}

// Check where data from
if ($_SERVER['REQUEST_METHOD']=="POST") {
    if ($_POST['type'] == "logo") {
        if($_FILES["logo"]["name"]=="") {
            echo "<script type='text/javascript'>alert('Invalid file!');</script>";
            header("Location: admin/admin.php");
            die();
        }
        // Upload image with name is empID

        $allowedExts = array("gif", "jpeg", "jpg", "png", "GIF", "JPEG", "JPG", "PNG");
        $temp = explode(".", $_FILES["logo"]["name"]);
        $extension = end($temp);

        uploadFileImage();
        $image_link = "img/logo." . $extension;

        if (($_FILES["logo"]["size"] < 2048000) && in_array($extension, $allowedExts)) {
            $query = "UPDATE setting SET `value` = '". $image_link ."' WHERE prop = 'logo'";
            $result = $conn->query($query);

            if ($result === TRUE) {
                echo "<h2>Image uploaded successfully!</h2><BR><BR>Initializing user interface . . .";
                header("Location: admin/admin.php");
            } else {
                echo "Error: " . $query . "<br>" . $conn->error;
            }
            mysqli_free_result($result);
            mysqli_close($conn);
        } else {
            echo "<script type='text/javascript'>alert('Invalid file!');</script>";
        }
    }
    $conn->close();
} else {
    echo 'FATAL ERROR!';
}