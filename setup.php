<?php
// SetupStat:
// 1: New user
// 2: Reset password
// 3: Change password

session_start();
$token = $_SESSION['token'];
include 'conn.php';
include 'encrypt/encrypter.php';
if(!isset($_SESSION['auth'])){
    header("Location: index.php?exp=1");
    die();
}

if (isset($_SESSION['logged-in'])) {
    if ($_SESSION['logged-in'] == 1) {
        $q = "UPDATE `emp` SET `logoutTime` = NOW(), `isLogin` = '0' WHERE `empEmail` = '" . encrypt($_SESSION['usr']) . "';";
        $result = $conn->query($q);
        session_unset();
        session_destroy();
        header("Location: index.php?out=1");
        die();
    }
}
$_SESSION['logged-in'] = 2;

if (!isset($_SESSION['type'])) {
    session_unset();
    session_destroy();
    header("Location: index.php?exp=1");
    die();
}

$error = "";
if ($_SERVER['REQUEST_METHOD'] == "POST") {
    switch ($_SESSION['type']) {
        case 1:
            if ($_POST['empPswd_new'] != $_POST['empPswd_re']) {
                $error = $error."Password mismatch!<BR>";
            }
            if ($_POST['empPasscode'] != $_POST['empPasscode_re']) {
                $error = $error."Passcode mismatch!<BR>";
            }
            if (strlen($_POST['empPswd_new']) < 8) {
                $error = $error."Password too short!<BR>";
            }
            if (strlen($_POST['empPasscode']) > 8 || strlen($_POST['empPasscode']) < 4) {
                $error = $error."Passcode must be 4-8 digit!<BR>";
            }
            if (!filter_var($_POST['empPasscode'], FILTER_VALIDATE_INT)) {
                $error = $error."Passcode must be number!<BR>";
            }
            if (isset($_POST['empIDcard']))
                if (empty($_POST['empIDcard']))
                    $_POST['empIDcard'] = " ";
            if (isset($_POST['empPhone']))
                if (empty($_POST['empPhone']))
                    $_POST['empPhone'] = " ";
            if (isset($_POST['empMob']))
                if (empty($_POST['empMob']))
                    $_POST['empMob'] = " ";
            if (isset($_POST['empSkype']))
                if (empty($_POST['empSkype']))
                    $_POST['empSkype'] = " ";
            $sql = "UPDATE `emp` SET ";
            if ($error == "") {
                $sql = "UPDATE `emp` SET `empPswd` = '".password($_SESSION['usr'],$_POST['empPswd_new']).
                    "', `empPasscode` = '".encrypt($_POST['empPasscode']).
                    "', `setupStat` = 0 WHERE `empID` = ".$_SESSION['empID'];
                $result = $conn->query($sql);
                unset($_SESSION['logged-in']);
                header("Location: home.php");
                die();
            }
            break;
        case 2:
            if ($_POST['empPswd_new'] != $_POST['empPswd_re']) {
                $error = $error."Password mismatch!<BR>";
            }
            if (strlen($_POST['empPswd_new'])<8) {
                $error = $error."Password too short!<BR>";
            }
            if ($error == "") {
                $sql = "UPDATE `emp` SET `empPswd` = '".password($_SESSION['usr'],$_POST['empPswd_new'])."', `setupStat` = 0 WHERE empID = '".$_SESSION['empID']."'";
                $res = $conn->query($sql);
                unset($_SESSION['logged-in']);
                header("Location: index.php?setup=1");
                die();
            }
            break;
        case 3:
            $q = "SELECT `empPswd` FROM emp WHERE empID = ".$_SESSION['empID'];
            $r = $conn->query($q);
            if ($r->num_rows > 0) {
                $row = $r->fetch_assoc();
                $hash = $row['empPswd'];
            } else {
                echo "<h1>FATAL ERROR!</h1>Please try again.";
                session_unset(); session_destroy();
                die();
            }
            if (password_check($_SESSION['usr'],$_POST['empPswd_old'],$hash)==0) {
                $error = $error."Wrong password!<BR>";
            }
            if ($_POST['empPswd_new'] != $_POST['empPswd_re']) {
                $error = $error."Password mismatch!<BR>";
            }
            if ($_POST['empPswd_new'] == $_POST['empPswd_old']) {
                $error = $error."New password must be different from old one!<BR>";
            }
            if (strlen($_POST['empPswd_new'])<8) {
                $error = $error."Password too short!<BR>";
            }
            if ($error == "") {
                $sql = "UPDATE `emp` SET `empPswd` = '".password($_SESSION['usr'],$_POST['empPswd_new'])."', `setupStat` = 0 WHERE empID = '".$_SESSION['empID']."'";
                $res = $conn->query($sql);
                unset($_SESSION['logged-in']);
                header("Location: index.php?setup=1");
                die();
            }
            break;
    }
}

if ($error != ""){
    $error = "<p>".$error."<BR>Please fix the problem above and try again."."</p>";
}

$q = "SELECT empGen, empDOB, empIDcard, empPhone, empMob, empSkype FROM `emp` WHERE empID = '{$_SESSION['empID']}'";
$result = $conn->query($q);
$row = $result->fetch_assoc();
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>ECONNECT - Account Setup</title>
    <link rel="shortcut icon" href="img/icon.png">
    <link rel="stylesheet" href="css/setup.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body>
<h2><span class="glyph">&#xe065;</span> ECONNECT <BR> Account Setup</h2><BR>
<h1><?php echo decrypt($_SESSION['empName']); ?></h1>
<form action="setup.php" method="post" name="setup" id="setup">
    <table class='table table-hover'>
    <?php
    switch ($_SESSION['type']) {
        case 1:
            echo "<tr><td colspan='2'><p>Information setup<BR><span style='font-weight: normal'> New Password and Passcode is REQUIRED<BR>You can edit other information later.</span></p></td></tr>";
            echo "<tr>
                    <td>Password</td>
                    <td>
                        <input type='password' class='form-control' name='empPswd_new' id='empPswd_new' placeholder='New password' required='true'><BR>
                        <input type='password' class='form-control' name='empPswd_re' id='empPswd_re' placeholder='Re-type password' required='true'><BR>
                    </td>
                </tr>
                <tr>
                    <td>Passcode</td>
                    <td>
                        <input type='password' class='form-control' name='empPasscode' id='empPasscode' maxlength='8' placeholder='4 to 8 DIGIT characters' required='true'><BR>
                        <input type='password' class='form-control' name='empPasscode_re' id='empPasscode_re' maxlength='8' placeholder='Re-type Passcode' required='true'>
                    </td>
                </tr>
                <tr>
                    <td colspan='2'>
                        {$error}
                    </td>
                </tr>";
            break;
        case 2:
            echo "<tr><td colspan='2'><p>Password reset - REQUIRED<BR><u></u></p></td></tr>";
            echo "<tr>
                    <td>Password</td>
                    <td>
                        <input type='password' class='form-control' name='empPswd_new' id='empPswd_new' placeholder='New password' required='true'><BR>
                        <input type='password' class='form-control' name='empPswd_re' id='empPswd_re' placeholder='Re-type password' required='true'><BR>
                        {$error}
                    </td>
                </tr>";
            break;
        case 3:
            echo "<tr><td colspan='2'><p>Periodical password change - REQUIRED<BR><u></u></p></td></tr>";
            echo "<tr>
                    <td>Password</td>
                    <td>
                        <input type='password' class='form-control' name='empPswd_old' id='empPswd_old' placeholder='Old password' required='true'><BR>
                        <input type='password' class='form-control' name='empPswd_new' id='empPswd_new' placeholder='New password' required='true'><BR>
                        <input type='password' class='form-control' name='empPswd_re' id='empPswd_re' placeholder='Re-type password' required='true'><BR>
                        {$error}
                    </td>
                </tr>";
            break;
            break;
        default:
            header("Location: home.php");
            break;
    }
    ?>
        <tr>
            <td>&nbsp;</td>
            <td>
                <input type='submit' name='submit' class="btn btn-primary" value=' Save changes '>
            </td>
        </tr>
    </table>
</form>


</body>
</html>
<?php
mysqli_free_result($result);
if (isset($res)) mysqli_free_result($res);
mysqli_close($conn);