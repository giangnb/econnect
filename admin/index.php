<!doctype html> 
<html lang="en" class="no-js"> 
<head> 
  <meta charset="UTF-8" /> 
  <meta name="description" content="A web app for employees in one organization can connect with others easily.">
  <meta name="keywords" content="project, aprotrain, aptech, econnect, connect, webapp, web, app">
  <title>Admin Login - eCONNECT</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="css/login.css" />
  <link rel="shortcut icon" href="img/icon.png">
  <script type='text/javascript' src='script/jquery-2.1.3.min.js'></script>
  <script type="text/javascript" src="script/peopleList.js"></script>
  <script type='text/javascript' src='script/login/jquery.particleground.min.js'></script>
  <script type='text/javascript' src='script/login/demo.js'></script>
  <meta name="norton-safeweb-site-verification" content="08hhfhsna1qjjgqqo7ubg01xwohhyjhuf2jta8-ilq1v9sc3hnagl2g5qg39wljfxbujlsh9mdbuzm-rrfxy-ws7k1-epkv7331z7b8edqflpvm3yjrf1dwvhs9d72yh" />
</head>

<?php flush(); ?>

<body>

<div id="particles">
  <div class="intro">
  <a href="javascript:alert('Project 1\nC1410L - Aprotrain Aptech\n\nBy: Giang + Đức + Quân');"><b>Project 1</b></a> - eConnect@C1410L.tk
    <h1><span style="font-size:28px">eConnect<BR></span>administrator</h1>

      <?php
      error_reporting(0);
      session_start();

      if (isset($_GET['w'])) {
          if ($_GET['w'] == 1) {
              echo "<p>You've entered wrong User name or password!</p>";
              session_unset(); session_destroy();
          }
      }
      elseif (isset($_GET['out'])) {
          if ($_GET['out'] == 1) {
              echo "<p>Logged out!</p>";
              session_unset(); session_destroy();
          }
      }
      elseif (isset($_GET['exp'])) {
          if ($_GET['exp']==1) {
              echo "<p>Your session has expired. Please login again!</p>";
              session_unset(); session_destroy();
          }
      }
      elseif (isset($_GET['inactive'])) {
          if ($_GET['inactive'] == 1) {
              echo "<p>Your account has been deactivated!<BR>Contact your IT manager for more information</p>";
              session_unset(); session_destroy();
          }
      }
      elseif (isset($_GET['setup'])) {
          if ($_GET['setup'] == 1) {
              echo "<p>Your setup has completed, please login again</p>";
              session_unset(); session_destroy();
          }
      }
      elseif (isset($_GET['resetPswd'])) {
          if ($_GET['resetPswd'] == 1) {
              echo "<p style='color: #ac2925; font-weight: bolder; background: cornsilk; padding: 10px; border-radius: 5px;'>You have entered wrong password too many time!<BR>We've reset your password and sent it to your email address.<BR>
                    <BR>Please follow these <a href=\"javascript:alert('Emergency password reset instructions:\\n1. Receive notification email with temporary password\\n2. Login with temporary password\\n3. Set your new password when required\\n\\nContact IT manager or System Administrator for further information.')\" style='background: #ac2925; color: cornsilk'> Instructions </a>.</p>
                    <a href='index.php' target='_top'><span class='btn' style='background: #1A2E43; color: #fff'>Login page</span></a>  ";
          }
      }else {
          echo "<p>Please login to connect with others</p>";
      }

      unset($_POST['pwsd']);
      unset($_POST['empID']);

      function hideInput() {
          if (isset($_GET['resetPswd'])) {
              if ($_GET['resetPswd'] == 1) {
                  echo "style = 'display: none;'";
              }
          }
      }
      ?>
      <form name="login" id="login" action="auth.php" method="post" autocomplete="off">

          <input type="text" placeholder="Employee ID / User name" name="empID" id="empID" required="true" autofocus autocomplete="off" <?php hideInput() ?>/><BR>
          <input type="password" placeholder="Password" name="pswd" id="pswd" required="true" autocomplete="off" <?php hideInput() ?>/>
          <p id="alert"></p>
          <input type="submit" class="btn" value="LOGIN" name="loginBtn" id="loginBtn" onClick="" <?php hideInput() ?>/><BR><BR>
          <a href="../index.php" target="_top">Back to user's area</a>
      </form>
  </div>
</div>

</body> 
</html> 