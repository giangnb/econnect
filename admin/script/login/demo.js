/**
 * Particleground demo
 * @author Jonathan Nicol - @mrjnicol
 */

$(document).ready(function() {
  $('#particles').particleground({
    dotColor: '#c0c0c0',
	lineColor: '#5cbdaa'
  });
  $('.intro').css({
    'margin-top': -($('.intro').height() / 2)
  });
});

var badges = function() {
	$("#ranking").hover( function() {
		$("#ranking").animate({
			right: '-80px',
			top: '30%'
		}, 250);
		$("#ranking img").animate({width:'100%'},300);
	}, function() {
		$("#ranking").animate({
			right: '-205px',
			top: '35%'
		}, 500);
		$("#ranking img").animate({width:'80%'},650);
	});
	
	$("#html5Badge").hover(function() {
		$(this).animate({
			right:'-30px'
		}, 150);
	}, function(){
		$(this).animate({
			right:'-165px'
		}, 450);
	});
	
	$("#valueBadge").hover(function() {
		$(this).animate({
			left:'-10px'
		}, 150);
	}, function(){
		$(this).animate({
			left:'-125px'
		}, 450);
	});
	
	$("#safeBadge").hover(function() {
		$(this).animate({
			left:'-5px'
		}, 150);
	}, function(){
		$(this).animate({
			left:'-180px'
		}, 450);
	});
}
$(document).ready(badges);