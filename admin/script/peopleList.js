// JavaScript Document
peopleListGen();
var resize =  function() {
    $("#people-name-list").css({"height":screen.height * 0.65});
}
$(document).ready(resize);

// People
function peopleListGen() {
    //document.getElementById("people-name-list").innerHTML = "";
    document.getElementById("peopleListBtn").innerHTML = "";
    var abc = new Array("a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z");
    /*for (var count=0; count<26; count++){
        document.getElementById("people-name-list").innerHTML += "<h2><span>"+abc[count]+"</span></h2><ul id=\"people-name-list-"+abc[count]+"\"></ul>";
    }*/
    for (var count=0; count<26; count++){
        document.getElementById("peopleListBtn").innerHTML += "<a href=\"#people-name-list-"+abc[count]+"\" style=\"float:left;\" class=\"people-list-alphabet\"><h2>"+abc[count]+"</h2></a>";
    }
}
var peopleList = function() {
    $("#people-name-list h2, #show-alphabet-popup").click(function() {
        $("#people-popup, #popup-bg").show(200);
        $("#people-popup h2").hide();
        window.setTimeout(function() { $("#people-popup h2").show().addClass("animated flipInX");}, 350);
    });
    $(".people-list-alphabet").click(function() {
        $("#people-popup, #popup-bg").hide(200);
        $("#people-popup h2").removeClass("animated flipInX");
    });
    $("#linkToSearch").click(function() {
        $("#peopleSearch").attr(autofocus);
    });

    $("#people-popup .close-popup").click(function() {
        $("#people-popup,#popup-bg").hide(200);
    });

    $("#people-avatar:not(img)").css("height",screen.height *0.21);
    //peopleListGenerator();
}
$(document).ready(peopleList);