// JavaScript Document

var global = function() {
    $("#ajaxShow").css({"height":screen.height * 0.8, "width":screen.width * 0.725});
    $(".popup").css({"height":screen.height * 0.5, "width":screen.width * 0.5}).hide();
    $("#popup-bg").hide();
	// Elements sizing
	$("#side-1,#side-2,#main").css("height","100%");
	/*if($(window).width() >= 1024) {*/
		$("#side-1").css("width","6%");
		$("#side-2").css("width","20%");
		$("#main").css("width","73%");
	/*} else {
		$("#side-1").css("width","10%");
		$("#side-2").css("width","25%").hide();
		$("#main").css("width","88%");
		$("#side-1 li, #side-2").hover(function() {
			$("#side-2").show(300);
			$("#main").css("width","68%");
		}, function() {
			$("#side-2").hide(300);
			$("#main").css("width","73%");
		});
	}*/
	$("#main .content").css("max-height", $(window).height()*0.95);
	
	// Content showing
	$("#side-1 li").click(function() {
        $("#menu li").removeClass("current");
    });
	
	$("#nav-dashboard").click(function() {
		document.getElementById("ajaxShow").innerHTML = "<h1>Loading ...</h1>";
		//document.getElementById("menu").innerHTML = "Loading ...";
		$("#nav-dashboard").addClass("current");
		$("#nav-general").removeClass("current");
		$("#nav-personnel").removeClass("current");
		$("#nav-system").removeClass("current");
		ajaxLoader(1);
		$("#menu ul").slideUp(100);
		$("#menu-1").slideDown(500);
		$("#menu-1 li:first-child").addClass("current");
	});
	
	$("#nav-general").click(function() {
		document.getElementById("ajaxShow").innerHTML = "<h1>Loading ...</h1>";
		//document.getElementById("menu").innerHTML = "Loading ...";
		$("#nav-dashboard").removeClass("current");
		$("#nav-general").addClass("current");
		$("#nav-personnel").removeClass("current");
		$("#nav-system").removeClass("current");
		ajaxLoader(2);
		$("#menu ul").slideUp(100);
		$("#menu-2").slideDown(500);
		$("#menu-2 li:first-child").addClass("current");
	});
	
	$("#nav-personnel").click(function() {
		document.getElementById("ajaxShow").innerHTML = "<h1>Loading ...</h1>";
		//document.getElementById("menu").innerHTML = "Loading ...";
		$("#nav-dashboard").removeClass("current");
		$("#nav-general").removeClass("current");
		$("#nav-personnel").addClass("current");
		$("#nav-system").removeClass("current");
		ajaxLoader(3);
		$("#menu ul").slideUp(100);
		$("#menu-3").slideDown(500);
		$("#menu-3 li:first-child").addClass("current");
	});
	
	$("#nav-system").click(function() {
		document.getElementById("ajaxShow").innerHTML = "<h1>Loading ...</h1>";
		//document.getElementById("menu").innerHTML = "Loading ...";
		$("#nav-dashboard").removeClass("current");
		$("#nav-general").removeClass("current");
		$("#nav-personnel").removeClass("current");
		$("#nav-system").addClass("current");
		ajaxLoader(4);
		$("#menu ul").slideUp(100);
		$("#menu-4").slideDown(500);
		$("#menu-4 li:first-child").addClass("current");
	});
	
	
}
$(document).ready(global);
