// JavaScript Document

// DASHBOARD -
function cleanSys(m) {
    var c = confirm("Are you sure to clean all data older than "+m+" month(s)?\n\nThis CANNOT be undone!");
    if (c) {
        var loadFile = "sections/1.2.php?encrypt="+encrypt+"&system=clean";
        $("#ajaxShow").load(loadFile);
    }
}
function shutdown(stat) {
    var c;
    var loadFile;
    if (stat=="WORKING") {
        c = confirm("Are you sure to SUSPEND the system?\n\nNo one can login after shutting down.");
        if (c) {
            loadFile = "sections/1.2.php?encrypt="+encrypt+"&system=shut";
            $("#ajaxShow").load(loadFile);
        }
    } else {
        c = confirm("Are you sure to START the system?\n\nEveryone can login normally after this.\nDon't do this if the debugging process doesn't completed or you're still under attack.");
        if (c) {
            loadFile = "sections/1.2.php?encrypt="+encrypt+"&system=start";
            $("#ajaxShow").load(loadFile);
        }
    }
}
// DASHBOARD end

// GENERAL -
function loadGeneralList (i) {
    var loadList = "sections/2.1.1.php?p="+i+"&encrypt="+encrypt;
    $("#m_List").html('Please wait . . .').load(loadList);
    loadGeneralForm (i);
}
function loadGeneralForm (p) {
    var loadForm = "sections/2.1.2.php?p="+p+"&encrypt="+encrypt;
    $("#general_form").html('Please wait . . .').load(loadForm);
}
function loadGeneralFormDesc (p,id) {
    var loadForm = "sections/2.1.2.php?p="+p+"&encrypt="+encrypt;
    $.post(loadForm, {
        id: id
    }, function(data) {
        $("#general_form").html(data);
    });
}
function generalEdit (id,p) {
    var name = $("#generalName").val();
    var addr = $("#branchAdd").val();
    var error="";
    if (name.length < 2) error+=" - Name is too short\n";
    if (addr.length < 5 && addr!=" ") error +=" - Address is too short";
    if (error!="") {
        alert("Error:\n\n"+error);
    } else {
        $("#btn-general-edit").val(" Saving . . . ");
        document.getElementById("btn-general-edit").disabled = true;
        var loadForm = "sections/2.1.2.php?p="+p+"&encrypt="+encrypt;
        $.post(loadForm, {
            edit: 1,
            id: id,
            name: name,
            add: addr
        }, function(data) {
            $("#general_form").html(data);
            document.getElementById("btn-general-edit").disabled = false;
        });
        loadGeneralList(p);
    }
}
function generalAdd (p) {
    var name = $("#generalName").val();
    var addr = $("#branchAdd").val();
    var error="";
    if (name.length < 2) error+=" - Name is too short\n";
    if (addr.length < 5 && addr!=" ") error +=" - Address is too short";
    if (error!="") {
        alert("Error:\n\n"+error);
    } else {
        $("#btn-general-add").val(" Saving . . . ");
        document.getElementById("btn-general-add").disabled = true;
        var loadForm = "sections/2.1.2.php?p="+p+"&encrypt="+encrypt;
        $.post(loadForm, {
            addNew: 1,
            name: name,
            add: addr
        }, function(data) {
            $("#general_form").html(data);
            loadGeneralList(p);
            document.getElementById("btn-general-add").disabled = false;
        });
    }
}
function delBranch(id) {
    var error = "";
    var isActive = 1;
    var branchID = document.getElementById("moveToBranch").value;
    // Checked => Deactivate ; Unchecked => Activate
    if (document.getElementById("disableEmp").checked) isActive=0;
    if (branchID==0) error+=" - Choose a branch to move employees to.";
    if (error!="") {
        alert("Error:\n\n"+error);
    } else {
        var c = confirm("Are you sure to remove this branch?\nThis cannot be undone.");
        if (c) {
            $("#btnBranchDel").val(" Please wait ... ");
            document.getElementById("btnBranchDel").disabled = true;
            var loadForm = "sections/2.1.2.php?p=2&encrypt="+encrypt;
            $.post(loadForm, {
                delete: 1,
                id: id,
                moveTo: branchID,
                isActive: isActive
            }, function() {
                loadGeneralList(2); loadGeneralForm (2);
            });
        }
    }
}
function reqFilter() {
    var loadList = "sections/2.2.php?p="+$("#selectReqType").val()+"&encrypt="+encrypt;
    $("#ajaxShow").html('Please wait . . .').load(loadList);
}
function loadRequestContent(id) {
    $("#requestContent").html("<BR><BR><BR<h4 style='text-align: center'>Loading . . .</h4>");
    $.post("sections/2.2.1.php?encrypt="+encrypt, {id: id}, function(data) {
        $("#requestContent").html(data);
    });
}
function requestResolved(id) {
    $("#btnReqResolved").val(" Please wait . . . ");
    document.getElementById("btnReqResolved").disabled = true;
    document.getElementById("btnRequestResponse").disabled = true;
    $.post("sections/2.2.1.php?encrypt="+encrypt, {id: id, resolved: 1}, function() {
        ajaxLoader(2.2);
    });
}
function requestResponse(id) {
    if (document.getElementById("requestResponseContent").value.length < 10) {
        alert("The response content is too short!");
    } else {
        $("#btnRequestResponse").val(" Please wait . . . ");
        document.getElementById("btnRequestResponse").disabled = true;
        document.getElementById("btnReqResolved").disabled = true;
        var content = "Hello user,<BR>This is a response email for your request at ECONNECT.<BR><BR>"+$("#requestResponseContent").val()+$("#originalContent").val();
        content = content.replace(/\r\n|\n|\r/g, '<br />');
        content = content.replace(/<BR>/gi, '<br />');
        $.post("sections/2.2.1.php?encrypt="+encrypt, {id: id, resolved: 1, response: content, empEmail: $("#reqEmpEmail").val()}, function() {
            ajaxLoader(2.2);
        });
    }
}
function updateDiscussSetting() {
    var post = document.getElementById("postPerPageLimit").value;
    var pin = document.getElementById("pinPostLimit").value;
    var chk = /^[0-9]{1,2}$/;
    var error = "";
    if (!chk.test(post) || !chk.test(pin)) error+=" - Invalid format, all fields must be numeric\n";
    if (pin<1 || pin>10) error+=" - Pinned post limit must between 1 and 10\n";
    if (post<3 || post>60) error+=" - Post limit must between 3 and 60\n";
    if (error!="") {
        alert("There are some errors occur:\n\n"+error);
    } else {
        $("#btnUpdateDiscuss").val(" Please wait . . . ");
        document.getElementById("btnUpdateDiscuss").disabled = true;
        $.post("sections/2.3.php?p=2&encrypt="+encrypt, {
            update: 1,
            post: post,
            pin: pin
        }, function(data) {
            $("#ajaxShow").html(data);
            document.getElementById("btnUpdateDiscuss").disabled = false;
        });
    }
}
// GENERAL end

// PERSONNEL -
function getPeopleSearch() {
    var loadFile = "sections/3.1.php?encrypt="+encrypt;
    var search = document.getElementById("peopleSearch").value;
    if (search!="") {
        $.post(loadFile, {
            q: search
        }, function(data) {
            $("#people-name-list").html(data);
        });
    }
}
function getPeople(id) {
    var loadFile = "sections/3.2.php?encrypt="+encrypt+"&empID="+id+"&noSearch";
    $("#personnel-form").load(loadFile);
}
function peopleSearchBox() {
    var search = $("#peopleSearch").val();
    if (search!="") {
        $.post("sections/3.1.php?encrypt="+encrypt, {
            q: search
        }, function(data) {
            $("#people-name-list").html(data);
        });
    } else {
        var loadFile = "sections/3.1.php?encrypt="+encrypt+"&all";
        $("#people-name-list").load(loadFile);
    }
}
function peopleFilter() {
    $("#peopleSearch").val('');
    var posID = $("#people-filter-byPos").val();
    var deptID = $("#people-filter-byDept").val();
    var branchID = $("#people-filter-byBranch").val();
    var loadFile = "dependences/3.2.php?encrypt="+encrypt+"&posID="+posID+"&deptID="+deptID+"&branchID="+branchID;
    $("#people-filter").load(loadFile);
}
$("#empImportBtn").hide();
function importFileBtn() {
    if (document.getElementById("empImport").value != "" ) {
        $("#empImportBtn").show(100);
    } else {
        $("#empImportBtn").hide(100);
    }
}
function resetPswd(id) {
    var mailchk = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    var error="";
    if (document.getElementById("empEmail").value=="" || mailchk.test(document.getElementById("empEmail").value)==false) {
        error += " - Not valid email address\n";
    }
    if (document.getElementById("empBackupEmail").value!="" && mailchk.test(document.getElementById("empBackupEmail").value)==false) {
        error += " - Not valid backup email\n";
    }
    if (error!="") {
        alert("Before resetting password, you have to fix these errors:\n\n"+error);
    } else {
        $.post("sections/3.2.php?encrypt="+encrypt, {
            personnelType: 2,
            empID: id,
            empEmail: $("#empEmail").val(),
            empBackupEmail: $("#empBackupEmail").val()
        }, function (data)  {
            $("#personnel-form").html(data);
        });
    }
}
function resetPic(id) {
    $.post("sections/3.2.php?encrypt="+encrypt, {
        personnelType: 3,
        empID: id
    }, function (data)  {
        $("#personnel-form").html(data);
    });
}
function updateInfo(id) {
    document.getElementById("submitBtn").disabled = true;
    $("#submitBtn").val(" Please wait... ");
    var posID = "";
    var change=0;
    var mailchk = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    var phonechk = /^([0-9]|[-+. ]){8,}$/;
    var skypechk = /^(?:[a-zA-Z]|live:)([a-zA-Z0-9]|[_\-,.@]){5,31}$/;
    var dobchk = /^[12][0-9]{3}(-[0-9]{2}){2}$/;
    var len = document.getElementById("empPos").length;
    for (var i=0; i<len; i++) {
        if (document.getElementById("empPos")[i].selected) {
            posID += document.getElementById("empPos")[i].value+";";
        }
    }
    function checked(part) {
        if (document.getElementById(part).checked) {
            return 1;
        } else {
            return 0;
        }
    }
    if (document.getElementById("emailBackup").value!=document.getElementById("empEmail").value) {
        change=1;
    }
    var error = "";
    if (document.getElementById("empNo").value=="") {
        error += " - Emp number required\n";
    }
    if (document.getElementById("empName").value=="") {
        error += " - Employee name required\n";
    }
    if (document.getElementById("empEmail").value=="" || mailchk.test(document.getElementById("empEmail").value)==false) {
        error += " - Valid Email required\n";
    }
    if (dobchk.test(document.getElementById("empDOB").value)==false) {
        error += " - Not valid birth date\n";
    }
    if (len<=0) {
        error += " - You have to select at least one position\n";
    }
    if (document.getElementById("empPhone").value!="" && phonechk.test(document.getElementById("empPhone").value)==false) {
        error += " - Not valid phone number\n";
    }
    if (document.getElementById("empMob").value!="" && phonechk.test(document.getElementById("empMob").value)==false) {
        error += " - Not valid mobile number\n";
    }
    if (document.getElementById("empSkype").value!="" && skypechk.test(document.getElementById("empSkype").value)==false) {
        error += " - Not valid Skype ID\n";
    }
    if (document.getElementById("empIDcard").value!="" && phonechk.test(document.getElementById("empIDcard").value)==false) {
        error += " - Not valid ID card number\n";
    }
    if (document.getElementById("empBackupEmail").value!="" && mailchk.test(document.getElementById("empBackupEmail").value)==false) {
        error += " - Not valid backup email\n";
    }
    if (error!="") {
        alert("Some errors occur:\n\n"+error);
        document.getElementById("submitBtn").disabled = false;
        $("#submitBtn").val(" Update ");
    } else {
        $.post("sections/3.2.php?encrypt="+encrypt, {
            personnelType: 1,
            empID: id,
            empPswd: $("#empPswd").val(),
            empNo: $("#empNo").val(),
            empName: $("#empName").val(),
            empEmail: $("#empEmail").val(),
            empGen: $("#empGen").val(),
            empDOB: $("#empDOB").val(),
            empPhone: $("#empPhone").val(),
            empMob: $("#empMob").val(),
            empSkype: $("#empSkype").val(),
            empIDcard: $("#empIDcard").val(),
            empBackupEmail: $("#empBackupEmail").val(),
            isActive: checked("isActive"),
            setupStat: $("#setupStat").val(),
            loginFail: 0,
            branchID: $("#empBranch").val(),
            deptID: document.getElementById("empDept").value,
            posID: posID,
            isAdmin: checked("isAdmin"),
            isUser: checked("isUser"),
            change: change
        }, function(data) {
            $("#personnel-form").html(data);
        });
    }
}
function addEmp() {
    document.getElementById("submitBtn").disabled = true;
    $("#submitBtn").val(" Please wait... ");
    var posID = "";
    var change=0;
    var mailchk = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    var phonechk = /^([0-9]|[-+. ]){8,}$/;
    var skypechk = /^(?:[a-zA-Z]|live:)([a-zA-Z0-9]|[_\-,.@]){5,31}$/;
    var dobchk = /^[12][0-9]{3}(-[0-9]{2}){2}$/;
    var len = document.getElementById("empPos").length;
    for (var i=0; i<len; i++) {
        if (document.getElementById("empPos")[i].selected) {
            posID += document.getElementById("empPos")[i].value+";";
        }
    }
    function checked(part) {
        if (document.getElementById(part).checked) {
            return 1;
        } else {
            return 0;
        }
    }
    var canPin=0;
    if (checked("isAdmin")) {
        canPin=1;
    }
    var error ="";
    if (document.getElementById("empName").value.length<5) {
        error += " - The name is too short\n";
    }
    if (document.getElementById("empNo").value.length<5) {
        error += " - The Employee Number is too short\n";
    }
    if (document.getElementById("empEmail").value=="" || mailchk.test(document.getElementById("empEmail").value)==false) {
        error += " - Valid Email required\n";
    }
    if (dobchk.test(document.getElementById("empDOB").value)==false) {
        error += " - Not valid birth date\n";
    }
    if (document.getElementById("empDept").value==0) {
        error += " - Choose a Department\n";
    }
    if (document.getElementById("empBranch").value==0) {
        error += " - Choose a Branch\n";
    }
    if (len<=0) {
        error += " - You have to select at least one position\n";
    }
    if (document.getElementById("empPhone").value!="" && phonechk.test(document.getElementById("empPhone").value)==false) {
        error += " - Not valid phone number\n";
    }
    if (document.getElementById("empMob").value!="" && phonechk.test(document.getElementById("empMob").value)==false) {
        error += " - Not valid mobile number\n";
    }
    if (document.getElementById("empSkype").value!="" && skypechk.test(document.getElementById("empSkype").value)==false) {
        error += " - Not valid Skype ID\n";
    }
    if (document.getElementById("empIDcard").value!="" && phonechk.test(document.getElementById("empIDcard").value)==false) {
        error += " - Not valid ID card number\n";
    }
    if (document.getElementById("empBackupEmail").value!="" && mailchk.test(document.getElementById("empBackupEmail").value)==false) {
        error += " - Not valid backup email\n";
    }
    if (error!="") {
        alert("Some errors occur:\n\n"+error);
        document.getElementById("submitBtn").disabled = false;
        $("#submitBtn").val(" Save ");
    } else {
        $.post("sections/3.2.php?encrypt="+encrypt, {
            personnelType: 4,
            empNo: $("#empNo").val(),
            empName: $("#empName").val(),
            empEmail: $("#empEmail").val(),
            empGen: $("#empGen").val(),
            empDOB: $("#empDOB").val(),
            empPhone: $("#empPhone").val(),
            empMob: $("#empMob").val(),
            empSkype: $("#empSkype").val(),
            empIDcard: $("#empIDcard").val(),
            empBackupEmail: $("#empBackupEmail").val(),
            isActive: checked("isActive"),
            setupStat: $("#setupStat").val(),
            loginFail: 0,
            branchID: $("#empBranch").val(),
            deptID: document.getElementById("empDept").value,
            posID: posID,
            isAdmin: checked("isAdmin"),
            isUser: checked("isUser"),
            change: change,
            canPin: canPin
        }, function(data) {
            $("#personnel-form").html(data);
        });
    }
}
function getTrackInfo(id) {
    var loadFile = "sections/3.3.2.php?encrypt="+encrypt+"&part=1&empID="+id;
    $("#tracking").html("Loading...").load(loadFile);
}
// PERSONNEL end

// SYSTEM -
function cleanSysCustom() {
    var check = /^[0-9]{1,2}$/;
    var m = document.getElementById("cleanMonth").value;
    if (check.test(m) && m>0 && m<73) {
        var c = confirm("Are you sure to clean all data older than "+m+" month(s)?\n\nThis CANNOT be undone!");
        if (c) {
            var loadFile = "sections/4.2.php?encrypt="+encrypt+"&system=clean&m="+m;
            $("#ajaxShow").load(loadFile);
        }
    } else {
        alert ("Please enter month number between 1 and 72");
    }
}
function shutdownSys(stat) {
    var c;
    var loadFile;
    if (stat=="WORKING") {
        c = confirm("Are you sure to SUSPEND the system?\n\nNo one can login after shutting down.");
        if (c) {
            loadFile = "sections/4.2.php?encrypt="+encrypt+"&system=shut";
            $("#ajaxShow").load(loadFile);
        }
    } else {
        c = confirm("Are you sure to START the system?\n\nEveryone can login normally after this.\nDon't do this if the debugging process doesn't completed or you're still under attack.");
        if (c) {
            loadFile = "sections/4.2.php?encrypt="+encrypt+"&system=start";
            $("#ajaxShow").load(loadFile);
        }
    }
}
function submitSysSettings() {
    var name = $("#companyName").val();
    if (name.length<3 || name.length >15) {
        alert("Company name must between 3 and 15 characters");
    } else {
        $.post("sections/4.php?encrypt="+encrypt, {
            editSetting: 1,
            companyName: name,
            dateFormat: $("#dateFormat").val(),
            timeFormat: $("#timeFormat").val(),
            loginFailLock: $("#loginFailLock").val()
        }, function(data) {
            $("#ajaxShow").html(data);
        });
    }
}
function getAdminPrivilege(id) {
    var loadFile = "sections/4.3.2.php?encrypt="+encrypt+"&empID="+id;
    $("#adminPrivilege-form").load(loadFile);
}
function checkPrivilege() {
    if (document.getElementById("master").checked) {
        document.getElementById("dashboard").checked = true;
        document.getElementById("setting").checked = true;
    } else {
        if (!document.getElementById("setting").checked) {
            document.getElementById("master").checked = false;
            document.getElementById("master").disabled = true;
        } else {
            document.getElementById("master").disabled = false;
        }
    }
}
function savePrivilege(id) {
    document.getElementById("submitBtn").disabled = true;
    $("#submitBtn").val(" Please wait... ");
    function check(d) {
        if (d) {
            return 1;
        } else {
            return 0;
        }
    }
    $.post("sections/4.3.2.php?encrypt="+encrypt+"&empID="+id, {
        empID: id,
        dashboard: check(document.getElementById("dashboard").checked),
        general: check(document.getElementById("general").checked),
        personnel: check(document.getElementById("personnel").checked),
        setting: check(document.getElementById("setting").checked),
        master: check(document.getElementById("master").checked)
    }, function(data) {
        $("#adminPrivilege-form").html(data);
    });
}
// SYSTEM end

// Main content
var main = function() {
	$("#people-popup,#popup-bg").hide();
	// Pop-ups controlling
	$(".comment-btn").click(function() {
		$("#discuss-popup-comment, #popup-bg").show(200);
	});
	$(".save-btn").click(function() {
		$("#discuss-popup-save, #popup-bg").show(200);
	});
	$(".close-popup, #popup-bg").click(function() {
		$(".popup, #popup-bg").hide(200);
	});
	
	$("#agenda-add-event").click(function() {
		$("#agenda-popup, #popup-bg").show(200);
	});
	
	
	/*$(document).keypress("s",function(e){
		if (e.ctrlKey && e.shiftKey) {
			$("#search-float").toggle(100);
			$("#search-float").toggleClass("nav-current");
			$("#nav-tabs").toggle(200);
			document.getElementById("search-master").value = "";
			document.getElementById("search-master").focus();
		}
	});*/
		
	
	
	$("#scroll-area-discuss").height($(document).height()*0.72);
	$("#scroll-area-event").height($(document).height()*0.80);
	
	$(".close-popup").click(function() {
		$(".popup, #popup-bg").hide(200);
	});
	
	$(".col").css("height", screen.height * 0.71);
	
	$("#menu ul li").click(function() {
        document.getElementById("ajaxShow").innerHTML = "<h1>Loading ...</h1>";
		$("#menu ul li").removeClass("current");
		$(this).addClass("current")
    });
	
}
$(document).ready(main);

