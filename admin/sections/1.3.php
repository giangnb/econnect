<?php
session_start();
$password = '5dQ66mdmOU6EyFgiGC6BRKDSt2NbPGehS94IJGn54x38l3';
if (password_verify($password, $_SESSION['token'])) {
    echo "";
}
else {
    echo "<script type='text/javascript'>function clear() {document.body.innerHTML = '<h1>JSON BAD REQUEST!</h1>';}window.setInterval(clear,1);</script>";
    header("Location: ../index.php?out=0&w=1");
    die();
}
if (!$_SESSION['dashRight']) {
    echo "<center><h2 class=\"text-danger\">You don't have permission to access this panel!</h2></center>";
    die();
}
$token = $_SESSION['token'];
include '../conn.php';
include '../encrypt/encrypter.php';
$sql = "SELECT empEmail FROM emp JOIN permission ON emp.empID = permission.empID WHERE master=1";
$r = $conn->query($sql);
$adminEmail = array();
if ($r->num_rows > 0) {
    while ($row = $r->fetch_assoc()) {
        $adminEmail[] = decrypt($row['empEmail']);
    }
}
?>

<h1>Your permission</h1>
<?php
function writeRight($rightVal) {
	if ($rightVal) {
		echo "<b class=\"text-info\">Yes</b>";
	} else {
		echo "<b class=\"text-danger\">No</b>";
	}
}
?>
<table class="table table-hover" width="100%" ng-controller="">
	<tr style="text-align:center;">
    	<th width="65%">Permission</th>
        <th>Status</th>
    </tr>
    <tr>
    	<td>Dashboard</td>
        <td><?php writeRight($_SESSION['dashRight']) ?></td>
    </tr>
    <tr>
    	<td>General</td>
        <td><?php writeRight($_SESSION['genRight']) ?></td>
    </tr>
    <tr>
    	<td>Personnel</td>
        <td><?php writeRight($_SESSION['perRight']) ?></td>
    </tr>
    <tr>
    	<td>Settings</td>
        <td><?php writeRight($_SESSION['setRight']) ?></td>
    </tr>
    <tr>
    	<td>User managements</td>
        <td><?php writeRight($_SESSION['usrRight']) ?></td>
    </tr>
</table>

<div class="jumbotron">
	Contact one of these emails below to request permission:<BR>
    <ul>
		<?php
        foreach ($adminEmail as $email) {
            echo "<li><a href=\"mailto:{$email}\">{$email}</li>";
        }
        if (count($adminEmail)==0) {
            echo " - No one is available to change your permission.<BR>Please contact eConnect programming team to resolve this: <a href='mailto:econnect@giangnb.com'>econnect@giangnb.com</a>";
        }
        ?>
    </ul>
</div>
				<script type="text/javascript" src="script/script.js"></script>