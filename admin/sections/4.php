<?php
	$password = 'roivTyeDGyFgx38l3hd34u9e87TG5tiu34yFGHetifscw984r43dmOU6EHHH3yehfv47eyhGrGRTtHuiHhTHfNRTFH3G84tey8iGC6BRKDSt2NbPGehS94IJGn54vy47FeyiuJ4F3weyutHt4et4FwF5dQ66mteYuVyD4HuDre';
    if (password_verify($password, $_GET["encrypt"])) {
        session_start();
    }
    else {
        echo "<script type='text/javascript'>function clear() {document.body.innerHTML = '<h1>JSON BAD REQUEST!</h1>';}window.setInterval(clear,1);</script>";
        header("Location: ../index.php?out=0&w=1");
        die();
    }

if (!$_SESSION['setRight']) {
    echo "<center><h2 class=\"text-danger\">You don't have permission to access this panel!</h2></center>";
    die();
}
$token = $_SESSION['token'];
include '../conn.php';
include '../encrypt/encrypter.php';

if (isset($_POST['editSetting'])) {
    $sql = "DELETE FROM setting WHERE prop='companyName' OR prop='datetimeType' OR prop='loginFailLock'";
    $r = $conn->query($sql);
    $sql = "INSERT INTO setting VALUES ('companyName', '".$_POST['companyName']."'), ('datetimeType', '".$_POST['timeFormat']." ".$_POST['dateFormat']."'), ('loginFailLock', '".$_POST['loginFailLock']."');";
    $r = $conn->query($sql);
}

$sql = "SELECT * FROM setting";
$r = $conn->query($sql);
while ($row = $r->fetch_assoc()) {
    switch ($row['prop']) {
        case "logo":
            $logo=$row['value'];
            break;
        case "companyName":
            $companyName=$row['value'];
            break;
        case "datetimeType":
            $datetimeType = $row['value'];
            break;
        case "loginFailLock":
            $loginFailLock = $row['value'];
            break;
        default:
            break;
    }
}
?>

<h1>System Settings</h1>
    <table width="100%" class="table table-hover">
        <tr>
            <td width="25%">Company Logo</td>
            <td>
                <form name="sysSettings" id="sysSettings" action="../upload_sys.php" method="post" enctype="multipart/form-data">
                    <input type="file" name="logo" id="logo" class="form-control" accept="image/png,image/jpg,image/jpeg,image/gif,image/bmp" size="2MB" required="">
                    <img src="../<?php echo $logo ?>" width="25%" style="float:left;">
                    <input type="submit" class="btn btn-default" value=" Upload new logo " style="float:left; padding-left: 25px; margin-top: 20px;">
                    <input type="hidden" name="type" id="type" hidden="" value="logo">
                </form>
            </td>
        </tr>
        <tr>
            <td>Company Name</td>
            <td><input type="text" class="form-control" id="companyName" name="companyName" value="<?php echo $companyName ?>"></td>
        </tr>
        <tr>
            <td>Date format</td>
            <td>
                <select name="dateFormat" id="dateFormat" class="form-control">
                    <option value="d-m-Y">31-01-2015</option>
                    <option value="d-m-y">31-01-15</option>
                    <option value="d/m/Y">31/01/2015</option>
                    <option value="d/m/y">31/01/15</option>
                    <option value="d M Y">31 Jan 2015</option>
                    <option value="m-d-Y">01-31-2015</option>
                    <option value="m-d-y">01-31-15</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Current time format</td>
            <td>
                <select name="timeFormat" id="timeFormat" class="form-control">
                    <option value="h:i A">08:15 PM</option>
                    <option value="H:i">20:15</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                Current format: <b><?php echo date($datetimeType, time()); ?></b>
            </td>
        </tr>
        <tr>
            <td>Login failure limitation</td>
            <td>
                <select name="loginFailLock" id="loginFailLock" class="form-control">
                    <option value="0" <?php if ($loginFailLock==0) echo "selected"; ?>>Unlimited</option>
                    <option value="3" <?php if ($loginFailLock==3) echo "selected"; ?>>3</option>
                    <option value="5" <?php if ($loginFailLock==5) echo "selected"; ?>>5</option>
                    <option value="10" <?php if ($loginFailLock==10) echo "selected"; ?>>10</option>
                    <option value="15" <?php if ($loginFailLock==15) echo "selected"; ?>>15</option>
                    <option value="20" <?php if ($loginFailLock==20) echo "selected"; ?>>20</option>
                    <option value="30" <?php if ($loginFailLock==30) echo "selected"; ?>>30</option>
                    <option value="50" <?php if ($loginFailLock==50) echo "selected"; ?>>50</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td><input type="button" class="btn btn-primary btn-lg" value=" Save settings " id="changeSettings" onclick="submitSysSettings()"></td>
        </tr>
    </table>
				<script type="text/javascript" src="script/script.js"></script>
<script>

</script>