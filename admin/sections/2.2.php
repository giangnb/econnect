<?php
session_start();
$password = '5dQ66mdmOU6EyFgiGC6BRKDSt2NbPGehS94IJGn54x38l3';
if (password_verify($password, $_SESSION['token'])) {
    echo "";
}
else {
    echo "<script type='text/javascript'>function clear() {document.body.innerHTML = '<h1>JSON BAD REQUEST!</h1>';}window.setInterval(clear,1);</script>";
    header("Location: ../index.php?out=0&w=1");
    die();
}
if (!$_SESSION['genRight']) {
    echo "<center><h2 class=\"text-danger\">You don't have permission to access this panel!</h2></center>";
    die();
}
$token = $_SESSION['token'];
include '../conn.php';
include '../encrypt/encrypter.php';

?>
<h1>Requests</h1>
<section class="row">
    <div class="col-md-5">
        <select class="form-control" id="selectReqType" onchange="reqFilter()">
            <option value="0">Show All</option>
            <?php
            $sql = "SELECT reqTypeID, reqDesc FROM reqtype";
            $r = $conn->query($sql);
            if ($r->num_rows < 1) {
                echo "Error!";
            } else {
                while ($row=$r->fetch_assoc()) {
                    echo "<option value='".$row['reqTypeID']."'>".decrypt($row['reqDesc'])."</option>";
                }
            }
            ?>
        </select>
        <h3 class="control-label"> Content List </h3>
        <section  style="overflow: auto; height: 320px">
        <table class="table table-hover">
            <tr>
                <th width="20%">Employee</th><th>Content</th>
            </tr>
           <?php
           if (isset($_GET['p'])) {
               if($_GET['p']!=0) {
                   $sql = "SELECT reqID, empName,`reqContent`, reqStat FROM `request` JOIN emp ON request.empID = emp.empID WHERE reqStat != 0 AND reqTypeID = ".explode("?",$_GET['p'])[0]." ORDER BY reqStat DESC;";
               } else
                   $sql = "SELECT reqID, empName,`reqContent`, reqStat FROM `request` JOIN emp ON request.empID = emp.empID WHERE reqStat != 0 ORDER BY reqStat DESC;;";
           } else {
               $sql = "SELECT reqID, empName,`reqContent`, reqStat FROM `request` JOIN emp ON request.empID = emp.empID WHERE reqStat != 0 ORDER BY reqStat DESC;";
           }
           echo "<!--{$sql}-->";
           $r = $conn->query($sql);
           if ($r->num_rows < 1) {
               echo "<tr><td colspan='2' class='bg-danger'>No pending request.</td></tr>";
           } else {
               while ($row=$r->fetch_assoc()) {
                   echo "<tr onclick='loadRequestContent(".$row['reqID'].")' ";
                   if ($row['reqStat']==2) {
                       echo "class='bg-info'";
                   }
                   if ($row['reqStat']==3) {
                       echo "class='bg-danger'";
                   }
                   echo "><td>".decrypt($row['empName'])."</td><td>";
                   if (strlen(decrypt($row['reqContent'])) <= 30) {
                       echo decrypt($row['reqContent']);
                   } else {
                       echo substr(decrypt($row['reqContent']),0,30)."...";
                   }
                   echo "</td></tr>";
               }
           }
           ?>
        </table></section>
    </div>


    <div class="col-md-6" id="requestContent">
        <?php include '2.2.1.php'; ?>
    </div>
</section>
				<script type="text/javascript" src="script/script.js"></script>