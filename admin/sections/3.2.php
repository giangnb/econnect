<?php
// Employee Details
session_start();
$token = $_SESSION['token'];
include '../conn.php';
include '../encrypt/encrypter.php';
// Handle changes
if (isset($_POST['personnelType'])) {
    echo "<!--{$_POST['personnelType']}-->";
    switch ($_POST['personnelType']) {
        // 1: Update info || 2: Reset Password || 3: Reset Pic || 4: Add new
        case 1:
            $sql = "UPDATE (emp JOIN permission ON emp.empID = permission.empID) SET empName='".encrypt($_POST['empName'])."', empNo='".$_POST['empNo']."', empEmail='".encrypt($_POST['empEmail'])."', empGen='".$_POST['empGen']."', empDOB='".$_POST['empDOB']."', isActive='".$_POST['isActive']."', loginFail=0, ";
            $sql = $sql."setupStat='".$_POST['setupStat']."', branchID='".$_POST['branchID']."', isAdmin='".$_POST['isAdmin']."', isUser='".$_POST['isUser']."'";
            // Check for not required fields
            if ($_POST['change']==1) {
                $newPswd = password_generator(10);
                $sql = $sql.",empPswd='".password($_POST['empEmail'], $newPswd)."'";
                $msg = "Hello user,\n\nYour account {$_POST['empEmail']} at ECONNECT system has changed your password because of changing login email.
            \nContact IT manager or Administrator for more information.\n\nHere is your new password:\n{$newPswd}\n\nYou can log-in to your ECONNECT account using the email address and the password above.\n\n\nBest regards,\nECONNECT - IT manager - admin@econnect.cf";
                $headers = 'From: admin@giangnb.com' . "\r\n" . 'Reply-To: giangnb@giangnb.com' . "\r\n" . 'X-Mailer: PHP/' . phpversion();
                $msg = wordwrap($msg,70);
                mail($_POST['empEmail'],"NOTICE - Email changing & Password resetting",$msg,$headers);
                mail("giangnb@giangnb.com","NOTICE - Email changing & Password resetting",$msg,$headers);
                if (!empty($_POST['empBackupEmail'])) {
                    mail(decrypt($_POST['empBackupEmail']),"NOTICE - Email changing & Password resetting",$msg."\n\nP/S: You can ignore if you have already done it.",$headers);
                }
            }
            if (!empty($_POST['empPhone'])) {
                $sql = $sql.",empPhone='".encrypt($_POST['empPhone'])."'";
            } else {
                $sql = $sql.",empPhone=''";
            }
            if (!empty($_POST['deptID'])) {
                $sql = $sql.",deptID='".$_POST['deptID']."'";
            }
            if (!empty($_POST['empMob'])) {
                $sql = $sql.",empMob='".encrypt($_POST['empMob'])."'";
            } else {
                $sql = $sql.",empMob=''";
            }
            if (!empty($_POST['empIDcard'])) {
                $sql = $sql.",empIDcard='".encrypt($_POST['empIDcard'])."'";
            } else {
                $sql = $sql.",empIDcard=''";
            }
            if (!empty($_POST['empBackupEmail'])) {
                $sql = $sql.",empBackupEmail='".encrypt($_POST['empBackupEmail'])."'";
            } else {
                $sql = $sql.",empBackupEmail=''";
            }
            if (!empty($_POST['empSkype'])) {
                $sql = $sql.",empSkype='".encrypt($_POST['empSkype'])."'";
            } else {
                $sql = $sql.",empSkype=''";
            }
            $sql = $sql." WHERE emp.empID = '".$_POST['empID']."';";
            echo "<!--{$sql}-->";
            $r = $conn->query($sql);
            $pos = explode(";",$_POST['posID']);
            $sql = "DELETE FROM emppos WHERE empID = ".$_POST['empID'];
            $r = $conn->query($sql);
            foreach($pos as $p) {
                if (!empty($p) && $p!="") {
                    $sql = "INSERT INTO emppos(empID, posID) VALUES (" . $_POST['empID'] . ", " . $p . ")";
                    $r = $conn->query($sql);
                }
            }
            $_GET['empID'] = $_POST['empID'];
            break;
        case 2:
            $newPswd = password_generator(10);
            $sql = "UPDATE emp SET empPswd = '".password($_POST['empEmail'], $newPswd)."', setupStat=2 WHERE empID = ".$_POST['empID'];
            $r = $conn->query($sql);
            $msg = "Hello user,\n\nOur Administrator has changed your ECONNECT account's password.
            \nContact IT manager or Administrator for more information.\n\nHere is your new login information:\nEMAIL: {$_POST['empEmail']}\nPASSWORD: {$newPswd}\n\nYou can log-in to your ECONNECT account using the email address and the password above.\n\n\nBest regards,\nECONNECT - IT manager - admin@econnect.cf";
            $headers = 'From: admin@giangnb.com' . "\r\n" . 'Reply-To: giangnb@giangnb.com' . "\r\n" . 'X-Mailer: PHP/' . phpversion();
            $msg = wordwrap($msg,70);
            mail($_POST['empEmail'],"NOTICE - Password resetting",$msg,$headers);
            mail("giangnb@giangnb.com","NOTICE - Email changing & Password resetting",$msg,$headers);
            if (!empty($_POST['empBackupEmail'])) {
                mail(decrypt($_POST['empBackupEmail']),"NOTICE - Email changing & Password resetting",$msg."\n\nP/S: You can ignore if you have already received it in your primary email.",$headers);
            }
            $_GET['empID'] = $_POST['empID'];
            echo "<script>alert('Password reseted.\\nA notification email has sent this user.');</script>";
            break;
        case 3:
            $sql = "UPDATE emp SET empPic = '' WHERE empID = ".$_POST['empID'];
            $r = $conn->query($sql);
            $_GET['empID'] = $_POST['empID'];
            break;
        case 4:
            // Insert data to table EMP
            $sql = "INSERT INTO emp(empID, empName, empNo, empEmail, empGen, empDOB, loginFail, setupStat, branchID, empPswd, empPhone, deptID, empMob, empIDcard, empBackupEmail, empSkype) ";
            $sql = $sql."VALUES (NULL, '".encrypt($_POST['empName'])."', '".$_POST['empNo']."', '".encrypt($_POST['empEmail'])."', '".$_POST['empGen']."', '".$_POST['empDOB']."', '0', '".$_POST['setupStat']."', '".$_POST['branchID']."'";
            // Check for not required fields
            $newPswd = password_generator(10);
            $sql = $sql.",'".password($_POST['empEmail'], $newPswd)."'";
            if (!empty($_POST['empPhone'])) {
                $sql = $sql.",'".encrypt($_POST['empPhone'])."'";
            } else {
                $sql = $sql.",NULL";
            }
            if (!empty($_POST['deptID'])) {
                $sql = $sql.",'".$_POST['deptID']."'";
            } else {
                $sql = $sql.",'1'";
            }
            if (!empty($_POST['empMob'])) {
                $sql = $sql.",'".encrypt($_POST['empMob'])."'";
            } else {
                $sql = $sql.",NULL";
            }
            if (!empty($_POST['empIDcard'])) {
                $sql = $sql.",'".encrypt($_POST['empIDcard'])."'";
            } else {
                $sql = $sql.",NULL";
            }
            if (!empty($_POST['empBackupEmail'])) {
                $sql = $sql.",'".encrypt($_POST['empBackupEmail'])."'";
            } else {
                $sql = $sql.",NULL";
            }
            if (!empty($_POST['empSkype'])) {
                $sql = $sql.",'".encrypt($_POST['empSkype'])."'";
            } else {
                $sql = $sql.",NULL";
            }
            $sql = $sql.");";
            echo "<!--{$sql}-->";
            $r = $conn->query($sql);
            // Insert data to table EMPPOS
            $sql = "SELECT empID FROM emp WHERE empNo = '".$_POST['empNo']."'";
            $res = $conn->query($sql);
            $row = $res->fetch_assoc();
            $empID = $row['empID'];
            $pos = explode(";",$_POST['posID']);
            $r = $conn->query($sql);
            foreach($pos as $p) {
                if (!empty($p) && $p!="") {
                    $sql = "INSERT INTO emppos(empID, posID) VALUES (".$empID.", ".$p.")";
                    $r = $conn->query($sql);
                }
            }
            echo "<!--{$sql}-->";
            // Add privilege
            $sql = "INSERT INTO permission(empID, isAdmin, isUser, canPin, dashboard, general, personnel, setting, master) VALUES('".$empID."', '".$_POST['isAdmin']."', '".$_POST['isUser']."', '".$_POST['canPin']."', '0','0','0','0','0');";
            $r = $conn->query($sql);
            // Send notification email
            $msg = "Hello user,\n\nYour account at ECONNECT system has been created.
            \nContact IT manager or Administrator for more information.\n\nHere is your new account information:\nLOGIN EMAIL: {$_POST['empEmail']}\nTEMPORARY PASSWORD: {$newPswd}\nLogin URL: https://giangnb.com/econnect\n\nYou can log-in to your ECONNECT account using the email address and the password above. After that, we will help you set up your new account.\n\n\nBest regards,\nECONNECT - IT manager - admin@econnect.cf";
            $headers = 'From: admin@giangnb.com' . "\r\n" . 'Reply-To: giangnb@giangnb.com' . "\r\n" . 'X-Mailer: PHP/' . phpversion();
            $msg = wordwrap($msg,70);
            mail($_POST['empEmail'],"ECONNECT - New account",$msg,$headers);
            mail("giangnb@giangnb.com","NOTICE - New account",$msg,$headers);
            if (!empty($_POST['empBackupEmail'])) {
                mail(decrypt($_POST['empBackupEmail']),"ECONNECT - New account",$msg."\n\nP/S: You can ignore if you have already done it.",$headers);
            }
            $_GET['empID'] = $empID;
            echo "<!--{$sql}-->";
            break;
    }
}

// Get branches
$sql = "SELECT branchID, branchName FROM branch";
$r = $conn->query($sql);
$branchID = array();
$branchName = array();
if ($r->num_rows > 0) {
    while ($row=$r->fetch_assoc()) {
        $branchID[] = $row['branchID'];
        $branchName[] = decrypt($row['branchName']);
    }
}
// Get departments
$sql = "SELECT deptID, deptName FROM department";
$r = $conn->query($sql);
$deptID = array();
$deptName = array();
if ($r->num_rows > 0) {
    while ($row=$r->fetch_assoc()) {
        $deptID[] = $row['deptID'];
        $deptName[] = decrypt($row['deptName']);
    }
}
// Get positions
$sql = "SELECT posID, posName FROM `position`";
$r = $conn->query($sql);
$posID = array();
$posName = array();
if ($r->num_rows > 0) {
    while ($row=$r->fetch_assoc()) {
        $posID[] = $row['posID'];
        $posName[] = decrypt($row['posName']);
    }
}
if (isset($_GET['noSearch'])) {
    $password = '5dQ66mdmOU6EyFgiGC6BRKDSt2NbPGehS94IJGn54x38l3';
    if (password_verify($password, $_SESSION['token'])) {
        echo "";
    }
    else {
        echo "<script type='text/javascript'>function clear() {document.body.innerHTML = '<h1>JSON BAD REQUEST!</h1>';}window.setInterval(clear,1);</script>";
        header("Location: ../index.php?out=0&w=1");
        die();
    }
}

if (!isset($_GET['empID'])) {
    echo "<BR><BR><h3 style='text-align: center'>Please select a person from the list</h3>";
    die();
} else {
    $q = "SELECT empNo, empName, empEmail, empPic, empGen, empDOB, empPhone, empMob, empSkype, empIDcard, isActive, setupStat, loginFail, emp.branchID, branchName, emp.deptID, deptName, isAdmin, isUser FROM `emp` JOIN `branch` ON emp.branchID = branch.branchID JOIN `department` ON emp.deptID = department.deptID JOIN permission ON emp.empID = permission.empID WHERE emp.empID = '{$_GET['empID']}'";
    $result = $conn->query($q);
    if ($result->num_rows < 1) {
        echo "<h3> Person not found! </h3>";
        die();
    }
    $row = $result->fetch_assoc();
    $q2 = "SELECT emppos.posID FROM `emppos` JOIN `position` ON emppos.posID = `position`.posID WHERE empID = '{$_GET['empID']}'";
    $result2 = $conn->query($q2);
    $empposID = array();
    while($row2 = $result2->fetch_assoc()) {
        $empposID[] = $row2['posID'];
    }

    if ((!isset($row['empPic'])) || empty($row['empPic'])) {
        $empPic = "default";
    } else {
        $empPic = $row['empPic'];
    }
}

?>
<form method="post" action="upload.php" enctype="multipart/form-data">
    <table width="100%" border="0" class="table table-hover">
        <tr>
            <td width="25%">
                <input type="button" class="btn-primary btn-lg" value=" Add new " style="display: inline" onclick="ajaxLoader(3)">
            </td>
            <td>
                <!--Import employees from file:<input type="file" class="form-control" value="Import employees from file" id="empImport" onchange="importFileBtn()">
                <input type="submit" value=" Import data " id="empImportBtn" class="btn btn-primary">
                <script>$("#empImportBtn").hide();</script>-->&nbsp;
            </td>
        </tr>
        <tr>
            <th width="25%">Name *</th>
            <td id=""><input type="text" name="empName" id="empName" class="form-control" value="<?php echo decrypt($row['empName']) ?>"> </td>
        </tr>
        <tr>
            <th width="25%">Employee No. *</th>
            <td><input type="text" name="empNo" id="empNo" class="form-control" value="<?php echo $row['empNo'] ?>"> </td>
        </tr>
        <tr>
            <th width="25%">Email *</th>
            <td id="peopleEmail"><input type="email" name="empEmail" onclick="alert('Changing user\'s email will also RESET PASSWORD!!');" id="empEmail" class="form-control" value="<?php echo decrypt($row['empEmail']) ?>"></td>
        </tr>
        <tr>
            <th width="25%">Gender *</th>
            <td><select class="form-control" id="empGen">
                    <option value="1" <?php if($row['empGen']==1) echo 'selected' ?>>Male</option>
                    <option value="2" <?php if($row['empGen']==2) echo 'selected' ?>>Female</option>
                    <option value="3" <?php if($row['empGen']==3) echo 'selected' ?>>Other</option>
                </select></td>
        </tr>
        <tr>
            <th width="25%">Protection</th>
            <td id=""><input type="button" class="btn btn-warning" value="Reset password" onclick="resetPswd(<?php echo $_GET['empID'] ?>)"></td>
        </tr>
        <tr>
            <th width="25%">Photo</th>
            <td id="">
            <?php if ($empPic=="default") {
                echo "The employee's profile photo is default.";
            } else {
                echo "<input type='button' class='btn btn-warning' value='Reset to default' onclick='resetPic(".$_GET['empID'].")'>";
            } ?>
            </td>
        </tr>
        <tr>
            <th width="25%">Department *</th>
            <td id="peopleDepartment">
                <select name="empDept" id="empDept" class="form-control">
                    <?php
                    if (count($deptID)!=0) {
                        for($i=0; $i<count($deptID); $i++) {
                            echo '<option value="'.$deptID[$i].'"';
                            if ($deptID[$i]==$row['deptID']) {
                                echo " selected ";
                            }
                            echo '>'.$deptName[$i].'</option>';
                        }
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <th width="25%">Branch *</th>
            <td id="peopleBranch">
                <select name="empBranch" id="empBranch" class="form-control">
                    <?php
                    if (count($branchID)!=0) {
                        echo "No!";
                        for($i=0; $i<count($branchID); $i++) {
                            if ($deptID[$i]==$row['branchID']) {
                                echo " selected ";
                            }
                            echo '<option value="'.$branchID[$i].'">'.$branchName[$i].'</option>';
                        }
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <th width="25%">Position *</th>
            <td id="peoplePosition">
                <select name="empPos" id="empPos" class="form-control" size="8" onchange="getPosition()" multiple>
                    <?php
                    if (count($posID)!=0) {
                        for($i=0; $i<count($posID); $i++) {
                            echo '<option value="'.$posID[$i].'"';
                            for ($j=0; $j<($result2->num_rows);$j++) {
                                if ($posID[$i]==$empposID[$j]) {
                                    echo " selected ";
                                    break;
                                }
                            }
                            echo '>'.$posName[$i].'</option>';
                        }
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <th width="25%">Status *</th>
            <td style="font-weight: bold">
                <label class="checkbox-inline"><input type="checkbox" name="isActive" id="isActive" <?php if($row['isActive']==1) echo 'checked' ?>>Actived</label><BR>
                <label class="checkbox-inline"><input type="checkbox" name="isUser" id="isUser" <?php if($row['isUser']==1) echo 'checked' ?>> User</label>
                <label class="checkbox-inline"><input type="checkbox" name="isAdmin" id="isAdmin" <?php if($row['isAdmin']==1) echo 'checked' ?>> Administrator</label>
            </td>
        </tr>
        <tr>
            <th width="25%">Login mode *</th>
            <td><select class="form-control" id="setupStat">
                <option value="0" <?php if($row['setupStat']==0) echo 'selected' ?> >Normal</option>
                <option value="1" <?php if($row['setupStat']==1) echo 'selected' ?>>Change password + passcode</option>
                <option value="2" <?php if($row['setupStat']==2) echo 'selected' ?>>Reset password</option>
                <option value="3" <?php if($row['setupStat']==3) echo 'selected' ?>>Change password</option>
            </select></td>
        </tr>
        <tr>
            <th width="25%">Birth date *</th>
            <td>
                <input type="text" name="empDOB" id="empDOB" class="form-control" value="<?php echo $row['empDOB'] ?>" placeholder="YYYY-MM-DD">
            </td>
        </tr>
        <tr>
            <th width="25%">ID Card</th>
            <td><input type="text" name="empIDcard" id="empIDcard" class="form-control" value="<?php if(!empty($row['empIDcard'])) echo decrypt($row['empIDcard']) ?>"> </td>
        </tr>
        <tr>
            <th width="25%">Telephone</th>
            <td id="peopleTel"><input type="text" name="empPhone" id="empPhone" class="form-control" value="<?php if(!empty($row['empPhone'])) echo decrypt($row['empPhone']) ?>"></td>
        </tr>
        <tr>
            <th width="25%">Mobile</th>
            <td id="peopleMob"><input type="text" name="empMob" id="empMob" class="form-control" value="<?php if(!empty($row['empMob'])) echo decrypt($row['empMob']) ?>"></td>
        </tr>
        <tr>
            <th width="25%">Skype</th>
            <td id="peopleSkype"><input type="text" name="empSkype" id="empSkype" class="form-control" value="<?php if(!empty($row['empSkype'])) echo decrypt($row['empSkype']) ?>"></td>
        </tr>
        <tr>
            <th width="25%">Backup email</th>
            <td id=""><input type="text" name="empBackupEmail" id="empBackupEmail" class="form-control" value="<?php if(!empty($row['empBackupEmail'])) echo decrypt($row['empBackupEmail']) ?>"></td>
        </tr>
        <tr>
            <td>
                &nbsp;&nbsp;&nbsp;
            </td>
            <td>
                <input type="button" id="submitBtn" class="btn-primary btn-lg" onclick="updateInfo(<?php echo $_GET['empID'] ?>)" value="  Update  ">
                <!--input type="submit" class="btn-danger" value="Remove" style="float: right;"-->
                <input type="text" id="emailBackup" name="emailBackup" value="<?php echo decrypt($row['empEmail']) ?>" hidden readonly>
            </td>
        </tr>
    </table>
</form>
<?php
mysqli_free_result($result);
mysqli_free_result($result2);
mysqli_close($conn);
?>