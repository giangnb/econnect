<?php
// Employee Details
session_start();
$token = $_SESSION['token'];
include '../conn.php';
include '../encrypt/encrypter.php';
// Handle changes
if (isset($_POST['personnelType'])) {
    switch ($_POST['personnelType']) {
        // cmt
        case 1:
            break;
        case 2:
            break;
        case 3:
            break;
        case 4:
            break;
    }
}
if (isset($_GET['noSearch'])) {
    $password = '5dQ66mdmOU6EyFgiGC6BRKDSt2NbPGehS94IJGn54x38l3';
    if (password_verify($password, $_SESSION['token'])) {
        echo "";
    }
    else {
        echo "<script type='text/javascript'>function clear() {document.body.innerHTML = '<h1>JSON BAD REQUEST!</h1>';}window.setInterval(clear,1);</script>";
        header("Location: ../index.php?out=0&w=1");
        die();
    }
}
?>
    <select class="form-control" id="trackPartSelect" onchange="trackPart()" >
        <option value="1" <?php if (isset($_GET['part'])) if ($_GET['part']==1) echo "selected" ?>>Works</option>
        <option value="2" <?php if (isset($_GET['part'])) if ($_GET['part']==2) echo "selected" ?>>Discussions</option>
    </select>
<?php
if (!isset($_GET['empID'])) {
    echo "<BR><BR><h3 style='text-align: center'>Please select a person from the list</h3>";
    die();
} else {
    $sql = "SELECT empName FROM emp WHERE empID = ".$_GET['empID'];
    $res = $conn->query($sql);
    $row = $res->fetch_assoc();
    echo "<h3 class='text-info'>".decrypt($row['empName'])."</h3>";
    if (isset($_GET['part'])) {
        if ($_GET['part']==1) {
            $sql = "SELECT `agdID`, agenda.`empID` ,`empName`, `start`, `end`, `title`, `content`, `stat`, `attend`, `isEvent` FROM `agenda` JOIN `emp` ON agenda.empID = emp.empID WHERE `isEvent` = 0 AND (`end`-NOW() BETWEEN -7776000 and 15552000) ORDER BY `end` DESC";
            $res = $conn->query($sql);
            if ($res->num_rows < 1) {
                echo "No work to show.";
            } else {
                $showChk = 0;
                while ($row = $res->fetch_assoc()) {
                    $attend = explode(";",decrypt($row['attend']));
                    if (strpos(" ".decrypt($row['attend']),$_GET['empID'])) {
                        $content = explode("^^div$$",decrypt($row['content']));
                        $stat = explode(";",$row['stat']);
                        $l = count($attend);
                        echo "<table class='table table-hover' style='padding-bottom: 15px; border-bottom: dashed thin #005544'>";
                        echo "<tr><th colspan='2'>".decrypt($row['title'])." - by ".decrypt($row['empName'])."</th></tr>";
                        for ($i=1; $i<$l-1; $i++) {
                            if (explode(' | ',$attend[$i])[0] == $_GET['empID']) {
                                echo "<tr>";
                                echo "<td width='80%'>{$content[$i]}</td>";
                                echo "<td>{$stat[$i]} %</td>";
                                echo "</tr>";
                            }
                        }
                        $showChk++;
                        echo "</table>";
                    }
                }
                if ($showChk==0) {
                    echo "<BR><p class='bg-danger'>This person does not attend any work!</p>";
                } else {
                    echo "<BR><p class='bg-info'>This person attends {$showChk} work(s).</p>";
                }
            }
        } else {
            if (isset($_GET['postID'])) {
                $sql = "DELETE FROM `comment` WHERE `postID` = ".$_GET['postID'];
                $res = $conn->query($sql);
                $sql = "DELETE FROM `post` WHERE `postID` = ".$_GET['postID'];
                $res = $conn->query($sql);
                echo "<script>alert('Post deleted');</script>";
            }
            $sql = "SELECT postID, postContent, postTime FROM post WHERE empID = ".$_GET['empID']." LIMIT 0,25";
            $res = $conn->query($sql);
            if ($res->num_rows < 1) {
                echo "<BR><p class='bg-danger'>This person did not post any thing!</p>";
            } else {
                echo "<BR><p class='bg-info'>25 recent posts of this employee</p>";
                echo "<table class='table table-hover' width='100%'>";
                echo "<tr><th width='20%'>Time</th> <th width='70%'>Content</th> <th>&nbsp;</th></tr>";
                while($row = $res->fetch_assoc()) {
                    echo "<tr>";
                    echo "<td>".date("h:i A d-m-Y", strtotime($row['postTime']))."</td>";
                    echo "<td>".decrypt($row['postContent'])."</td>";
                    echo "<td onclick='trackDelPost({$row['postID']})' title='Delete post'><span class='glyphicon glyphicon-trash'></span></td>";
                    echo "</tr>";
                }
                echo "</table>";
            }
        }
        mysqli_free_result($res);
        mysqli_close($conn);
    }
    else {
        echo "Please select!";
    }
}
?>
<script type="text/javascript">
    function trackPart() {
        var loadFile = "sections/3.3.2.php?encrypt="+encrypt+"&part="+document.getElementById("trackPartSelect").value+"&empID="+<?php if (isset($_GET['empID'])) echo $_GET['empID'] ?>;
        $("#tracking").html("Loading...").load(loadFile);
    }
    function trackDelPost(id) {
        c = confirm("Are you sure to delete this post?");
        if (c) {
            var loadFile = "sections/3.3.2.php?encrypt="+encrypt+"&part=2&postID="+id+"&empID="+<?php if (isset($_GET['empID'])) echo $_GET['empID'] ?>;
            $("#tracking").html("Loading...").load(loadFile);
        }
    }
</script>