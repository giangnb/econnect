<?php
session_start();
$password = '5dQ66mdmOU6EyFgiGC6BRKDSt2NbPGehS94IJGn54x38l3';
if (password_verify($password, $_SESSION['token'])) {
    echo "";
}
else {
    echo "<script type='text/javascript'>function clear() {document.body.innerHTML = '<h1>JSON BAD REQUEST!</h1>';}window.setInterval(clear,1);</script>";
    header("Location: ../index.php?out=0&w=1");
    die();
}
$token = $_SESSION['token'];
include '../conn.php';
include '../encrypt/encrypter.php';
?>

<h1>Overview</h1>

<?php
$sql = "SELECT (SELECT COUNT(`empID`) FROM emp) as totalEmp,(SELECT COUNT(empID) FROM emp WHERE isActive=0) as inActive,(SELECT COUNT(empID) FROM emp WHERE isLogin=1) as isLogin,(SELECT COUNT(isUser) FROM permission WHERE isUser=1) as isUser,(SELECT COUNT(isAdmin) FROM permission WHERE isAdmin=1) as isAdmin,(SELECT COUNT(postID) FROM post) as totalPost,(SELECT COUNT(cmtID) FROM comment) as totalCmt,(SELECT ABS(SUM(logoutTime-loginTime)) FROM emp) as loginTime";
$r = $conn->query($sql);
$row = $r->fetch_assoc();
$totalEmp = $row['totalEmp'];
$inActive = $row['inActive'];
$isLogin = $row['isLogin'];
$isUser = $row['isUser'];
$isAdmin = $row['isAdmin'];
$totalPost = $row['totalPost'];
$totalCmt = $row['totalCmt'];
$loginTime = $row['loginTime'];

mysqli_free_result($r);

?>

<div class="jumbotron">
    <h2>Analytics</h2>
    <span class="row">
    <div class="col-md-4">
    	Total users: <span class="bold lead text-info"><?php echo $totalEmp ?></span><BR>
        <canvas id="chart-1"></canvas>
    </div>
    
    <div class="col-md-4">
    	Posts & Comments<BR>
        <canvas id="chart-2"></canvas>
    </div>
    
    <div class="col-md-4">
        Administration privilege:<BR>
        <canvas id="chart-3"></canvas>
    </div>
    </span>
    
    <span class="row">
    <div class="col-md-4">
        Avg comments per post:<BR><span class="bold lead text-info"><?php echo round($totalCmt/$totalPost,2); ?></span>
    </div>
    
    <div class="col-md-4">
    	Using time:<BR><span class="bold lead text-info"><?php echo round($loginTime/3600,2)." hour(s)"; ?></span>
    </div>
    
    <div class="col-md-4">
    	Avg using time per user:<BR><span class="bold lead text-info"><?php echo round(($loginTime/3600)/$totalEmp,2)." hour(s)"; ?></span>
    </div>
    
    </span>
    <div class="clear"></div>
    
</div>
				<script type="text/javascript" src="script/script.js"></script>
<script type="text/javascript" src="script/Chart.min.js"></script>
<script>

    var pieData1 = [
        {
            value: <?php echo $isLogin ?>,
            color:"#F7464A",
            highlight: "#FF5A5E",
            label: "Using now"
        },
        {
            value: <?php echo $totalEmp-$isLogin-$inActive; ?>,
            color: "#46BFBD",
            highlight: "#5AD3D1",
            label: "Not using"
        },
        {
            value: <?php echo $inActive ?>,
            color: "#FDB45C",
            highlight: "#FFC870",
            label: "Disabled"
        }
    ];
    var pieData2 = [
        {
            value: <?php echo $totalPost ?>,
            color: "#949FB1",
            highlight: "#A8B3C5",
            label: "Posts"
        },
        {
            value: <?php echo $totalCmt ?>,
            color: "#4D5360",
            highlight: "#616774",
            label: "Comments"
        }
    ];
    var pieData3 = [
        {
            value: <?php echo $totalEmp-$isUser ?>,
            color:"#801515",
            highlight: "#AA3939",
            label: "Admin"
        },
        {
            value: <?php echo $totalEmp-$isAdmin ?>,
            color: "#567714",
            highlight: "#7A9F35",
            label: "User"
        },
        {
            value: <?php echo $isAdmin+$isUser-$totalEmp ?>,
            color: "#0D4D4D",
            highlight: "#226666",
            label: "Both"
        }
    ];

    function showChart(){
        var ctx1 = document.getElementById("chart-1").getContext("2d");
        window.myPie = new Chart(ctx1).Pie(pieData1);
        var ctx2 = document.getElementById("chart-2").getContext("2d");
        window.myPie = new Chart(ctx2).Pie(pieData2);
        var ctx3 = document.getElementById("chart-3").getContext("2d");
        window.myPie = new Chart(ctx3).Pie(pieData3);
    }
    showChart();
</script>