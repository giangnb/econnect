<?php
session_start();
$password = '5dQ66mdmOU6EyFgiGC6BRKDSt2NbPGehS94IJGn54x38l3';
if (password_verify($password, $_SESSION['token'])) {
    echo "";
}
else {
    echo "<script type='text/javascript'>function clear() {document.body.innerHTML = '<h1>JSON BAD REQUEST!</h1>';}window.setInterval(clear,1);</script>";
    header("Location: ../index.php?out=0&w=1");
    die();
}
if (!$_SESSION['dashRight']) {
    echo "<center><h2 class=\"text-danger\">You don't have permission to access this panel!</h2></center>";
    die();
}
$token = $_SESSION['token'];
include '../conn.php';
include '../encrypt/encrypter.php';

$sql = "SELECT (SELECT `value` FROM setting WHERE prop='cleanTime') as cleanTime, (SELECT `value` FROM setting WHERE prop='sysStat')as sysStat;";
$r = $conn->query($sql);
$row = $r->fetch_assoc();
$cleanTime = $row['cleanTime'];
$sysStat = $row['sysStat'];

mysqli_free_result($r);

if (isset($_GET['system'])) {
    switch ($_GET['system']) {
        case "shut":
            $sql = "UPDATE `setting` SET `value` = '0' WHERE `prop` = 'sysStat';";
            break;
        case "start":
            $sql = "UPDATE `setting` SET `value` = '1' WHERE `prop` = 'sysStat';";
            break;
        case "clean":
            $time = $cleanTime*30*24*60*60;
            $sql = "SET @p0='{$time}'; CALL `sp_clean`(@p0);";
            break;
    }
    $r = $conn->query($sql);
    echo "<script>ajaxLoader(1.2);</script>";
}
?>

<h1>Quick control</h1>
<?php
function hide($permission) {
	if ($permission) {
		echo "inherit";
	} else {
		echo "none";
	}
}
?>
<div class="jumbotron">
	<h3 class="text-danger">
        <?php
        if(!$_SESSION['setRight'] && !$_SESSION['usrRight']) {
            echo "You don't have permission to access this panel.<BR>See \"Your permission panel\" for more information";
            die();
        }?>
    </h3>
	<span class="row" style="text-align:center;">
		<div class="col-md-8">
            Default clean time is <span class="bold lead text-info"><?php echo $cleanTime ?></span> month(s)<BR>
        	<p class="btn btn-primary btn-default btn-lg" style="display:<?php hide($_SESSION['setRight']); ?>;" onclick="cleanSys(<?php echo $cleanTime ?>)"> System clean-up </p>
        </div>
        <div class="col-md-4">
            System is currently <span class="bold lead text-danger">
            <?php
            if ($sysStat) {
                $stat = "WORKING";
            } else {
                $stat = "SUSPENDED";
            }
            echo $stat;
            ?></span><BR>
        	<p class="btn btn-info btn-danger btn-lg"  style="display:<?php hide($_SESSION['usrRight']); ?>;" onclick="shutdown('<?php echo $stat ?>')">
                <?php
                if ($stat=="WORKING") {
                    echo "SUSPEND ";
                } else {
                    echo "START ";
                }
                echo "system";
                ?>
            </p>
		
        </div>
    </span>
    <div class="clear"></div>
</div>
				<script type="text/javascript" src="script/script.js"></script>