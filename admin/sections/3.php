<?php
session_start();
$password = '5dQ66mdmOU6EyFgiGC6BRKDSt2NbPGehS94IJGn54x38l3';
if (password_verify($password, $_SESSION['token'])) {
    echo "";
}
else {
    echo "<script type='text/javascript'>function clear() {document.body.innerHTML = '<h1>JSON BAD REQUEST!</h1>';}window.setInterval(clear,1);</script>";
    header("Location: ../index.php?out=0&w=1");
    die();
}
$token = $_SESSION['token'];
include '../conn.php';
include '../encrypt/encrypter.php';
if (!$_SESSION['perRight']) {
    echo "<center><h2 class=\"text-danger\">You don't have permission to access this panel!</h2></center>";
    die();
}
$sql = "SELECT `empID`, `empName`, `isActive` FROM `emp`";
$res = $conn->query($sql);
$empName = array();
$empID = array();
$active = array();
if ($res->num_rows < 1) {
    echo "<h1>NO EMPLOYEES!</h1>";
} else {
    while ($row = $res->fetch_assoc()) {
        $empID[] = $row['empID'];
        $empName[] = " ".decrypt($row['empName']);
        $active[] = $row['isActive'];
    }
}
// Get branches
$sql = "SELECT branchID, branchName FROM branch";
$r = $conn->query($sql);
$branchID = array();
$branchName = array();
if ($r->num_rows > 0) {
    while ($row=$r->fetch_assoc()) {
        $branchID[] = $row['branchID'];
        $branchName[] = decrypt($row['branchName']);
    }
}
// Get departments
$sql = "SELECT deptID, deptName FROM department";
$r = $conn->query($sql);
$deptID = array();
$deptName = array();
if ($r->num_rows > 0) {
    while ($row=$r->fetch_assoc()) {
        $deptID[] = $row['deptID'];
        $deptName[] = decrypt($row['deptName']);
    }
}
// Get positions
$sql = "SELECT posID, posName FROM `position`";
$r = $conn->query($sql);
$posID = array();
$posName = array();
if ($r->num_rows > 0) {
    while ($row=$r->fetch_assoc()) {
        $posID[] = $row['posID'];
        $posName[] = decrypt($row['posName']);
    }
}
?>

<h1>Employees management</h1>
<div class="row">
    <section class="col-md-4" id="name-pane">
        <input type="text" id="peopleSearch" name="peopleSearch" placeholder="Search" onchange="getPeopleSearch()"/>
        <span class="glyphicon glyphicon-list" id="show-alphabet-popup"></span>
        <section class="scroll" id="people-name-list">
            <?php include '3.1.php'; ?>
        </section>
    </section>
    <section class="col-md-5" style="z-index: -1">&nbsp;</section>
    <section class="col-md-6" id="personnel-form">
        <form action="upload.php" method="post" enctype="multipart/form-data">
            <table width="100%" border="0" class="table table-hover">
                <tr>
                    <td width="25%">
                        <span class="bold lead text-info">Add Employee</span>
                    </td>
                    <td>
                        &nbsp;
                        <!--Import employees from file:<input type="file" class="form-control" value="Import employees from file" id="empImport" onchange="importFileBtn()">
                        <input type="submit" value=" Import data " id="empImportBtn" class="btn btn-primary">-->
                    </td>
                </tr>
                <tr>
                    <th width="25%">Name *</th>
                    <td id=""><input type="text" name="empName" id="empName" class="form-control"> </td>
                </tr>
                <tr>
                    <th width="25%">Employee No. *</th>
                    <td><input type="text" name="empNo" id="empNo" class="form-control"> </td>
                </tr>
                <tr>
                    <th width="25%">Email *</th>
                    <td id="peopleEmail"><input type="email" name="empEmail" id="empEmail" class="form-control"></td>
                </tr>
                <tr>
                    <th width="25%">Gender *</th>
                    <td><select class="form-control" id="empGen">
                            <option value="1">Male</option>
                            <option value="2">Female</option>
                            <option value="3">Other</option>
                        </select></td>
                </tr>
                <tr>
                    <th width="25%">Department *</th>
                    <td id="peopleDepartment">
                        <select name="empDept" id="empDept" class="form-control">
                            <option value="0">-- Select --</option>
                            <?php
                            if (count($deptID)!=0) {
                                for($i=0; $i<count($deptID); $i++) {
                                    echo '<option value="'.$deptID[$i].'">'.$deptName[$i].'</option>';
                                }
                            }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th width="25%">Branch *</th>
                    <td id="peopleBranch">
                        <select name="empBranch" id="empBranch" class="form-control">
                            <option value="0">-- Select --</option>
                            <?php
                            if (count($branchID)!=0) {
                                for($i=0; $i<count($branchID); $i++) {
                                    echo '<option value="'.$branchID[$i].'">'.$branchName[$i].'</option>';
                                }
                            }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th width="25%">Position *</th>
                    <td id="peoplePosition">
                        <select name="empPos" id="empPos" class="form-control" multiple>
                            <?php
                            if (count($posID)!=0) {
                                for($i=0; $i<count($posID); $i++) {
                                    echo '<option value="'.$posID[$i].'">'.$posName[$i].'</option>';
                                }
                            }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th width="25%">Status *</th>
                    <td style="font-weight: bold">
                        <label class="checkbox-inline"><input type="checkbox" name="isActive" id="isActive">Actived</label><BR>
                        <label class="checkbox-inline"><input type="checkbox" name="isUser" id="isUser" > User</label>
                        <label class="checkbox-inline"><input type="checkbox" name="isAdmin" id="isAdmin" > Administrator</label>
                    </td>
                </tr>
                <tr>
                    <th width="25%">Login mode *</th>
                    <td><select id="setupStat" class="form-control">
                        <option value="0">Normal</option>
                        <option value="1" selected>Change password + passcode</option>
                        <option value="2">Reset password</option>
                        <option value="3">Change password</option>
                    </select></td>
                </tr>
                <tr>
                    <th width="25%">Birth date *</th>
                    <td>
                        <input type="text" name="empDOB" id="empDOB" class="form-control" placeholder="YYYY-MM-DD">
                    </td>
                </tr>
                <tr>
                    <th width="25%">ID Card</th>
                    <td><input type="text" name="empIDcard" id="empIDcard" class="form-control"> </td>
                </tr>
                <tr>
                    <th width="25%">Telephone</th>
                    <td id="peopleTel"><input type="text" name="empPhone" id="empPhone" class="form-control"></td>
                </tr>
                <tr>
                    <th width="25%">Mobile</th>
                    <td id="peopleMob"><input type="text" name="empMob" id="empMob" class="form-control"></td>
                </tr>
                <tr>
                    <th width="25%">Skype</th>
                    <td id="peopleSkype"><input type="text" name="empSkype" id="empSkype" class="form-control"></td>
                </tr>
                <tr>
                    <th width="25%">Backup email</th>
                    <td id=""><input type="text" name="empBackupEmail" id="empBackupEmail" class="form-control"></td>
                </tr>
                <tr>
                    <td>
                         &nbsp;&nbsp;&nbsp;
                    </td>
                    <td>
                        <input type="button" class="btn-primary btn-lg" id="submitBtn" onclick="addEmp()" value="  Save  ">
                        <!--input type="submit" class="btn-danger" value="Remove" style="float: right;"-->
                    </td>
                </tr>
            </table>
        </form>
    </section>
</div>

				<script type="text/javascript" src="script/script.js"></script>
				<script type="text/javascript" src="script/peopleList.js"></script>