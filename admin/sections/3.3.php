<?php
session_start();
$password = '5dQ66mdmOU6EyFgiGC6BRKDSt2NbPGehS94IJGn54x38l3';
if (password_verify($password, $_SESSION['token'])) {
    echo "";
}
else {
    echo "<script type='text/javascript'>function clear() {document.body.innerHTML = '<h1>JSON BAD REQUEST!</h1>';}window.setInterval(clear,1);</script>";
    header("Location: ../index.php?out=0&w=1");
    die();
}
$token = $_SESSION['token'];
include '../conn.php';
include '../encrypt/encrypter.php';
if (!$_SESSION['perRight']) {
    echo "<center><h2 class=\"text-danger\">You don't have permission to access this panel!</h2></center>";
    die();
}
$sql = "SELECT `empID`, `empName`, `isActive` FROM `emp`";
$res = $conn->query($sql);
$empName = array();
$empID = array();
$active = array();
if ($res->num_rows < 1) {
    echo "<h1>NO EMPLOYEES!</h1>";
} else {
    while ($row = $res->fetch_assoc()) {
        $empID[] = $row['empID'];
        $empName[] = " ".decrypt($row['empName']);
        $active[] = $row['isActive'];
    }
}
?>

<h1>Employees tracking</h1>
<div class="row">
    <section class="col-md-4" id="name-pane">
        <input type="text" id="peopleSearch" name="peopleSearch" placeholder="Search"/>
        <span class="glyphicon glyphicon-list" id="show-alphabet-popup"></span>
        <section class="scroll" id="people-name-list">
            <?php include '3.3.1.php'; ?>
        </section>
    </section>
    <section class="col-md-5" style="z-index: -1">&nbsp;</section>
    <section class="col-md-6" id="tracking">
        <BR>
        <section>
            <h3>Select an employee</h3>
        </section>
    </section>
</div>

<script type="text/javascript" src="script/script.js"></script>
<script type="text/javascript" src="script/peopleList.js"></script>