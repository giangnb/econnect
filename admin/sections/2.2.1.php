<?php
/*session_start();
$password = '5dQ66mdmOU6EyFgiGC6BRKDSt2NbPGehS94IJGn54x38l3';
if (isset($_SESSION['token'])) {
    if (password_verify($password, $_SESSION['token'])) {
        echo "";
        $token = $_SESSION['token'];
    }
    else {
        echo "<script type='text/javascript'>function clear() {document.body.innerHTML = '<h1>JSON BAD REQUEST!</h1>';}window.setInterval(clear,1);</script>";
        header("Location: ../index.php?out=0&w=1");
        die();
    }
}
if (isset($_GET['encrypt'])) {
    if (password_verify($password, $_GET['encrypt'])) {
        echo "";
        $token = $_GET['encrypt'];
        include '../conn.php';
        include '../encrypt/encrypter.php';
    }
    else {
        echo "<script type='text/javascript'>function clear() {document.body.innerHTML = '<h1>JSON BAD REQUEST!</h1>';}window.setInterval(clear,1);</script>";
        header("Location: ../index.php?out=0&w=1");
        die();
    }
}
if (!isset($_SESSION['token']) && !isset($_GET['encrypt'])) {
    header("Location: ../index.php?out=0&w=1");
    die();
}
if (!$_SESSION['genRight']) {
    echo "<center><h2 class=\"text-danger\">You don't have permission to access this panel!</h2></center>";
    die();
}
*/
if (!isset($_POST['id'])) {
    die('<BR><BR><BR><h4 style="text-align: center">Please select a request</h4>');
} else {
    session_start();
    $token = $_SESSION['token'];
    include '../conn.php';
    include '../encrypt/encrypter.php';
    if (isset($_POST['resolved'])) {
        $sql = "UPDATE request SET reqStat = 0 WHERE reqID = ".$_POST['id'];
        $r = $conn->query($sql);
        if (isset($_POST['response'])) {
            $headers = 'From: admin@giangnb.com' . "\r\n" . 'Reply-To: '.$_SESSION['usr'] . "\r\n" . 'X-Mailer: PHP/' . phpversion();
            mail($_POST['empEmail'],"ECONNECT NOTIFICATION - Request response [ID:".$_POST['id']."]",str_replace("<br />","\n",$_POST['response']),$headers);
        }
        die('<BR><BR><h4 style="text-align: center">Request resolved<BR>Please select another request</h4>');
    }
    $sql = "SELECT DISTINCT reqID, empName, empEmail, branchName, deptName, reqDesc, reqContent, reqStat FROM request JOIN reqtype ON reqtype.reqTypeID = request.reqTypeID JOIN emp ON emp.empID = request.empID JOIN branch ON branch.branchID = emp.branchID JOIN department ON department.deptID = emp.deptID WHERE reqID = ".$_POST['id'];
    $r = $conn->query($sql);
    if ($r->num_rows < 1) {
        die("<h3>Error: Request not found!</h3>");
    } else {
        $row = $r->fetch_assoc();
    }
}
?>
        <h3>Request content</h3>
        <table class="table table-hover" width="100%">
            <tr>
                <td>
                    Employee: <b><?php echo decrypt($row['empName']); ?></b><BR>
                    Department: <b><?php echo decrypt($row['deptName']); ?></b><BR>
                    Branch: <b><?php echo decrypt($row['branchName']); ?></b>
                </td>
                <td>
                    Request type:<BR>
                    <b><?php echo decrypt($row['reqDesc']); ?></b>
                </td>
            </tr>

            <tr>
                <td colspan="2"><label>Request Content</label>
                    <textarea class="form-control" rows="3" readonly><?php echo str_replace("<br />", "\n",decrypt($row['reqContent'])); ?></textarea>
                    <input type="hidden" id="originalContent" value="<?php echo "<BR><BR><BR>========================================<BR>Your request content:<BR>".decrypt($row['reqContent']) ?>" readonly>
                </td>
            </tr>

            <tr>
                <td colspan="2"><label>Response</label>
                    <textarea rows="4" class="form-control" id="requestResponseContent" placeholder="Write your response to <?php echo decrypt($row['empName'])."\n"; ?>(Optional)"></textarea>
                    <input type="hidden" id="reqEmpEmail" value="<?php echo decrypt($row['empEmail']); ?>">
                </td>
            </tr>

            <tr>
                <td colspan="2">
                    <button type="button" class="btn btn-primary" id="btnRequestResponse" onclick="requestResponse(<?php echo $row['reqID']; ?>)"> Send response </button>
                    <button type="button" title="Without sending response to user" class="btn btn-warning" id="btnReqResolved" onclick="requestResolved(<?php echo $row['reqID']; ?>)"> Mark as resolved </button>
                </td>
            </tr>
				<script type="text/javascript" src="script/script.js"></script>