<?php
// Employee Details
session_start();
$token = $_SESSION['token'];
include '../conn.php';
include '../encrypt/encrypter.php';
if (!$_SESSION['usrRight']) {
    echo "<center><h2 class=\"text-danger\">You don't have permission to access this panel!</h2></center>";
    die();
}
// Handle changes
if (isset($_POST['empID'])) {
    $abort = 0;
    if ($_POST['master']==0) {
        $sql = "UPDATE permission SET dashboard='".$_POST['dashboard']."', general='".$_POST['general']."', personnel='".$_POST['personnel']."', setting='".$_POST['setting']."', master='".$_POST['master']."' WHERE empID = ".$_POST['empID'];
        $r = $conn->query($sql);
        $sql = "SELECT COUNT(empID) as counting FROM permission WHERE master = 1";
        $r = $conn->query($sql);
        $row = $r->fetch_assoc();
        if ($row['counting']==0) {
            echo "<BR><BR><p class='bg-danger' style='font-weight: bold; padding: 10px;'>There's must be at least 1 person have 'Manage Privilege' permission in order to manage the system!<BR></p>";
            $sql = "UPDATE permission SET master='1' WHERE empID = ".$_POST['empID'];
            $r = $conn->query($sql);
			$sql = "UPDATE permission SET setting='1' WHERE empID = ".$_POST['empID'];
            $r = $conn->query($sql);
        }
    } else {
        $sql = "UPDATE permission SET dashboard='".$_POST['dashboard']."', general='".$_POST['general']."', personnel='".$_POST['personnel']."', setting='".$_POST['setting']."', master='".$_POST['master']."' WHERE empID = ".$_POST['empID'];
        $r = $conn->query($sql);
    }
}

if (!isset($_GET['empID'])) {
    echo "<BR><BR><h3 style='text-align: center'>Please select a person from the list</h3>";
    die();
} else {
    $sql = "SELECT dashboard, general, personnel, setting, master, emp.empName FROM permission JOIN emp ON permission.empID = emp.empID WHERE permission.empID = ".$_GET['empID'];
    $r = $conn->query($sql);
    if ($r->num_rows < 1) {
        echo "<b>ERROR!</b>";
        die();
    } else {
        $row = $r->fetch_assoc();
    }
}

?>

<table class="table table-hover">
    <tr>
        <th width="30%">Name</th>
        <td>
            <b><?php echo decrypt($row['empName']) ?></b>
        </td>
    </tr>
    <tr>
        <th width="30%">Dashboard</th>
        <td>
            <input type="checkbox" name="dashboard" id="dashboard" <?php if ($row['dashboard']==1) echo "checked" ?> onclick="checkPrivilege()">
        </td>
    </tr>
    <tr>
        <th width="30%">General settings</th>
        <td>
            <input type="checkbox" name="general" id="general" <?php if ($row['general']==1) echo "checked" ?> onclick="checkPrivilege()">
        </td>
    </tr>
    <tr>
        <th width="30%">Personnel manager</th>
        <td>
            <input type="checkbox" name="personnel" id="personnel" <?php if ($row['personnel']==1) echo "checked" ?> onclick="checkPrivilege()">
        </td>
    </tr>
    <tr>
        <th width="30%">System settings</th>
        <td>
            <input type="checkbox" name="setting" id="setting" <?php if ($row['setting']==1) echo "checked" ?> onclick="checkPrivilege()">
        </td>
    </tr>
    <tr>
        <th width="30%">Manage privilege</th>
        <td>
            <input type="checkbox" name="master" id="master" <?php if ($row['master']==1) echo "checked"; if ($row['setting']==0) echo "disabled"; ?> onclick="checkPrivilege()">
        </td>
    </tr>
    <?php
    if ($row['dashboard']==0 && $row['general']==0 && $row['personnel']==0 && $row['setting']==0) {
        echo "<tr><td colspan='2' class='bg-danger'>This user can only view the analytics.</td></tr>";
    }
    ?>
    <tr>
        <th width="30%">&nbsp;</th>
        <td>
            <input type="button" id="submitBtn" onclick="savePrivilege(<?php echo $_GET['empID'] ?>)" class="btn btn-primary btn-lg" value=" Save ">
        </td>
    </tr>
</table>

<?php
mysqli_free_result($r);
mysqli_close($conn);
?>