<?php
session_start();
$password = '5dQ66mdmOU6EyFgiGC6BRKDSt2NbPGehS94IJGn54x38l3';
if (password_verify($password, $_SESSION['token'])) {
    echo "";
}
else {
    echo "<script type='text/javascript'>function clear() {document.body.innerHTML = '<h1>JSON BAD REQUEST!</h1>';}window.setInterval(clear,1);</script>";
    header("Location: ../index.php?out=0&w=1");
    die();
}
$token = $_SESSION['token'];
include '../conn.php';
include '../encrypt/encrypter.php';
if (isset($_GET['p'])) {
    switch($_GET['p']) {
        /* ! NOTICE !
         * p = 1:Department | 2:Branch | 3:Position
         * addNew: Add new item to the list of p
         * edit: Edit item
         * delete: Delete item (for Branch only)
         * */
        case 1:
            echo "<h3 style='text-align: center'>Department Management</h3>";
            if (isset($_POST['addNew'])) {
                $sql = "INSERT INTO department(deptID, deptName) VALUES (NULL, '".encrypt($_POST['name'])."');";
                $r = $conn->query($sql);
                unset($sql);
            }
            if (isset($_POST['id'])) {
                if (isset($_POST['edit'])) {
                    $sql = "UPDATE department SET deptName = '".encrypt($_POST['name'])."' WHERE deptID = '".$_POST['id']."';";
                    $r = $conn->query($sql);
                }
                $sql = "SELECT deptName as 'name' FROM department WHERE deptID = ".$_POST['id'];
            }
            break;
        case 2:
            echo "<h3 style='text-align: center'>Branch Management</h3>";
            if (isset($_POST['addNew'])) {
                $sql = "INSERT INTO branch(branchID, branchName, branchAdd) VALUES (NULL, '".encrypt($_POST['name'])."', '".encrypt($_POST['add'])."');";
                $r = $conn->query($sql);
                unset($sql);
            }
            if (isset($_POST['id'])) {
                if (isset($_POST['edit'])) {
                    $sql = "UPDATE branch SET branchName = '".encrypt($_POST['name'])."', branchAdd = '".encrypt($_POST['add'])."' WHERE branchID = '".$_POST['id']."';";
                    $r = $conn->query($sql);
                }
                if (isset($_POST['delete'])) {
                    $sql = "UPDATE emp SET branchID = ".$_POST['moveTo'].", isActive = ".$_POST['isActive']." WHERE branchID = ".$_POST['id']." AND isActive = 1";
                    $r = $conn->query($sql);
                    $sql = "UPDATE emp SET branchID = ".$_POST['moveTo']." WHERE branchID = ".$_POST['id']." AND isActive = 0";
                    $r2 = $conn->query($sql);
                    $sql = "DELETE FROM agenda WHERE branchID = ".$_POST['id'];
                    $r3 = $conn->query($sql);
                    $sql = "DELETE FROM branch WHERE branchID = ".$_POST['id'];
                    $r4 = $conn->query($sql);
                }
                $sql = "SELECT branchName as 'name', branchAdd FROM branch WHERE branchID = ".$_POST['id'];
            }
            break;
        case 3:
            echo "<h3 style='text-align: center'>Position Management</h3>";
            if (isset($_POST['addNew'])) {
                $sql = "INSERT INTO `position`(posID, posName) VALUES (NULL, '".encrypt($_POST['name'])."');";
                $r = $conn->query($sql);
                unset($sql);
            }
            if (isset($_POST['id'])) {
                if (isset($_POST['edit'])) {
                    $sql = "UPDATE `position` SET posName = '".encrypt($_POST['name'])."' WHERE posID = '".$_POST['id']."';";
                    $r = $conn->query($sql);
                }
                $sql = "SELECT posName as 'name' FROM `position` WHERE posID = ".$_POST['id'];
            }
            break;
    }
    if (isset($sql)) {
        $r = $conn->query($sql);
        if ($r->num_rows < 1) {
            die("<h1>Error: Invalid ID!</h1>");
        } else {
            $row = $r->fetch_assoc();
        }
    }
} else {
    echo "<h3>Please select a section</h3>";
    die();
}
?>
<section>
        <table class="table table-hover">
            <?php
            if (isset($_POST['id'])) {
                echo '<tr><td colspan="2"><button type="button" class="btn btn-default" name="btn-Add" id="btn-Add" onclick="loadGeneralForm('.explode("?",$_GET['p'])[0].')"> Add new </button></td></tr>';
            }
            ?>
            <tr>
                <td><label class="control-label">Name</label></td>
                <td>
                    <input type="text" name="generalName" id="generalName" class="form-control" value="<?php if (isset($row)) echo decrypt($row['name']); ?>">
                </td>
            </tr>

            <?php
            if ($_GET['p']==2) {
                echo "<tr>";
                echo '<td><label class="control-label">Address</label></td>';
                echo '<td><input type="text" name="branchAdd" id="branchAdd" class="form-control" value="';
                if (isset($row)) echo decrypt($row['branchAdd']);
                echo'"></td>';
                echo "</tr>";
            } else {
                echo '<input type="hidden" name="branchAdd" id="branchAdd" class="form-control" value=" ">';
            } ?>

            <tr>
                <td> </td>
                <td>
                    <?php
                    if (isset($_POST['id'])) {
                        echo '<button type="button" class="btn btn-primary" name="btn-Edit" id="btn-general-edit" onclick="generalEdit('.$_POST['id'].','.explode("?",$_GET['p'])[0].')"> Save changes </button>';
                    } else {
                        echo '<button type="button" class="btn btn-primary" name="btn-Add" id="btn-general-add" onclick="generalAdd('.explode("?",$_GET['p'])[0].')"> Add new </button>';
                    }
                    ?>
                </td>
            </tr>
            <?php
            // Show delete form if user is editing branch
            if ($_GET['p']==2) {
                if (isset($_POST['id'])) {
                    echo '<tr><td colspan="2">&nbsp;</td></tr>';
                    echo '<tr><th colspan="2" style="text-align: center">Delete branch</th></tr>';
                    echo "<tr>";
                    echo '<td><label class="control-label">Move employees</label></td>';
                    echo '<td>Move employees to branch:<select id="moveToBranch" class="form-control">';
                    $q = "SELECT branchID, branchName FROM branch WHERE branchID != ".$_POST['id'];
                    $r = $conn->query($q);
                    if ($r->num_rows < 1) {
                        echo "Cannot find other branches!";
                    } else {
                        echo "<option value='0'>-- Please choose --</option>";
                        while ($row=$r->fetch_assoc()) {
                            echo "<option value='".$row['branchID']."'>".decrypt($row['branchName'])."</option>";
                        }
                    }
                    echo '</select></td>';
                    echo "</tr>";
                    echo "<tr>";
                    echo '<td><label class="control-label">Additional</label></td>';
                    echo '<td><label><input type="checkbox" id="disableEmp"> Also disable these accounts</label></td>';
                    echo "</tr>";
                    echo '<tr><td>&nbsp;</td><td><input type="button" id="btnBranchDel" onclick="delBranch('.$_POST['id'].')" value=" Remove Branch " class="btn btn-danger"></td></tr>';
                }
            }
            ?>
        </table>
</section>

<script type="text/javascript" src="script/script.js"></script>

<?php
if (isset($_POST['delete'])) {
    mysqli_free_result($r);
    mysqli_free_result($r2);
    mysqli_free_result($r3);
}
?>