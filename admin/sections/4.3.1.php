<?php
// Administrator List
if (!$_SESSION['usrRight']) {
    echo "<center><h2 class=\"text-danger\">You don't have permission to access this panel!</h2></center>";
    die();
}
    $sql = "SELECT emp.`empID`, `empName`, `isActive` FROM `emp` JOIN `permission` ON emp.empID = permission.empID AND `isAdmin` = 1";
    $res = $conn->query($sql);

    if ($res->num_rows < 1) {
        echo "<h1>NO EMPLOYEES!</h1>";
    } else {
        echo "<ul>";
        while ($row = $res->fetch_assoc()) {
            echo "<li onclick='getAdminPrivilege(".$row['empID'].")' ";
            if ($row['isActive']==0) {
                echo "style='font-style:italic;color:#b9b9b9;' ";
            }
            echo ">".decrypt($row['empName'])."</li>";
        }
        echo "</ul>";
    }
mysqli_free_result($res);
mysqli_close($conn);
?>
