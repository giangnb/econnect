<?php
session_start();
$password = '5dQ66mdmOU6EyFgiGC6BRKDSt2NbPGehS94IJGn54x38l3';
if (password_verify($password, $_SESSION['token'])) {
    echo "";
}
else {
    echo "<script type='text/javascript'>function clear() {document.body.innerHTML = '<h1>JSON BAD REQUEST!</h1>';}window.setInterval(clear,1);</script>";
    header("Location: ../index.php?out=0&w=1");
    die();
}
if (!$_SESSION['genRight']) {
    echo "<center><h2 class=\"text-danger\">You don't have permission to access this panel!</h2></center>";
    die();
}
$token = $_SESSION['token'];
include '../conn.php';
include '../encrypt/encrypter.php';

// Update handler
if (isset($_POST['update'])) {
    $sql = "UPDATE setting SET `value`='".$_POST['post']."' WHERE prop='postPerPage';";
    $r = $conn->query($sql);
    $sql = "UPDATE setting SET `value`='".$_POST['pin']."' WHERE prop='pinPostLimit';";
    $r1 = $conn->query($sql);
}

// Load data
$sql = "SELECT (SELECT `value` FROM setting WHERE prop='postPerPage') as post, (SELECT `value` FROM setting WHERE prop='pinPostLimit')as pin";
$r = $conn->query($sql);
$res = $r->fetch_assoc();
?>

<h1>Discuss Settings</h1>
<table class="table table-hover">
    <tr>
        <th width="30%">Limit pinned post showing</th>
        <td><input type="number" class="form-control" id="pinPostLimit" value="<?php echo $res['pin'] ?>" min="1" max="10" step="1"></td>
    </tr>
    <tr>
        <th>Posts per page</th>
        <td><input type="number" class="form-control" id="postPerPageLimit" value="<?php echo $res['post'] ?>" min="3" max="60" step="1"></td>
    </tr>
    <tr>
        <th>&nbsp;</th>
        <td><input type="button" class="btn btn-primary" id="btnUpdateDiscuss" onclick="updateDiscussSetting()" value=" Save settings "></td>
    </tr>
</table>
				<script type="text/javascript" src="script/script.js"></script>
<?php
mysqli_free_result($r);
?>