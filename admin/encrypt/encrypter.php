<?php
include 'AES.php';
function encrypt($inp) {
    $inpKey = 'thudrab5wufu5eCR3kUzex3Spuphutet';
    $blockSize = 256;
    $aes = new AES($inp, $inpKey, $blockSize);
    $enc = $aes->encrypt();
    return $enc;
}
function encrypt_advanced($inp, $key, $block) {
    $aes = new AES($inp, $key, $block);
    $enc = $aes->encrypt();
    return $enc;
}
function decrypt($inp) {
    $inpKey = 'thudrab5wufu5eCR3kUzex3Spuphutet';
    $blockSize = 256;
    $aes = new AES($inp, $inpKey, $blockSize);
    $dec=$aes->decrypt();
    return $dec;
}
function decrypt_advanced($inp, $key, $block) {
    $aes = new AES($inp, $key, $block);
    $enc = $aes->decrypt();
    return $enc;
}
function password($usr,$pswd) {
    $count = rand(1,5);
    $hash = hash('whirlpool',$pswd.$usr);
    for ($i=0;$i<$count;$i++) {
        $hash = hash('whirlpool',$hash);
    }
    return $count.$hash;
}
function password_strong($usr,$pswd) {
    $count = rand(10,30);
    $hash = hash('whirlpool',$pswd.$usr);
    for ($i=0;$i<$count;$i++) {
        $hash = hash('whirlpool',$hash);
    }
    return $count.$hash;
}
function password_check($usr,$pswd, $hash) {
    $count = substr($hash,0,1);
    $enc = substr($hash,1);
    $temp = hash('whirlpool',$pswd.$usr);
    for ($i=0;$i<$count;$i++) {
        $temp = hash('whirlpool',$temp);
    }
    if ($enc==$temp) {
        return 1;
    } else {
        return 0;
    }
}
function password_strong_check($usr,$pswd, $hash) {
    $count = substr($hash,0,2);
    $enc = substr($hash,2);
    $temp = hash('whirlpool',$pswd.$usr);
    for ($i=0;$i<$count;$i++) {
        $temp = hash('whirlpool',$temp);
    }
    if ($enc==$temp) {
        return 1;
    } else {
        return 0;
    }
}
function password_generator($num) {
    $pass_chars = array('0','1','2','3','4','5','6','7','8','9','a','A','b','B','c','C','d','D','e','E','f','F','g','G','h','H','i','I','j','J','K','K','l','L','m','M','n','N','o','O','p','P','q','Q','r','R','s','S','t','T','u','U','v','V','x','X','y','Y','z','Z','-','_','+','=','!',')','@','(','#','*','$','&','%','^','.',',','?');
    $password_gen = "";
    for ($i=0; $i<$num; $i++) {
        $passchar_rand = rand(0,count($pass_chars)-1);
        $password_gen = $password_gen.$pass_chars[$passchar_rand];
    }
    return $password_gen;
}