<!doctype html>
<?php
session_start();

$token = $_SESSION['token'];
include 'conn.php';
include 'encrypt/encrypter.php';

if(!isset($_SESSION['auth'])){
    session_unset();
    session_destroy();
    header("Location: index.php?exp=1");
    die();
}

if (isset($_GET['logout']))
    if ($_GET['logout']==1) {
        $q = "UPDATE `emp` SET `logoutTime` = NOW(), `isLogin` = '0' WHERE `empEmail` = '".encrypt($_SESSION['usr'])."';";
        $result = $conn -> query($q);
        mysqli_free_result($result);
        mysqli_close($conn);
        session_unset();
        session_destroy();
        header("Location: index.php?out=1");
        die();
    }


if(isset($_SESSION['logged-in'])){
    if ($_SESSION['logged-in']==1 || $_SESSION['logged-in']==2) {
        $q = "UPDATE `emp` SET `isLogin` = '0' WHERE `empEmail` = '".encrypt($_SESSION['usr'])."';";
        $result = $conn -> query($q);
        mysqli_free_result($result);
        mysqli_close($conn);
        session_unset();
        session_destroy();
        header("Location: index.php?exp=1");
        die();
    }
}

// Check session: Logout after 30 mins of inactive
/*if (isset ($_SESSION['EXPIRES'])) {
    if ($_SESSION['EXPIRES'] < time()-3600 && $_SESSION['logged-in']!=3) {
        $q = "UPDATE `emp` SET `logoutTime` = NOW(), `isLogin` = '0' WHERE `empEmail` = '".encrypt($_SESSION['usr'])."';";
        $result = $conn -> query($q);
        session_unset();
        session_destroy();
        header("Location: index.php?exp=1");
        die();
    }
}

$_SESSION['logged-in'] = 1;*/

//Hide section(s) when permission is false
function hide($permission) {
	if ($permission) {
		echo "inherit";
	} else {
		echo "none";
	}
}
?>
<html>
<head>
	<script type="text/javascript">
	var app = angular.module('adminApp',[]);
	app.controller('control',['$scope', function($scope){
		$scope.usr = {
			loginCount: 100,
			totalUsr:150,
			usingAvg: 50,
			empCount: 120,
			manaCount: 20,
			adminCount: 10,
			inactiveCount: 2
		};
	}]);
	</script>
    <meta charset="utf-8">
    <title>eConnect Managements</title>
    
    <link rel="shortcut icon" href="img/icon.png">
    
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/scrollbar.css">
    <link rel="stylesheet" type="text/css" href="css/animate.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    
    <script type="text/javascript" src="script/jquery-2.1.3.min.js"></script>
    <script type="text/javascript" src="script/peopleList.js"></script>
    <script type="text/javascript" src="script/Chart.min.js"></script>
	<meta name="norton-safeweb-site-verification" content="08hhfhsna1qjjgqqo7ubg01xwohhyjhuf2jta8-ilq1v9sc3hnagl2g5qg39wljfxbujlsh9mdbuzm-rrfxy-ws7k1-epkv7331z7b8edqflpvm3yjrf1dwvhs9d72yh" />
</head>

<body>
	<?php 
    	$pre_encrypt ='roivTyeDGyFgx38l3hd34u9e87TG5tiu34yFGHetifscw984r43dmOU6EHHH3yehfv47eyhGrGRTtHuiHhTHfNRTFH3G84tey8iGC6BRKDSt2NbPGehS94IJGn54vy47FeyiuJ4F3weyutHt4et4FwF5dQ66mteYuVyD4HuDre';
		$encrypt = password_hash($pre_encrypt, PASSWORD_DEFAULT);
		echo "<script type=\"text/javascript\">var encrypt = \"{$encrypt}\";</script>";
    ?>
	<section id="wrapper">
        <aside id="side-1">
        
        	<ul>
            	<li id="nav-dashboard" class="current" style="display:<?php hide($_SESSION['dashRight']); ?>;"><h1 class="glyphicon glyphicon-home"></h1>Dashboard</li>
                <li id="nav-general" style="display:<?php hide($_SESSION['genRight']); ?>;"><h1 class="glyphicon glyphicon-list-alt"></h1>General</li>
                <li id="nav-personnel" style="display:<?php hide($_SESSION['perRight']); ?>;"><h1 class="glyphicon glyphicon-user"></h1> Personnel</li>
                <li id="nav-system" style="display:<?php hide($_SESSION['setRight']); ?>;"><h1 class="glyphicon glyphicon-cog"></h1><BR>System</li>
                <li id="nav-logout"><a href="admin.php?logout=1" style="color:#fff !important;"><h1 class="glyphicon glyphicon-off"></h1><BR>Log out</a></li>
            </ul>
        <div class="content"></div>
        </aside>
        
        <aside id="side-2" style="position: relative">
        <div id="menu">
        	<ul id="menu-1"><li id="1" class="current" onClick="ajaxLoader(1)" style="display:<?php hide($_SESSION['dashRight']); ?>;">Overview</li>
			<li id="1.2" onClick="ajaxLoader(1.2)" style="display:<?php hide($_SESSION['dashRight']); ?>;">Quick control</li>
			<li id="1.3" onClick="ajaxLoader(1.3)" style="display:<?php hide($_SESSION['dashRight']); ?>;">Your permission</li></ul>
            
            <ul id="menu-2"><li class="current" id="2" onClick="ajaxLoader(2)">Managements</li>
			<li id="2.2" onClick="ajaxLoader(2.2)">Requests</li>
			<li id="2.3" onClick="ajaxLoader(2.3)">Discuss settings</li>
			<!--li id="2.4" onClick="ajaxLoader(2.4)">Tasks settings</li-->
			</ul>
            
            <ul id="menu-3"><li class="current" id="3" onClick="ajaxLoader(3)">Employee management</li>
			<li id="3.3" onClick="ajaxLoader(3.3)">Tracking</li></ul>
            
            <ul id="menu-4"><li id="4" value="4" onClick="ajaxLoader(4)">System settings</li>
			<li id="4.2" value="4.2" onClick="ajaxLoader(4.2)">System clean-up</li>
            <li id="4.3" value="4.3" onClick="ajaxLoader(4.3)" style="display:<?php hide($_SESSION['usrRight']); ?>;">Managers</li>
            <li id="4.4" value="4.4" onClick="ajaxLoader(4.4)" style="display:<?php hide($_SESSION['usrRight']); ?>;">Database status</li></ul>
        </div>
            <p style="position: absolute; background: #082F48; padding-left: 15px; color:#fff; width: 100%; bottom: 25px;">
                <?php
                echo "<b>".$_SESSION['empName']."</b><BR><BR>Email: ".$_SESSION['usr']."<BR>ID: ".$_SESSION['empID'];
                ?>
            </p>
        </aside>
        
        <aside id="main">
            <div class="content" id="ajaxShow">
                <section style="text-align: center">
                    <h2>Welcome to ECONNECT Administrator Area</h2>
                    Please wait . . .
                </section>
            </div>
        </aside>
    </section>

    <div id="popup-bg"></div>

    <div id="people-popup" class="popup">
        <img src="img/exit-ico.png" class="close-popup"/>
        <span id="peopleListBtn"></span>
    </div>


    <script type="text/javascript" src="script/ajax.js">
    </script>
	<script type="text/javascript" src="script/script.js">
	</script>
    <script type="text/javascript" src="script/global.js">
	</script>
</body>
</html>

