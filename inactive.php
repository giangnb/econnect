<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>ECONNECT - Disabled</title>
    <link rel="shortcut icon" href="img/icon.png">
    <link rel="stylesheet" href="css/templock.css">
</head>
<body>
    <h2>
        <?php if(isset($_SESSION['companyName'])) { if(!empty($_SESSION['companyName'])) echo $_SESSION['companyName']; else echo "econnect"; }?>
        <BR>
        <?php
        if (isset($img))
            if (!empty($img))
                echo "<img src='{$img}' style='height: 72px' alt='Company Logo'>";
        session_unset();
        session_destroy();
        ?>
    </h2>

    <BR>
    <h1><span class="glyph">&#xe017;</span> SYSTEM DISABLED</h1>

    <p>
        System is temporary disabled now<BR>
        You cannot use eConnect until it enabled again<BR><BR>
        Contact System Administrator or IT Manager for more information
    </p>
</body>
</html>