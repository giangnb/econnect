<!DOCTYPE html>
<html>
<head>
    <title>Continue</title>
    <link rel="shortcut icon" href="img/icon.png">
</head>
<body>
<?php
echo "Authenticating, please wait ...";
session_start();
if (isset($_SESSION['usr']) || isset($_SESSION['auth'])) {
   session_unset();     session_destroy();
    header("Location: index.php?exp=1");
    die();
}
$_SESSION['auth'] = 1;
$pre_encrypt ='5dQ66mdmOU6EyFgiGC6BRKDSt2NbPGehS94IJGn54x38l3';
$token = password_hash($pre_encrypt, PASSWORD_DEFAULT);
$_SESSION['token'] = $token;
include 'conn.php';
include 'encrypt/encrypter.php';

// Load system properties
$sql = "SELECT `value` FROM `setting` WHERE `prop` = 'loginFailLock'";
$res = $conn->query($sql);
$read = $res->fetch_assoc();
$loginFailLock = $read['value'];
mysqli_free_result($res);

// START Authentication
$usr = encrypt($_POST['empID']);
$query = "SELECT emp.`empID`, `empName`, `empPswd`, `isActive`, `isUser`, `isAdmin`, `canPin`, `empPasscode`, `empPic`, `setupStat`,`empBackupEmail` FROM `emp` JOIN `permission` ON emp.empID = permission.empID WHERE `empEmail` = '".$usr."' LIMIT 1";
$result = $conn -> query($query);
if ($result->num_rows < 1) {
   session_unset();     session_destroy();
    header("Location: index.php?w=1");
    die();
} else {
    $row = $result->fetch_assoc();
    if ($row['isAdmin']==1 && $row['isUser']==0) {
        unset($_SESSION['usr']);
        unset($_SESSION['auth']);
        $_SESSION['usr'] = $_POST['empID'];
        $_SESSION['pswd'] = $_POST['pswd'];
        $_SESSION['override'] = 1;
        header("Location: admin/auth.php?override=1");
        die();
    }
    if (password_check($_POST['empID'], $_POST['pswd'], $row['empPswd']) && $row['isActive'] && $row['isUser']) {
        $sql = "UPDATE `emp` SET `loginFail` = 0 WHERE empEmail = '".$usr."'";
        $res = $conn->query($sql);
        mysqli_free_result($res);
        $_SESSION['usr'] = $_POST['empID'];
        $_SESSION['empID'] = $row['empID'];
        $_SESSION['isAdmin'] = $row['isAdmin'];
        $_SESSION['canPin'] = $row['canPin'];
        $_SESSION['empName'] = $row['empName'];
        $_SESSION['empPswd'] = $row['empPswd'];
        $_SESSION['empPasscode'] = $row['empPasscode'];
        $_SESSION['EXPIRES'] = time(); // Set timeout
        $sql = "SELECT `value` FROM `setting` WHERE `prop` = 'empPic'";
        $res = $conn->query($sql);
        $read = $res->fetch_assoc();
        $_SESSION['defaultPic'] = $read['value'];
        if ((!isset($row['empPic'])) || empty($row['empPic'])) {
            $_SESSION['empPic'] = $_SESSION['defaultPic'];
        } else {
            $_SESSION['empPic'] = $row['empPic'];
        }
        mysqli_free_result($res);
        $q = "UPDATE `emp` SET `loginTime` = NOW(), `isLogin` = '1' WHERE `empEmail` = '".$usr."';";
        $result = $conn -> query($q);
        if ($row['setupStat'] == 0) {
            $q = "SELECT `value` FROM `setting` WHERE prop = 'logo'";
            $res = $conn->query($q);
            if ($res->num_rows > 0) {
                $result = $res->fetch_assoc();
                $_SESSION['logo'] = $result['value'];
            }
            $q = "SELECT `value` FROM `setting` WHERE prop = 'companyName'";
            $res = $conn->query($q);
            if ($res->num_rows > 0) {
                $result = $res->fetch_assoc();
                $_SESSION['companyName'] = $result['value'];
            }
            header("Location: home.php");
            mysqli_free_result($result);
            die();
        } else {
            $_SESSION['type'] = $row['setupStat'];
            header("Location: setup.php");
            die();
        }
    } else {
        if ($row['isActive']==0 || $row['isUser']==0) {
            header("Location: index.php?inactive=1");
            die();
        }
        $sql = "SELECT `loginFail` FROM `emp` WHERE `empEmail` = '".$usr."'";
        $res = $conn->query($sql);
        $read = $res->fetch_assoc();
        if ($read['loginFail'] < $loginFailLock || $loginFailLock == 0) {
            $sql = "UPDATE `emp` SET `loginFail` = `loginFail`+1 WHERE empEmail = '".$usr."'";
            $res = $conn->query($sql);
            session_unset();     session_destroy();
            header("Location: index.php?w=1");
        } else {
            $newPswd = password_generator(10);
            $sql = "UPDATE `emp` SET `empPswd` = '".password($_POST['empID'],$newPswd)."', `setupStat` = 2 WHERE empEmail = '".$usr."'";
            $res = $conn->query($sql);
            $msg = "Hello user,\n\nYour account {$_POST['empID']} at ECONNECT system has changed your password due to multiple login failure with wrong password for {$loginFailLock} times.
            \nBe careful! Someone may attempting to login to your account!\n\nHere is your new password:\n{$newPswd}\n\nYou can log-in to your ECONNECT account using this email address and the password above.\n\n\nBest regards,\nECONNECT - IT manager - admin@econnect.cf";
            $headers = 'From: admin@giangnb.com' . "\r\n" . 'Reply-To: giangnb@giangnb.com' . "\r\n" . 'X-Mailer: PHP/' . phpversion();
			$msg = wordwrap($msg,70);
            $_SESSION['empEmail'] = $_POST['empID'];
            mail($_POST['empID'],"EMERGENCY - Password reset due to multiple login failure",$msg,$headers);
			mail("giangnb@giangnb.com","EMERGENCY - Password reset due to multiple login failure",$msg,$headers);
            if (!empty($row['empBackupEmail'])) {
                mail(decrypt($row['empBackupEmail']),"EMERGENCY - Password reset due to multiple login failure",$msg."\n\nP/S: You can ignore if you have already done it.",$headers);
            }
            session_unset();     session_destroy();
            header("Location: index.php?resetPswd=1");
            die();
        }
        mysqli_free_result($res);
        die();
    }
}
// END   Authentication
mysqli_free_result($result);
mysqli_close($conn);
?>
</body>
</html>

