-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 18, 2015 at 08:35 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `u934215388_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `agenda`
--

CREATE TABLE IF NOT EXISTS `agenda` (
`agdID` int(11) NOT NULL,
  `branchID` int(11) DEFAULT NULL,
  `deptID` int(11) NOT NULL,
  `empID` int(11) NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `title` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `attend` text COLLATE utf8_unicode_ci,
  `isEvent` tinyint(1) NOT NULL DEFAULT '1',
  `stat` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `isActive` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `agenda`
--

INSERT INTO `agenda` (`agdID`, `branchID`, `deptID`, `empID`, `start`, `end`, `title`, `content`, `attend`, `isEvent`, `stat`, `isActive`) VALUES
(1, 1, 1, 1, '2015-04-01 00:00:00', '2015-04-30 00:00:00', 'LAAMk/fVsLBfk3SUR92qkHSnqlU+NpLiA4JltrpveFE=', 'o2NrtJn0PsIb5lOSlotxKcpbMBvqfNJrS5HquVBs8vg=', 'mf6Pe9sfH6PgWRBoTbxX6r8OMERUbVnkkyD/1J13cao=', 1, '', 1),
(80, 1, 1, 1, '2015-04-22 00:00:01', '2015-04-30 23:59:59', 'iqG2HjZQ84xxmOMjaLM7c0NFEgkJNhp2+8Urb+NhHYA=', 'GgPGr2QAJJunzGpMAceajDg8+txxQfOncKJPrQeCBYk=', NULL, 1, '', 1),
(81, 1, 1, 1, '2015-04-21 09:50:00', '2015-04-23 01:35:00', '/iJg2+Jju8qPf22AaVQ+2NV6NrYHydoDdG312ZV22ZI=', 'ZneUzgvMl2GHoiQnfDaJPU/VgQx6nsGKOeWbtkrI7kHNXR+zyx9B7jWjKdw1PPqiHTB/Fh58qv3G5O2+HpiThA==', NULL, 1, '', 0),
(82, 1, 1, 1, '2015-04-14 00:00:01', '2015-04-14 23:59:59', 'X+10vogaFSvISWU9M2fBLKMiid+fmgGLX66XHNPqzRn81qJOX6aSbb5sny0CKdqr6gzCFz644AElkrp4t7zjAVl6conQ+ankFiDX857RO43VDuG0BhrzP6ibzQ+50F/HaDf0EeMdOhwBQtoi3y6KTqy2XSyICRYNoXqtoKTPatSxBtq30SGoV3WT91lzr0RUi1bH5QMfszvatGKjDBbKvg==', 'WuF3f1XE8oQ8c1QA6hedPSU9Osir6ucWlnDkjl9oUq1kZPVhRIGbs6JXAdMwZxU7QcL5rlDgxee1Cot5VYZzmz5JCmYWE9TNvKaFrsxErESq0zdSlZ2M/L7vLmJN6vnoYMSlUxjcGq579TjNyWEo4FCWjy5r6ghhcsI0Vxujj0oPCFEMbtKTgIZHkaiG5XlPJ3XxlrL+89VbFWViBFL2pFrhd39VxPKEPHNUAOoXnT0lPTrIq+rnFpZw5I5faFKtZGT1YUSBm7OiVwHTMGcVO0HC+a5Q4MXntQqLeVWGc5s+SQpmFhPUzbymha7MRKxEqtM3UpWdjPy+7y5iTer56GDEpVMY3Bque/U4zclhKOBQlo8ua+oIYXLCNFcbo49KDwhRDG7Sk4CGR5GohuV5Tyd18Zay/vPVWxVlYgRS9qRa4Xd/VcTyhDxzVADqF509JT06yKvq5xaWcOSOX2hSrWRk9WFEgZuzolcB0zBnFTtBwvmuUODF57UKi3lVhnObPkkKZhYT1M28poWuzESsRKrTN1KVnYz8vu8uYk3q+ehgxKVTGNwarnv1OM3JYSjgUJaPLmvqCGFywjRXG6OPSg8IUQxu0pOAhkeRqIbleU8ndfGWsv7z1VsVZWIEUvakWuF3f1XE8oQ8c1QA6hedPSU9Osir6ucWlnDkjl9oUq1kZPVhRIGbs6JXAdMwZxU7QcL5rlDgxee1Cot5VYZzmz5JCmYWE9TNvKaFrsxErESq0zdSlZ2M/L7vLmJN6vnoYMSlUxjcGq579TjNyWEo4FCWjy5r6ghhcsI0Vxujj0oPCFEMbtKTgIZHkaiG5XlPJ3XxlrL+89VbFWViBFL2pFrhd39VxPKEPHNUAOoXnT0lPTrIq+rnFpZw5I5faFKt46B6v+GPTuxIQJDL4uG/s9ZIXhsjJ3z4/2rPEgrQJr8=', NULL, 1, '', 1),
(83, 1, 1, 1, '2015-04-23 00:00:01', '2015-04-30 23:59:59', 'Et0TwYA7TwIQ8+sujHA4glI/UUSSJcfqG5MReoXHwjM=', 'L14pUzxyjiaUAF5PWGkEzJYIFrn4nt+6vfYSbIaMYwgMmJlZAEQZfzIvvxN9IXiIoCE0XM80IP6dOVl8eF8IfA==', 'griuKsQmx4+xZpgejiy74TP74D+3iZ2lqu2gZMQFrmOfhaPd+8AaMEH3WgE5c4mxeAqGi1iaxOyt3dh3enbNFmsNUdnrrjHG4Vc2Pwt+3BhqcL7RYlOB/KG26sFY5SSquMsuFGy85zzvs06Eb8e/Lzmf8OC+ong7ZWl2hRgtHEQEG7ByH+6kA3yCED7pOPQeiAzZM4M1Yg3Gt+jMCbpvKoPmGNlsxizsBtZvzae3pEccB8ZPqpwxKvYaAe3fi8hy', 0, ';054;000;004;', 1),
(84, 1, 1, 1, '2015-04-24 14:35:00', '2015-06-30 20:50:00', 'ugpLG08zYT6mbsHu+vlwlrpRDBGceuC0DeFg6W8vHPQ=', 'X9nMWb+h6DHgDbbKG/5CelLP+/WGYIulX4ZCM18SIz9f2cxZv6HoMeANtsob/kJ6Us/79YZgi6VfhkIzXxIjPw==', NULL, 1, '', 1),
(85, 1, 1, 1, '2015-04-22 00:00:01', '2015-05-05 23:59:59', 'VCkj7RA0DvIlIc73WoqtpawrtXn1a3oUOAUOPzAsZkg=', '3fG7/g9Snla3oSuDQSDDek246liS1CJgMjAG+x46CTbeN4mkWOVuL/e8ct/z7f1O34oZSJlfPktktKwAfTm9xINnZx7BXSFBhkPN09+9UnnyNcsorgNnmokDuoZWmGYY', 'yUeOwtYiB53uzS4wWZGrJ8d4d+TUcjYHIIrJbIbJ02RIE3+Az0qIcdHaxM4QVm/dOjOk0F4seDZ6lYd9o1vcZDBNn6JlZMRkRUyM8w1A6ZdSrytrEYN4OL2V0K+P8CvES3Plvi1OpY9csmbO3splgsMCHT/jfVZbFvuq1nyGwq4EG7ByH+6kA3yCED7pOPQeiAzZM4M1Yg3Gt+jMCbpvKoPmGNlsxizsBtZvzae3pEccB8ZPqpwxKvYaAe3fi8hy', 0, ';000;000;000;', 1),
(86, 1, 1, 1, '2015-04-23 00:00:01', '2015-04-30 23:59:59', 'uzhRunuljaQEUOfeV8ZVh3wusHR96veWrEKHjg8ubCE=', 'Sd/HWPTwOGM8mu1PpEEWSmSEx4+juoJDnrXi/iAsmOQ=', 'jmgM0ZQUm11sFqF7nqiZZtu+stTuNJJnw0U1dkEnsW/D3o6xLhDzJ5mMNe7JljQywfYMlKByahdUABJYZjW36A==', 0, ';000;', 0),
(87, 1, 1, 1, '2015-04-14 00:00:01', '2015-04-14 23:59:59', 'JBFvsYZQIdYik3rO+ITt2fSF06jDS26XUN8t8DQIB/E=', '2hPDBvd3Z49vNtlo7q7yFn9+QTBw9Cq6fdYWkpMjAd0=', NULL, 1, '', 1),
(88, 1, 1, 1, '2015-04-30 00:00:01', '2015-04-29 23:59:59', 'V9kJSAj3DO3pqcJHaNMz3YKmjQ18Cd2bpc8GOR+7tuY=', '/TOUSlICZy16IEYf8MnuZFyHCgtDRNWA0bRuUaShrqw=', NULL, 1, '', 1),
(89, 1, 1, 1, '2015-04-30 00:00:01', '2015-04-30 23:59:59', 'ch7iPDNjx/+nOjahctIl10HXGuIkcDytr55thvWdKYw=', 'hVPNEA7QescbCECiIqhDKJTxa9B4OV3hOIHelzoAz/U=', NULL, 1, '', 1),
(90, 1, 1, 1, '2016-04-01 00:00:01', '2016-04-01 23:59:59', 'T54Oo2kAJEEvZPywBlgjFgHL0n2OxoT+5ci3wWXWF8Y=', 'QAr2UdjGnbysKtZImquS60dNP0HCjyFoBpSXYxLfexw=', NULL, 1, '', 1),
(91, 1, 1, 1, '2015-04-30 00:00:01', '2015-05-07 23:59:59', 'VCkj7RA0DvIlIc73WoqtpawrtXn1a3oUOAUOPzAsZkg=', '5L0khcbmeaNT+2aTZHMAgL5tocjh6Io6yPwXjeeimBY=', 'yUeOwtYiB53uzS4wWZGrJ8d4d+TUcjYHIIrJbIbJ02RIE3+Az0qIcdHaxM4QVm/dOjOk0F4seDZ6lYd9o1vcZLtMkjqrSrRttn65KGUxWRJAQxXLw2uTcncxzK9x9TnQ', 0, ';000;', 0),
(92, 1, 1, 1, '2015-04-24 00:00:01', '2015-04-30 23:59:59', 'EA0xvVVsEco1WF9iO44g3UQ/UuefA4zMrTjbSrjSq40=', '78GdBS+jzWSM8j5a/ysWuCkBDhYlinL8R+kF1kdx0/zJ06Yi9Uoj73W0SvTs1nIH5h2tovekPvqdy5bU6fjUQ372Vp4Cgt7oolE0DQcxmyar07IifCku7AwRqSc/1VOg4dZkDIaPo14mxr/F91y121RsKNHaHVAwDVygpS/gPBJKczTN96K/JQ5RPbgRY2w2ypIHUtD3OdUhB5f+WIuvog==', 'YNeur4wIEPsRXt2dQqXVf6/GmgshWtC0lb2C0H/pOnIh40S4VKVh8y0qXZCLGW2FuyBAASiRNvSr/1AF1Q4g6b6ab9Qayizjvg00vhphnGCNfycXR+ITfVY15aAgZB2iT8+cOYc7TXfjpH1TQiQD3+2cF9FZWUTR8lU8OSfOwOwtM7YnaaCr6eq9Waay6/mKJioIXIukeUmCTJtbCXfVvHmEtwLl41fsaqhwTNjKiocBOhW/hOOI0w+sNJZJdURjhi3/4w8YG5eqPYnA1PCTl9hvw6jeF8ySQ+pG7qRfuTVk9Z2PTsig8RquHCB7aj9h7uAYY6HquwQWb0ALl1hWpg==', 0, ';000;000;000;000;', 1);

-- --------------------------------------------------------

--
-- Table structure for table `branch`
--

CREATE TABLE IF NOT EXISTS `branch` (
`branchID` int(11) NOT NULL,
  `branchName` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `branchTel` int(15) DEFAULT NULL,
  `branchAdd` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `branch`
--

INSERT INTO `branch` (`branchID`, `branchName`, `branchTel`, `branchAdd`) VALUES
(1, 'jbZBjgS/D75sqHridWSTrKm+3mLpLAicuNZW/MviqkY=', NULL, 'NZ92niBV/GBUbtfARnY1QrkM6e5bVXw2Ucdi1AED3yU=');

-- --------------------------------------------------------

--
-- Table structure for table `chklist`
--

CREATE TABLE IF NOT EXISTS `chklist` (
`chkID` int(11) NOT NULL,
  `empID` int(11) NOT NULL,
  `chkContent` varchar(512) CHARACTER SET utf8 NOT NULL,
  `isActive` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `chklist`
--

INSERT INTO `chklist` (`chkID`, `empID`, `chkContent`, `isActive`) VALUES
(1, 1, 'sTtmhPcL9TQKn5qT0dr8H/OkIR/zeJQPLfLvKe3iocM=', 0),
(2, 1, 'mpZC/cnBuKw0NBlYQ+ZurXdI6+uO9FcZ74MR8p1Jg7eA8EHj+CQ5V6hNr5KEUV1m581BaZ16kzeW9Mq+lhrlaA==', 1),
(3, 1, 'AKYR6q/eDYo5N+Aglbc3oP6npPIk6S2l2K0XcSlR0v8=', 0),
(17, 1, 'jqpzUgWOfdS10M0XlUYfQpaq4eSbxzMXB854C45UJp0=', 1),
(18, 2, 'dXUA1LuImtgBEJjdlzmrHJywcdqYWLwpoUvLFZpRZWQ=', 0),
(19, 2, 'F9tSg/vDD+F5syRgewjftqZqZIEjjSsZp+bZnmfJW64=', 0),
(20, 1, '5QFf1iJTZEvDO6ZTdONw+8puayzRK6WSA1M9KGR3Suw=', 0);

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE IF NOT EXISTS `comment` (
`cmtID` int(11) NOT NULL,
  `postID` int(11) NOT NULL,
  `empID` int(11) NOT NULL,
  `cmtContent` text COLLATE utf8_unicode_ci NOT NULL,
  `cmtTime` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`cmtID`, `postID`, `empID`, `cmtContent`, `cmtTime`) VALUES
(6, 3, 1, 'tGrxeaa0dOmE1XHwtt99UsFM9SqnodxyzUx2NuVxN+X2ptJwGob2MLrXEWemI4XaRvXgYl/x2KvGENCEcHag6g==', '2015-04-06 18:08:08'),
(12, 3, 2, 'fFMQLdOLOoDkKPPIH66qoRvCNEkCC9DhSr6Wvgvc7ps=', '2015-04-06 23:15:26'),
(13, 3, 2, 'iWkRBzyiCrV9i8oToPe7JOyFWlCWbZch12OA2FQCYAY=', '2015-04-06 23:17:33'),
(15, 1, 2, '5VRc5o6Ajvn6d/uPSmRsajCHDubQ1elio7MpXhbf6Mo=', '2015-04-06 23:20:00'),
(16, 29, 1, 'fNDnkCG1FJ9Y7r9N8KI+bAWSL8TsaiImBg7263NlZ4k=', '2015-04-07 18:03:25'),
(17, 22, 1, 'WdoOJ3z1hUN6YU62Wxn4ko5ywioDc6fmDErNt3EsnHE=', '2015-04-07 18:05:17'),
(18, 10, 1, 'PSalxYhLiOb7FcENoSsaZ1wYD1gDE/OH40tXE7gtzIk=', '2015-04-08 11:07:36'),
(19, 22, 1, 'Tn/EoWRh/fMPDeBFB9xzSdK0Bo9y36akzRSZ+C4gILQ=', '2015-04-08 12:32:09'),
(20, 10, 2, 'TL76qaWpuN1n7JR1hTeTRbwGQIusZqdjxsieUp8VTlA=', '2015-04-09 00:21:46'),
(21, 28, 2, '/XKA9d25CK4S5/c6+/Y0VVoYEc8b+c0G40bkVzQx/fU=', '2015-04-09 11:03:28'),
(23, 10, 1, 'N4OgX2EILvih7RnA0SAoqx6uV6DLPRWO2+knaxcvfGE=', '2015-04-10 18:02:23'),
(24, 3, 1, 'Ag9+kv5hNW7HCxoiFcj2ggAPsYVMdIglkeO6EN2nvpons9v/ybrIdZVE8EzwsOOFKKYcgfiNZSnCzpQxWgexSunuiPSes6FBEXUzemMRTG5d8l0gMkzB3vjCHW+0sfJLTtT7Qk1wCBo6UZrWGI/tvgx+7NLkH4Yna1s+ndwuV37I6B8TnhNMj1xXtY5udCj93GTL317X42ypAmO+ZftlirwHthByxoX3avcd3ehJJyJtLu1bF93zz5ME6RV5NUsLl4ZQl+VWyELsa/pL55xCXrlAqDMF2kDSIewQBsg6LD8XDgVVSxvLUBn/oTBcB8euDs98u9x+FFVtHudeNx9Mjoc3LpHKXZjUbqNbSNcqu6Oq1Ehsf6coikuib6PTZpycEKfq/XnJtY6IAPMDb8leW9uiip2soje3Ac77eDkAPYEYWwx26sxB5inOtx3UslH+mJuWovDtvICoJKAXhU2F4fsBuHklMMn9pxaNdshKxrvRbwfTNCM5FALniiQnxR6XDCgR+qzTfAPVl4uikccKIKg6QvdbwNBO9jBjNvzI/1/S2eyy+LXAaoV9N7oIjuRJ3/2YfMFMKS9EkNLxOlW9yYSAcBtaqH2pXfSWS59AcgrDdyCjWszeeUgwta5cz3EswUVAHxnB+VxjaL8PGWdNgwfldSBmAmOushSWMNXj8L4rgBVI71CuNga8/ORGjz/pbwYm676d87sxWJMGqyx1dFvkkSKSSvSis1tAXOdfg15ksvaTknmvGX2z/dbfvY9WFOckfa8el4eZtw41PjW18rM2Zlvze8JzxtuI/iSdqXnOsFg1TsfriV4nAtRfA6En9CdNNMau7TPC569kkzUNAKQ+mmrbHOodC4zf73Ju7K41o/kz30OpGBL10wWOhvP80TZzgWP6T9tknuTUvij2/TRftFDZkOkob5+1qooGPKvuCZ/Qqq3T49V9DeFmUhxK0dDv6HYpi8Xok6t5qPVnbwIPfpL+YTVuxwsaIhXI9oIAD7GFTHSIJZHjuhDdp76aJ7Pb/8m6yHWVRPBM8LDjhSimHIH4jWUpws6UMVoHsUrp7oj0nrOhQRF1M3pjEUxuXfJdIDJMwd74wh1vtLHyS07U+0JNcAgaOlGa1hiP7b4MfuzS5B+GJ2tbPp3cLld+yOgfE54TTI9cV7WObnQo/dxky99e1+NsqQJjvmX7ZYq8B7YQcsaF92r3Hd3oSScibS7tWxfd88+TBOkVeTVLC5eGUJflVshC7Gv6S+ecQl65QKgzBdpA0iHsEAbIOiw/jZ7vkrUVA5yh/WemHYROpthh2o3NYKe/qcmHqjhsXwQ=', '2015-04-14 01:02:27'),
(26, 30, 1, 's6lRByW4/ECowxi4JAO7lWRqb/0QWeIiZaHmqEkPCC4=', '2015-04-16 00:47:33'),
(27, 27, 1, 'd2UpqlbFewOM/SbhJe+VAE22CXMQNnJ1/uJrOWp/4Ak=', '2015-04-16 00:47:48'),
(28, 19, 1, 'KljUXik3KcOCeIaApumiQAW96NazVsfvgcWkx0uePwA=', '2015-04-16 00:48:17'),
(29, 33, 1, 'oUoVOg+9bG9QVPoJ8z2/ahUZpp9Uz0JL/+j/tstMJgBLZcmfnt3BRfD69lzujvPD6Nus/ssO7ljAwfAhBEm8SQ==', '2015-04-17 00:29:55'),
(30, 33, 1, 'Qc4lQbWCy5hx1WrtPzUJ79k8aQJkgBbE/DS2ek64MPc=', '2015-04-17 00:30:04');

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE IF NOT EXISTS `department` (
`deptID` int(11) NOT NULL,
  `deptName` varchar(256) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`deptID`, `deptName`) VALUES
(1, '075zASf706k/BPfKRixKyeGiuY0oiccDJfQB8TTXJpE=');

-- --------------------------------------------------------

--
-- Table structure for table `emp`
--

CREATE TABLE IF NOT EXISTS `emp` (
`empID` int(11) NOT NULL,
  `empNo` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `empEmail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `empPswd` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `empPasscode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `branchID` int(11) NOT NULL,
  `deptID` int(11) NOT NULL,
  `isActive` tinyint(1) NOT NULL DEFAULT '1',
  `setupStat` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0: no action; 1: new account; 2: reset pswd; 3: pswd reset interval',
  `isLogin` tinyint(1) NOT NULL DEFAULT '0',
  `loginTime` datetime DEFAULT NULL,
  `logoutTime` datetime DEFAULT NULL,
  `empName` varchar(256) CHARACTER SET utf8 DEFAULT NULL,
  `empGen` tinyint(1) NOT NULL,
  `empDOB` date NOT NULL,
  `empIDcard` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `empPhone` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `empMob` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `empSkype` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `empPic` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `loginFail` tinyint(3) NOT NULL DEFAULT '0',
  `empBackupEmail` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `emp`
--

INSERT INTO `emp` (`empID`, `empNo`, `empEmail`, `empPswd`, `empPasscode`, `branchID`, `deptID`, `isActive`, `setupStat`, `isLogin`, `loginTime`, `logoutTime`, `empName`, `empGen`, `empDOB`, `empIDcard`, `empPhone`, `empMob`, `empSkype`, `empPic`, `loginFail`, `empBackupEmail`) VALUES
(1, 'SA00000000', 'H+tt7b0DAaixxL7gMuqrkWKZCbLEsuEx/Ac7zg1pAd0=', '3bc7cb9e7eafda3af62ed82acfe27d511e0187e6f4d5310d5920a34d0e613faa712eab4ba701f7ff1495e669895d8368f88f79f8c21f125bb1e0c0c528e08e7cd', 'zD0Frfga6zqAZhF+z7Mi/h5MQqr3X9XZbL1KDKD7LV0=', 1, 1, 1, 0, 0, '2015-04-19 00:57:37', '2015-04-19 00:57:40', 'ENUjozS0X/DRnsQFanUSbLuda9ROdsfQt8FBf2H0ylU=', 1, '1995-01-01', 'tFUU68W1xhfs8t7YaSeBYOrQwn1UVsLNE/VMjlpiLlg=', 'tNzJdmyVkbg4In1iGL0GOLEpvlYR7lQMG9ePbnwwbWE=', 'JMHHBCH0NzR1T/t5tIG62Yu0scAfBvFhUzjaFUkH5f0=', NULL, 'http://3.bp.blogspot.com/-FkYziyjX_rU/UWEoY6cAQPI/AAAAAAAABXA/B7cwC8a0beI/s1600/admin.png', 0, NULL),
(2, 'TEMP000000', '+7/KuyelviG9K6g/PTXDF0rzmhhr0AOKF/v/YDFjRwo=', '3896c5fa28fe0791b8ebeee4ce3a36a67ee60945525d866746f351f14a3beb52709db51bdf44f37518b5b0fae5ef691adef7d29ffb8c37a14b4442b11825de973', 'WyANxSiUGaG96J0ilElH/+1dxLJawOi52Ve0WV/DCNU=', 1, 1, 1, 0, 1, '2015-04-19 01:18:15', '2015-04-14 00:05:07', 'tQdzVVepQyRtBQg/BHxKAuIxZkR9cGmYXCOcM8T7VvI=', 1, '1996-07-22', '0LLarDqjeu8r+b70P6oxULiZ61hXxZVyM4xiDgz6MDM=', 'ZwCvza19Ect4/rX/SR0ci+bti9oF3MjLpULTV3mmxp0=', 'ZwCvza19Ect4/rX/SR0ci+bti9oF3MjLpULTV3mmxp0=', 'cwMPhswVSPFo3ZmcK1bQJ6OIRxHybtfIOt+mzEBYZpM=', 'img/empImages/2.png', 0, 'xrAdGw5e5rSWixPawlspB5qEvQgKShm7Uc6mZYCuWn8='),
(3, 'TEMP000001', 'xrAdGw5e5rSWixPawlspB5qEvQgKShm7Uc6mZYCuWn8=', '3b8f05e5e608379181c279048cc5fd9fc6f57b15c3f26f439789e58e4ca22ebb5c6dc331f1cabc916bb2e66551874d8274a02f615d8c56ffec54ba32966af67cf', 'WyANxSiUGaG96J0ilElH/+1dxLJawOi52Ve0WV/DCNU=', 1, 1, 0, 2, 0, '2015-04-08 15:45:13', '2015-04-08 15:45:18', 'tQdzVVepQyRtBQg/BHxKAuIxZkR9cGmYXCOcM8T7VvI=', 1, '1996-07-27', '1234', '1234', '1234', '1234', 'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xfp1/v/t1.0-1/p160x160/10430431_571731726290341_8674011150107273264_n.jpg?oh=11f5c420aaf6afc662fe755416ba2546&oe=559D3165&__gda__=1436198670_ef93598ba8083c081fb53ee71ef33379', 1, 'rAdGw5e5rSWixPawlspB5qEvQgKShm7Uc6mZYCuWn8=');

-- --------------------------------------------------------

--
-- Table structure for table `emppos`
--

CREATE TABLE IF NOT EXISTS `emppos` (
  `empID` int(11) NOT NULL,
  `posID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `emppos`
--

INSERT INTO `emppos` (`empID`, `posID`) VALUES
(1, 1),
(1, 2),
(2, 2),
(3, 2);

-- --------------------------------------------------------

--
-- Table structure for table `permission`
--

CREATE TABLE IF NOT EXISTS `permission` (
  `empID` int(11) NOT NULL,
  `isAdmin` int(1) NOT NULL DEFAULT '0',
  `isUser` int(1) NOT NULL DEFAULT '1',
  `canPin` int(1) NOT NULL DEFAULT '0',
  `dashboard` int(1) NOT NULL DEFAULT '0',
  `general` int(1) NOT NULL DEFAULT '0',
  `personnel` int(1) NOT NULL DEFAULT '0',
  `setting` int(1) NOT NULL DEFAULT '0',
  `master` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permission`
--

INSERT INTO `permission` (`empID`, `isAdmin`, `isUser`, `canPin`, `dashboard`, `general`, `personnel`, `setting`, `master`) VALUES
(1, 1, 1, 1, 1, 1, 1, 1, 1),
(2, 0, 1, 0, 0, 0, 0, 0, 0),
(3, 0, 1, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `position`
--

CREATE TABLE IF NOT EXISTS `position` (
`posID` int(11) NOT NULL,
  `posName` varchar(128) CHARACTER SET utf8 NOT NULL,
  `canPin` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `position`
--

INSERT INTO `position` (`posID`, `posName`, `canPin`) VALUES
(1, 'GBpID5rsm9rmT0SssAjS3vE21t94j5LOE4qCvLo/nbQ=', 1),
(2, 'v0vE1fpKAyLJziMJ2Pi2JEcMoWhbux6RijkvAAV5Rc8=', 0);

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE IF NOT EXISTS `post` (
`postID` int(11) NOT NULL,
  `empID` int(11) NOT NULL,
  `postContent` text COLLATE utf8_unicode_ci NOT NULL,
  `postTime` datetime NOT NULL,
  `isPinned` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`postID`, `empID`, `postContent`, `postTime`, `isPinned`) VALUES
(1, 1, '3RtDI8KwxvPoGnQlN7HiAHwKPdgj1COh9U9oJv9s33v3hWgxRZ4bNwg5myH5BIL7uycYzXj/yQeCXcRes0XimQ==', '2015-04-04 21:04:57', 1),
(3, 1, '25JNF11VJVaqyXLViOmEXLkV4oXx5Yu3Z+MHPOpJ+lk=', '2015-04-04 22:44:26', 1),
(4, 2, 'BQ39OjheB60LD1oFYTrZyz9Vic+yaqj4Z+yrpbe5Mk5DLIvk2/7AMk+1clzXwwgGK/RSiPYsr5qfbPxojs2895Li6go9bZh209YylY5PVbRIOILPLD1M+qFHWdtA2zbiYwAO/YHOE+dlQCqD+Qes1q1ahgRCg0n/wUk1qOKJi3aseOmmuJ9LBdyGrZke+F+3/1UO4UU+CDuxCjdUgq2PCkXp1O56iT3g6GNyDul5EWJ4WfaGtWwyVKqC5GlUrn+hbDtQP3gAXEP8xkNANAEY8IK9MA74L0Ay/QdyF3LYtuQLivCf4sRxu1VsCe8PoKbJrlslCWFzQ8gOJGGibNN6pA==', '2015-04-06 01:11:27', 0),
(10, 1, 'H78wWMwDDCeQK9vpNQPE8MZ7nxU6szpGxdspcBBbR2TcLv70Zmt0rM7I7DmpvKi8qdrn0s9GcUBtFrisfvp3Jw==', '2015-04-06 02:01:05', 1),
(13, 1, 'hQz785HcXJvDj0FiAxjv7feJJRBw9ZStH74JdQjYSa4=', '2015-04-06 02:05:26', 0),
(17, 1, 'S9+Pqmo1fRD/vKJ8ElLgjIVfGVhPjn5IJCjCLS424oA=', '2015-04-06 02:13:55', 0),
(18, 1, 'lhLgKVNTFnB/H6/dBxIVtJy+VBbJEad/N1b7v9G4VQo=', '2015-04-06 02:16:47', 0),
(19, 1, 'VGcipbeWJyT3UumbLY4p+lHi6vO7UsZ1HR6w0KgsvSw=', '2015-04-06 02:17:02', 0),
(22, 3, 'EtzLQtMbFikKpI5eP2waRABW7wxkxddV/SBmDYorLiOk9Y4B00NK9dErp0wuF9+9xvMLrTEu/KNxt7oDeNOqPp31bULJkskJXKsYX2PBJzt0x5ksiC3eXUnVA+kqwJHw', '2015-04-06 02:24:42', 0),
(27, 1, 'HOx9zVCJMa+YmEspwszr0hxKqNfqpVhlpeDH+Ytzvsc=', '2015-04-07 01:04:05', 0),
(28, 1, 'qsxDgFkNeF+tTXDP3SGmjMf73aKlhHPdwONKGDSX9eM=', '2015-04-07 18:02:02', 1),
(29, 1, 'ltggSpn+XEqjAVcuQuhQ97SZxdklOMrqs8jXGmAg5Ya06aB6ZQ4PDFrp0Eg6zuZ1ybqrN3MY3LH60eE2Kw1yLnKAvDRXK1Yql89ibU2UfCN8q1CCSP3eojie8rpJaawP', '2015-04-07 18:03:01', 0),
(30, 1, 'IHdhC2tcxI+Q3ceIzooqOXhvgIyuxF8qVxb/Wb6ptxFKPTYg07faCj+WsipB/bwODbY3QVBlqfylMR61fWuRCw==', '2015-04-10 17:36:50', 0),
(33, 1, 'ARe0AeclIoN0ovBhP+ph4uaUNhZGzAOKgbOWyE+15yk=', '2015-04-16 01:39:41', 1),
(34, 1, 'WuMQrnO9LalT2e31MJ+AvWjv4GcwlV0epJUoZVK08YRbJfgXLtj1p8phf4VBGJCboLpwyPI2oFE6IyWD1meI2A==', '2015-04-16 01:51:59', 0),
(35, 1, 'uGcYBj71DkKbmTfZI58mLt1qqCU1C8jRIDZEvili3w+YRKX5cplDjfi9/BIptW+U5niK5gEXG6kj7t6aBB6oCg==', '2015-04-16 01:52:05', 0);

-- --------------------------------------------------------

--
-- Table structure for table `reqtype`
--

CREATE TABLE IF NOT EXISTS `reqtype` (
`reqTypeID` int(11) NOT NULL,
  `reqDesc` varchar(256) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `reqtype`
--

INSERT INTO `reqtype` (`reqTypeID`, `reqDesc`) VALUES
(1, 'Y7jOK+lUopwiWlIo7y/xQaByxEr1C3VqQ1hh8xwAQJc='),
(2, '1xquxC4K1rj6/ESLl+qP3tQxpSIK5w1AP7FC3qassKE=');

-- --------------------------------------------------------

--
-- Table structure for table `request`
--

CREATE TABLE IF NOT EXISTS `request` (
`reqID` int(11) NOT NULL,
  `reqTypeID` int(11) NOT NULL,
  `empID` int(11) NOT NULL,
  `reqContent` text COLLATE utf8_unicode_ci NOT NULL,
  `reqStat` int(1) NOT NULL DEFAULT '0' COMMENT 'Is processed yet.'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `request`
--

INSERT INTO `request` (`reqID`, `reqTypeID`, `empID`, `reqContent`, `reqStat`) VALUES
(1, 1, 1, 'Cy1gYlyZrSIIyv4bMWK+Pi3XfT7WYCC7GMFVIhsLt+g=', 0),
(5, 1, 1, 'iW6N9TKoaHt3YyK+3tC9IOyBs6BkYdR2gXaRTgXCe2Q=', 2),
(6, 1, 1, 'JsSPrtCZ3DcAp8iKKjcUGLkeL1fzzM/X8JwrchxIRCQ=', 3),
(7, 2, 1, '7iv5YRRThXzdByCWsP+b6EVwcdLxm71w67iO6YCV/eI=', 3),
(8, 2, 1, 'vt9tZHO2gxIuRzM/gOwOyI5do8Bv3aEGSZt55xCC2MA=', 3);

-- --------------------------------------------------------

--
-- Table structure for table `restrictword`
--

CREATE TABLE IF NOT EXISTS `restrictword` (
  `word` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `isDefault` tinyint(1) NOT NULL DEFAULT '0',
  `isActive` tinyint(1) NOT NULL DEFAULT '1',
  `usageTime` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `restrictword`
--

INSERT INTO `restrictword` (`word`, `isDefault`, `isActive`, `usageTime`) VALUES
('f*ck', 1, 1, 0),
('sh*t', 0, 1, 0),
('*sshole', 1, 1, 0),
('d*mn', 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE IF NOT EXISTS `setting` (
  `prop` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`prop`, `value`) VALUES
('appVer', '0.9.9.0'),
('cmtLength', '128'),
('companyName', 'Project 1'),
('datetimeType', 'h:i A - d/m/Y'),
('dbVer', '1.0.4.5'),
('empPic', 'img/empl.png'),
('loginFailLock', '5'),
('logo', 'img/logo.png'),
('pinPostLimit', '5'),
('postLength', '0'),
('postPerPage', '10'),
('pswdResetInterval', '365'),
('sysStat', '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `agenda`
--
ALTER TABLE `agenda`
 ADD PRIMARY KEY (`agdID`), ADD KEY `FK_agd_db` (`branchID`), ADD KEY `FK_agd_emp` (`empID`), ADD KEY `deptID` (`deptID`);

--
-- Indexes for table `branch`
--
ALTER TABLE `branch`
 ADD PRIMARY KEY (`branchID`);

--
-- Indexes for table `chklist`
--
ALTER TABLE `chklist`
 ADD PRIMARY KEY (`chkID`), ADD KEY `isActive` (`isActive`), ADD KEY `empID` (`empID`);

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
 ADD PRIMARY KEY (`cmtID`,`postID`,`empID`), ADD KEY `FK_cmt_emp` (`empID`), ADD KEY `FK_cmt_post` (`postID`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
 ADD PRIMARY KEY (`deptID`);

--
-- Indexes for table `emp`
--
ALTER TABLE `emp`
 ADD PRIMARY KEY (`empID`), ADD UNIQUE KEY `empEmail` (`empEmail`), ADD KEY `ID_empName` (`empName`(255)), ADD KEY `branchID` (`branchID`,`deptID`), ADD KEY `deptID` (`deptID`), ADD KEY `branchID_2` (`branchID`), ADD KEY `deptID_2` (`deptID`);

--
-- Indexes for table `emppos`
--
ALTER TABLE `emppos`
 ADD PRIMARY KEY (`empID`,`posID`), ADD KEY `FK_pos_emp` (`posID`);

--
-- Indexes for table `permission`
--
ALTER TABLE `permission`
 ADD PRIMARY KEY (`empID`);

--
-- Indexes for table `position`
--
ALTER TABLE `position`
 ADD PRIMARY KEY (`posID`);

--
-- Indexes for table `post`
--
ALTER TABLE `post`
 ADD PRIMARY KEY (`postID`), ADD KEY `FK_post_emp` (`empID`);

--
-- Indexes for table `reqtype`
--
ALTER TABLE `reqtype`
 ADD PRIMARY KEY (`reqTypeID`);

--
-- Indexes for table `request`
--
ALTER TABLE `request`
 ADD PRIMARY KEY (`reqID`), ADD KEY `FK_req_emp` (`empID`), ADD KEY `FK_reg_reqType` (`reqTypeID`), ADD KEY `reqStat` (`reqStat`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
 ADD PRIMARY KEY (`prop`), ADD UNIQUE KEY `U_property` (`prop`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `agenda`
--
ALTER TABLE `agenda`
MODIFY `agdID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=93;
--
-- AUTO_INCREMENT for table `branch`
--
ALTER TABLE `branch`
MODIFY `branchID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `chklist`
--
ALTER TABLE `chklist`
MODIFY `chkID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
MODIFY `cmtID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
MODIFY `deptID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `emp`
--
ALTER TABLE `emp`
MODIFY `empID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `position`
--
ALTER TABLE `position`
MODIFY `posID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
MODIFY `postID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `reqtype`
--
ALTER TABLE `reqtype`
MODIFY `reqTypeID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `request`
--
ALTER TABLE `request`
MODIFY `reqID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `agenda`
--
ALTER TABLE `agenda`
ADD CONSTRAINT `FK_agd_branch` FOREIGN KEY (`branchID`) REFERENCES `branch` (`branchID`),
ADD CONSTRAINT `FK_agd_dept` FOREIGN KEY (`deptID`) REFERENCES `department` (`deptID`),
ADD CONSTRAINT `agenda_ibfk_1` FOREIGN KEY (`empID`) REFERENCES `emp` (`empID`);

--
-- Constraints for table `chklist`
--
ALTER TABLE `chklist`
ADD CONSTRAINT `chklist_ibfk_1` FOREIGN KEY (`empID`) REFERENCES `emp` (`empID`);

--
-- Constraints for table `comment`
--
ALTER TABLE `comment`
ADD CONSTRAINT `comment_ibfk_1` FOREIGN KEY (`empID`) REFERENCES `emp` (`empID`),
ADD CONSTRAINT `comment_ibfk_2` FOREIGN KEY (`postID`) REFERENCES `post` (`postID`);

--
-- Constraints for table `emp`
--
ALTER TABLE `emp`
ADD CONSTRAINT `FK_emp_branch` FOREIGN KEY (`branchID`) REFERENCES `branch` (`branchID`),
ADD CONSTRAINT `FK_emp_dept` FOREIGN KEY (`deptID`) REFERENCES `department` (`deptID`);

--
-- Constraints for table `emppos`
--
ALTER TABLE `emppos`
ADD CONSTRAINT `FK_emppos_emp` FOREIGN KEY (`empID`) REFERENCES `emp` (`empID`),
ADD CONSTRAINT `FK_emppos_pos` FOREIGN KEY (`posID`) REFERENCES `position` (`posID`);

--
-- Constraints for table `permission`
--
ALTER TABLE `permission`
ADD CONSTRAINT `FK_emp_permission` FOREIGN KEY (`empID`) REFERENCES `emp` (`empID`);

--
-- Constraints for table `post`
--
ALTER TABLE `post`
ADD CONSTRAINT `post_ibfk_1` FOREIGN KEY (`empID`) REFERENCES `emp` (`empID`);

--
-- Constraints for table `request`
--
ALTER TABLE `request`
ADD CONSTRAINT `request_ibfk_1` FOREIGN KEY (`reqTypeID`) REFERENCES `reqtype` (`reqTypeID`),
ADD CONSTRAINT `request_ibfk_2` FOREIGN KEY (`empID`) REFERENCES `emp` (`empID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
