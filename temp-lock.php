<?php
session_start();
$token = $_SESSION['token'];
include 'conn.php';
include 'encrypt/encrypter.php';
$_SESSION['logged-in'] = 2;

if ($_SERVER['REQUEST_METHOD']=="POST") {
    if (empty($_POST['passcode']) || !isset($_POST['passcode'])) {
        $_SESSION['wrong']++;
    }
    elseif ($_SESSION['empPasscode'] == encrypt($_POST['passcode'])) {
        $_SESSION['logged-in'] = 3;
        unset($_SESSION['wrong']);
        header("Location: home.php");
        die();
    } else {
        $_SESSION['wrong']++;
    }
}
if (!isset($_SESSION['wrong'])) {
    $_SESSION['wrong'] = 0;
} else {
    if ($_SESSION['wrong'] > 3) {
        $q = "UPDATE `emp` SET `logoutTime` = NOW(), `isLogin` = '0' WHERE `empEmail` = '".encrypt($_SESSION['usr'])."';";
        $result = $conn -> query($q);
        mysqli_free_result($result);
        mysqli_close($conn);
        session_unset(); session_destroy();
        header("Location: index.php?w=1");
        die();
    }
    if($_SESSION['wrong'] == 0) $_SESSION['wrong']++;
}

/*if(!isset($_SESSION['auth'])){
    header("Location: index.php?exp=1");
    die();
}*/

if (!isset($_SESSION['logged-in']))
    if ($_SESSION['logged-in']!=1) {
        $q = "UPDATE `emp` SET `logoutTime` = NOW(), `isLogin` = '0' WHERE `empEmail` = '".encrypt($_SESSION['usr'])."';";
        $result = $conn -> query($q);
        mysqli_free_result($result);
        mysqli_close($conn);
        session_unset(); session_destroy();
        header("Location: index.php?out=1");
        die();
    }

if(!isset($_SESSION['logged-in'])){
    session_unset(); session_destroy();
    header("Location: index.php?w=1");
    die();
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>ECONNECT - TemporaryLock</title>
    <link rel="shortcut icon" href="img/icon.png">
    <link rel="stylesheet" href="css/templock.css">
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body>
    <h2><span class="glyph">&#xe033;</span> ECONNECT <BR> Temporary Lock</h2><BR>
    <h1><?php echo decrypt($_SESSION['empName']); ?></h1>

        <?php
        if ($_SESSION['wrong']!=0) {
            echo "<p>You have entered wrong passcode {$_SESSION['wrong']} times!</p>";
        }
        ?>

    <form name="tempLock" id="tempLock" action="#" method="post">
        <input type="password" name="passcode" id="passcode" placeholder="Passcode" autofocus="true">
        <BR>
        <input type="submit" name="passSubmit" id="passSubmit" value="Unlock">
    </form>
</body>
</html>