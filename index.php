<?php
$pre_encrypt ='5dQ66mdmOU6EyFgiGC6BRKDSt2NbPGehS94IJGn54x38l3';
$token = password_hash($pre_encrypt, PASSWORD_DEFAULT);
$_SESSION['token'] = $token;
include 'conn.php';
include 'encrypt/encrypter.php';
session_start();
$q = "SELECT `value` FROM `setting` WHERE prop = 'logo'";
$res = $conn->query($q);
if ($res->num_rows > 0) {
    $result = $res->fetch_assoc();
    $img = $result['value'];
}
$q = "SELECT `value` FROM `setting` WHERE prop = 'companyName'";
$res = $conn->query($q);
if ($res->num_rows > 0) {
    $result = $res->fetch_assoc();
    $_SESSION['companyName'] = $result['value'];
}

$sql = "SELECT `value` FROM setting WHERE prop = 'sysStat'";
$r = $conn->query($sql);
$re= $r->fetch_assoc();
if ($re['value']==0) {
    // System shut-down
    include 'inactive.php';
    die();
}
mysqli_free_result($r);
mysqli_free_result($res);
mysqli_close($conn);
?>
<!doctype html>
<html lang="en" class="no-js"> 
<head> 
  <meta charset="UTF-8" /> 
  <meta name="description" content="A web app for employees in one organization can connect with others easily.">
  <meta name="keywords" content="project, aprotrain, aptech, econnect, connect, webapp, web, app">
  <meta name="msapplication-TileColor" content="#54ACD2"/>
  <meta name="msapplication-square150x150logo" content="img/icon.png"/>
  <meta name="application-name" content="eConnect" />
  <meta name="msapplication-tooltip" content="Private WebApp for Organizations" />
  <meta name="msapplication-navbutton-color" content="#2969B0" />
  <title>Login - ECONNECT</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="css/login.css" />
  <link rel="shortcut icon" href="img/icon.png">
  <script type='text/javascript' src='script/jquery-2.1.3.min.js'></script>
  <script type='text/javascript' src='script/login/jquery.particleground.min.js'></script>
    <script type="text/javascript">
        var main = function () {
            $('#loginForm').attr('autocomplete', 'off');
        }
        $(document).ready(main);
    </script>
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
  <meta name="norton-safeweb-site-verification" content="08hhfhsna1qjjgqqo7ubg01xwohhyjhuf2jta8-ilq1v9sc3hnagl2g5qg39wljfxbujlsh9mdbuzm-rrfxy-ws7k1-epkv7331z7b8edqflpvm3yjrf1dwvhs9d72yh" />
</head>

<?php flush();
?>

<body>
<script type="text/javascript">
	function login(usrName, loginStat) {
		/*var login = "home.php?login="+i;
		$("html").load(login);
		$.post("home.php",
		  {
			user: usrName,
			login: loginStat
		  });*/
		$("html").load(login);
		$.ajax({
		  type: "POST",
		  url: 'home.php',
		  user: usrName,
		  login: loginStat,
		  success: $("html").load('home.php')
		});
	}
</script>
<div id="particles">
  <div class="intro">
  <a href="javascript:alert('Project 1\nC1410L - Aprotrain Aptech\n\nBy: Giang + Đức + Quân');"><b>Project 1</b></a> - eConnect<BR>
      <?php
      if (isset($img))
          if (!empty($img))
              echo "<img src='".$img."' style='height: 50px' alt='Company Logo'>";
      ?>
    <h1><span style="text-transform: lowercase"></span><?php if(isset($_SESSION['companyName'])) { if(!empty($_SESSION['companyName'])) echo $_SESSION['companyName']; else echo "econnect"; }?></h1>

	  <?php
      error_reporting(0);
      session_start();

      if (isset($_GET['w'])) {
          if ($_GET['w'] == 1) {
              echo "<p>You've entered wrong User name or password!</p>";
              session_unset(); session_destroy();
          }
      }
      elseif (isset($_GET['out'])) {
          if ($_GET['out'] == 1) {
              echo "<p>Logged out!</p>";
              session_unset(); session_destroy();
          }
      }
      elseif (isset($_GET['exp'])) {
          if ($_GET['exp']==1) {
              echo "<p>Your session has expired. Please login again!</p>";
              session_unset(); session_destroy();
          }
      }
      elseif (isset($_GET['inactive'])) {
          if ($_GET['inactive'] == 1) {
              echo "<p>Your account has been deactivated!<BR>Contact your IT manager for more information</p>";
              session_unset(); session_destroy();
          }
      }
	  elseif (isset($_GET['setup'])) {
          if ($_GET['setup'] == 1) {
              echo "<p>Your setup has completed, please login again</p>";
              session_unset(); session_destroy();
          }
      }
      elseif (isset($_GET['resetPswd'])) {
          if ($_GET['resetPswd'] == 1) {
              echo "<p style='color: #ac2925; font-weight: bolder; background: cornsilk; padding: 10px; border-radius: 5px;'>You have entered wrong password too many time!<BR>We've reset your password and sent it to your email address.<BR>
                    <BR>Please follow these <a href=\"javascript:alert('Emergency password reset instructions:\\n1. Receive notification email with temporary password\\n2. Login with temporary password\\n3. Set your new password when required\\n\\nContact IT manager or System Administrator for further information.')\" style='background: #ac2925; color: cornsilk'> Instructions </a>.</p>
                    <a href='index.php' target='_top'><span class='btn' style='background: #1A2E43; color: #fff'>Login page</span></a>  ";
          }
      }else {
          echo "<p>Please login to connect with others</p>";
      }

	  unset($_POST['pwsd']);
	  unset($_POST['empID']);

      function hideInput() {
          if (isset($_GET['resetPswd'])) {
              if ($_GET['resetPswd'] == 1) {
                  echo "style = 'display: none;'";
              }
          }
      }
	  ?>
    <form name="login" id="login" action="auth.php" method="post" autocomplete="off">

  		<input type="text" placeholder="Employee ID / User name" name="empID" id="empID" required="true" autofocus autocomplete="off" <?php hideInput() ?>/><BR>
        <input type="password" placeholder="Password" name="pswd" id="pswd" required="true" autocomplete="off" <?php hideInput() ?>/>
        <p id="alert"></p>
        <input type="submit" class="btn" value="LOGIN" name="loginBtn" id="loginBtn" onClick="" <?php hideInput() ?>/><BR><BR>
        <a href="javascript:alert('Reset password instructions:\n1. Contact IT manager to reset your password\n2. Receive notification email with temporary password\n3. Login with temporary password\n4. Set your new password when required\n\nContact IT manager or System Administrator for further information.')" target="_blank" id="forgotPswd" <?php hideInput() ?>>Forgot password?</a> | <a href="docs/manual.pdf" target="_blank" class="link">User Manual</a>
  </form>
  </div>
</div>
<!--
<img src="http://www.w3.org/html/logo/badge/html5-badge-h-connectivity-css3-graphics-performance.png" width="229" height="64" alt="Powered with HTML5 for Realtime Connectivity, CSS3, Graphics, 3D Effects, and Performance" title="Powered with HTML5 for Realtime Connectivity, CSS3, Graphics, 3D Effects, and Performance" id="html5Badge">
<a href="https://safeweb.norton.com/report/show?url=econnect.cf" target="_blank" id="safeBadge"><img src="img/norton-safe-web.png" alt="Norton Safe Web" title="Norton Safe Web"/></a>
-->
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-61854803-1', 'auto');
    ga('send', 'pageview');

</script>
</body> 
</html> 