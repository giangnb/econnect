<?php
session_start();

$token = $_SESSION['token'];
include 'conn.php';
include 'encrypt/encrypter.php';

	if(!isset($_SESSION['auth'])){
        session_unset();
        session_destroy();
		header("Location: index.php?exp=1");
		die();
	}

    if (isset($_GET['logout']))
        if ($_GET['logout']==1) {
            $q = "UPDATE `emp` SET `logoutTime` = NOW(), `isLogin` = '0' WHERE `empEmail` = '".encrypt($_SESSION['usr'])."';";
            $result = $conn -> query($q);
            mysqli_free_result($result);
            mysqli_close($conn);
            session_unset();
            session_destroy();
            header("Location: index.php?out=1");
            die();
        }

    if(isset($_SESSION['logged-in'])){
        if ($_SESSION['logged-in']==1 || $_SESSION['logged-in']==2) {
            $q = "UPDATE `emp` SET `isLogin` = '0' WHERE `empEmail` = '".encrypt($_SESSION['usr'])."';";
            $result = $conn -> query($q);
            mysqli_free_result($result);
            mysqli_close($conn);
            session_unset();
            session_destroy();
            header("Location: index.php?exp=1");
            die();
        }
    }

// Check session: Logout after 30 mins of inactive
if (isset ($_SESSION['EXPIRES'])) {
    if ($_SESSION['EXPIRES'] < time()-3600 && $_SESSION['logged-in']!=3) {
        $q = "UPDATE `emp` SET `logoutTime` = NOW(), `isLogin` = '0' WHERE `empEmail` = '".encrypt($_SESSION['usr'])."';";
        $result = $conn -> query($q);
        session_unset();
        session_destroy();
        header("Location: index.php?exp=1");
        die();
    }
}

$_SESSION['logged-in'] = 1;
$_SESSION['usr-logged-in'] = 1;
$q = "SELECT empName FROM emp WHERE empEmail = '".encrypt($_SESSION['usr'])."'";
$result = $conn -> query($q);
if ($result->num_rows > 0) $_SESSION['usrDetail'] = $result->fetch_assoc();
?>

<!doctype html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <title>eConnect</title>
    
    <link rel="shortcut icon" href="img/icon.png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="<?php if(isset($_SESSION['companyName'])) { if(!empty($_SESSION['companyName'])) echo $_SESSION['companyName']; else echo "ECONNECT"; }?> - eConnect">
    <link rel="stylesheet" type="text/css" href="css/style.css">
	<meta name="msapplication-TileColor" content="#2C82C9"/>
    <meta name="msapplication-square150x150logo" content="img/icon.png"/>
	<meta name="application-name" content="eConnect" />
    <meta name="msapplication-tooltip" content="Private WebApp for Organizations" />
    <meta name="msapplication-navbutton-color" content="#2969B0" />
    <link rel="stylesheet" type="text/css" href="css/scrollbar.css">
    <link rel="stylesheet" type="text/css" href="css/animate.css">
    <link rel="stylesheet" type="text/css" href="css/dhtmlxcalendar.css"/>
    <link rel="stylesheet" type="text/css" href="css/jquery-ui.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/jquery.ui.timepicker.css"/>
    <style>
        div#discussPost {
            width: 70%;
            height: 50px;
            border: 1px solid #a4bed4;
        }
        #calendar-home .ui-datepicker-inline {
            width: 95% !important;
            text-align: center;
        }
    </style>
    <!--link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"-->
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <script type="text/javascript" src="script/jquery-2.1.3.min.js"></script>
    <script type="text/javascript" src="script/angular.min.js"></script>
    <script src="script/dhtmlxcalendar.js"></script>
    <script type="text/javascript" src="script/Chart.min.js"></script>
    <script type="text/javascript" src="script/jquery-ui.min.js"></script>
    <script type="text/javascript" src="script/jquery.ui.timepicker.js"></script>
	<script type="text/javascript" src="script/tinymce/tinymce.min.js"></script>
	<meta name="norton-safeweb-site-verification" content="08hhfhsna1qjjgqqo7ubg01xwohhyjhuf2jta8-ilq1v9sc3hnagl2g5qg39wljfxbujlsh9mdbuzm-rrfxy-ws7k1-epkv7331z7b8edqflpvm3yjrf1dwvhs9d72yh" />
</head>

<?php flush(); ?>

<body>
	<?php
    	$pre_encrypt ='5dQ66mdmOU6EyFgiGC6BRKDSt2NbPGehS94IJGn54x38l3';
		$encrypt = password_hash($pre_encrypt, PASSWORD_DEFAULT);
        echo "<script type='text/javascript'> var encrypt = '".$encrypt."';</script>";
    ?>
	<div id="greeting"><center>
		<BR><BR><BR><h1 style="font-size:54px; font-weight:200; color:#FFFFFF">CONNECT</h1>
        Welcomes you!
    </center></div>
	<header>
    	<!--nav id="top-nav">
        	<ul>
            	<li class="nav-left">&nbsp;</li>
                <li class="nav-center"></li>
                
            </ul>
        </nav-->
        
        <div class="clear"></div>
        
        <nav id="navigator">
        	<ul>
            	<li class="nav-left" id="nav-logo" title="Company Name" style="background:url(<?php if (isset($_SESSION['logo'])) if (!empty($_SESSION['logo'])) echo "'".$_SESSION['logo']."'"; ?>) left no-repeat; background-size: contain;">
                	&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<b style="font-size:21px; text-align: right">
                        <?php if(isset($_SESSION['companyName'])) { if(!empty($_SESSION['companyName'])) echo $_SESSION['companyName']; else echo "ECONNECT"; }?>
                    </b>
                </li>
                <ul id="nav-tabs" class="nav-center">
                	<li id="nav-discuss" class="nav-current context">Discuss</li>
                    <li id="nav-agenda" class="context">Tasks</li>
                    <li id="nav-people" class="context">Contacts</li>
                    <li id="nav-support" class="context">Support</li>
                    <li id="nav-search"><!--img src="img/search-ico.png" width="24px"/--><span class="glyph" style="width: 24px">&#xe003;</span> <!--Search--></li>
                </ul>
                <ul id="nav-icons" class="nav-right">
                	<li title="Person Information" id="nav-user" onclick="ajaxLoader('usr-page')"><img src="<?php echo $_SESSION['empPic']; ?>"></li>
                    <!--li id="nav-icon-settings" title="Settings"><span class="glyph">&#xe019;</span> </li-->
                    <?php if ($_SESSION['isAdmin']) echo '<li title="Administrator page"><a href="admin/auth.php" class="glyph">&#xe019;</a> </li>'; ?>
                    <li id="nav-icon-logout" title="Logout"><!--img src="img/logout-ico.png"--><span class="glyph">&#xe033;</span></li>
                </ul>
                <!--li class="nav-right"><input type="text" placeholder="To-do list search" onChange="" name="search-todo" id="search-todo"></li-->
            </ul>
            
            <div id="search-float">
            	<input type="text" placeholder="Search everywhere" onKeyUp="ajaxSearch(0)" name="search-master" id="search-master">
                <section id="search-result">
                	What do you want to search?
                    <p>You can use this to seach for people information or an event</p>
                </section>
                <img src="img/exit-ico.png" id="exit-ico" title="Close Search bar">
            </div>
            
            <div class="nav-float" id="nav-settings">
            	<ul>
                	<li><a onclick="ajaxLoader('usr-page')">Edit personal information</a></li>
                    <li><a onclick="ajaxLoader('usr-page')">Security settings</a></li>
                    <li><span class="glyph" title="Language">&#xe135;</span> &nbsp;&nbsp;&nbsp;<select id="Lang"><option value="en" onchange="">English</option> <option value="vi">Vietnamese</option> </select></li>
                </ul>
            </div>
            
            <div class="nav-float" id="nav-logout">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<BR>
                <center>
                <a href="temp-lock.php"><span style="padding:2px 5px; background:#aacc4e; font-weight:bold" title="You can temporary lock your session with passcode and you can unlock without re-entering email and password.">TEMPORARY LOCK</span></a><BR>
                <a href="home.php?logout=1"><span style="padding:2px 5px; background:#ff503f; font-weight:bold">LOGOUT</span></a></center>
            </div>
        </nav>
        
        <div class="clear"></div>
    </header>
    
    <section id="main-framework">
    	<section id="frame-left">
        	<section id="sub-menu">
            	Loading sub-menu . . .
            </section>

        	<h1 id="home-widget-toggle" title="Calendar & Calculator widget switch" style="border-top:solid thin #2E303D; padding-top:5px;">CALENDAR <small>/</small> <small style="font-size: x-small">Calculator</small></h1>
                <div id="calendar-home" style="position:relative; padding-left:5px; width: 100%;height:320px;">

                </div>
            <iframe width="100%" height="50%" id="calculator-home" src="http://web2.0calc.com/widgets/minimal/" scrolling="no" style="border: none; margin: auto"><h2>Your browser doesn't support!<BR>Please update it.</h2></iframe>
        </section>

        <section id="frame-center">
        	<section id="ajaxShow" class="container-custom">
            	<!-- AJAX here! --><h1>Loading . . .</h1>
            </section>
        </section>
        
        <section id="frame-right" class="container-custom">
        	<h1>TO-DO LIST</h1>
            <input type="text" name="addTodo" id="addTodo" placeholder="Type & press Enter to add task"/>
            <div id="todoScroll"><ol>
            	<!-- Checklist Showcase -->
                    Loading...

                <!-- END Checklist -->

            </div>
        </section>
    </section>
    
    <div class="clear"></div>
    <!-- POP-UPS -->
    <div id="popup-bg"></div>
    
    <div id="shortcuts"><h1 style="color:#fff; text-align:left">Specified Shortcuts</h1></div>
    
    <div id="discuss-popup-comment" class="popup">
    	<img src="img/exit-ico.png" class="close-popup"/>
        <h3>Comments</h3><BR>
        <div class="discuss-popup-scroll">

        </div>
        <textarea name="comment" id="comment" placeholder="Your comment" rows="2"></textarea>
        <input type="button" onclick="sendCmt()" style="float: right; margin-right: 45px;" id="send-cmt-btn" value=" Send ">
        <div class="clear"></div>
    </div>
    <div id="discuss-popup-save" class="popup">
        <img src="img/exit-ico.png" class="close-popup"/>
        <h3>Edit post</h3><BR>
        <div class="discuss-edit-window">

        </div>
    </div>
    <div id="discuss-send-post" class="popup">
        <img src="img/exit-ico.png" class="close-popup"/>
        <h3>Create new post</h3><BR>
        <div class="discuss-send-window">

        </div>
    </div>
    
    <div id="agenda-popup" class="popup">
        <img src="img/exit-ico.png" class="close-popup"/>
        <h3>
            Add new
            <select name="taskType" id="taskType" onchange="taskTypeSwitch()" style="font-size: large; font-weight: bold; min-width: 100px">
                <option value="1">Event</option>
                <option value="2">Work</option>
            </select>
        </h3><BR>
        <div id="eventFrm">
            Title: <input type="text" name="eventTitle" id="eventTitle" placeholder="Event title" style="width: 90%"><BR>
            From: <input type="text" name="startDate" id="startDate" placeholder="dd-mm-yyyy" readonly> &nbsp;&nbsp;&nbsp;
            <span id="agenda-toggle-1">To: <input type="text" name="endDate" id="endDate" placeholder="dd-mm-yyyy" readonly> &nbsp;&nbsp;&nbsp; </span>
            <button type="checkbox" name="sameDay" id="sameDay" onclick="eventSameDay()">Same day</button><BR><BR>
            <span id="agenda-toggle-2">Time: <input type="time" name="startTime" id="startTime" placeholder="hh:mm ss" step="900" readonly> &nbsp; <span id="allDayCaption">-</span> &nbsp;
            <input type="time" name="endTime" id="endTime" placeholder="hh:mm ss" step="900" readonly> &nbsp;&nbsp;&nbsp; </span>
            <button type="button" name="allDay" id="allDay" onclick="eventAllDay()">All day event</button><BR><BR>
            <textarea name="eventContent" id="eventContent" placeholder="Event content"></textarea>
            <BR><input type="button" name="eventSubmit" id="eventSubmit" value="   Set event   " style="float: right"/>&nbsp;&nbsp;&nbsp;
            <!--<input type="reset" value="Reset"/>-->
            <div class="clear"></div>
        </div>

        <div id="workFrm">
            Initializing . . .
        </div>
    </div>

    <div id="empWorkChoose-popup" class="popup">
        <h3>Choose employee</h3><BR><BR>
        <input type="text" id="workEmpChoose" placeholder="Enter employee name / department / branch" style="width: 90%; padding:5px"><BR>
        <!--<input type="button" id="empWorkChooseBtn" value="  OK  ">-->
        <input type="hidden" id="workEmpChoose-id"><BR>
        <p id="workEmpChoose-description"></p>
        <BR><p>Type, choose and press ENTER to complete.</p>
    </div>
    
    <div id="people-popup" class="popup">
    	<img src="img/exit-ico.png" class="close-popup"/>
        <a href="#peopleSearch" style="float:left;" class="people-list-alphabet" id="linkToSearch">
            <h2 class="glyph">&#xe003;</h2>
        </a><span id="peopleListBtn"></span>
    </div>
    <script type="text/javascript" src="script/global.js">
    </script>
    <script type="text/javascript" src="script/ajax.js">
    </script>
	<script type="text/javascript" src="script/script.js">
	</script>
    <script type="text/javascript">

    </script>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-61854803-1', 'auto');
        ga('send', 'pageview');

    </script>

</body>
</html>
<?php
mysqli_free_result($result);
mysqli_close($conn);
?>