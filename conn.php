<?php
$db_server = "localhost";
$db_usrname = "root";
$db_pswd = "";
$db_name = "u934215388_db";

$conn = new mysqli($db_server,$db_usrname,$db_pswd,$db_name);

if(!isset($token)) {
    echo "<h1>ERROR 401: UNAUTHORIZED ACCESS</h1><BR/>You are attempting to access this page manually.<BR><b>Please access this page with a valid <a href=\"javascript:alert('Token key:\\n An unique string randomly generated when log-on process successfully. The only way to get it is log-on to the system with a valid credential.');\">TOKEN</a>!</b><BR><BR><h2>Connection to database refused.</h2>";
    session_destroy();
    die();
} else {
    if (isset($_GET['connection-status'])) {
        if ($conn -> connect_error) {
            echo "<h1>FATAL ERROR!</h1><hr>Connection Error: {$conn->connect_error}";
            die();
        } else {
            echo "<h1>Connected.</h1>";
        }
        die();
    }
}

/**
 * @param $q
 * @return int
 */
function select($q) {
    $result = $conn->query($q);
    if ($result->num_rows <1) {
        return 0;
    } else {
        return ($result->fetch_assoc());
    }
}
?>
