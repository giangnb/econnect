// JavaScript Document

//jQuery UI Tooltips
$(function() {
    $( document ).tooltip({
        show: {
            effect: "slideDown",
            delay: 250
        },
        track: true
    });
});

var global = function() {
    // Mozilla: Disable automatic password remember prompt
    $('#loginForm').attr('autocomplete', 'off');
	// Pop-ups resizing
	$("#search-float").css({"max-height":screen.height * 0.63});
	$(".popup").css({"height":screen.height * 0.5, "width":screen.width * 0.5}).hide();
	$("#popup-bg").hide();
	$(".discuss-popup-scroll").css("height", screen.height * 0.37);
	//$("#comment").css("height", screen.height * 0.06);
	$("#eventContent").css("height", screen.height * 0.25);
	$("#search-result").css("max-height", screen.height*0.6);
	$("#sub-menu").css({"height": screen.height*0.3, "max-height": screen.height*0.3});
    $("#workFrm").hide();
	
	// Welcome showcase
	$("#greeting").fadeOut(1200);
	
	// jQuery for Shortcuts
	$("#shortcuts").hide();
	$(document).keydown(function(e) {
        if(e.ctrlKey) {
            if (e.ctrlKey && e.shiftKey) {
                $("#shortcuts").show().addClass("animated fadeIn");
            }
			$(document).keyup(function() {
				if (!(e.shiftKey)) {
					$("#shortcuts").removeClass("fadeIn").addClass("fadeOut");
					window.setTimeout( function(){$("#shortcuts").removeClass("fadeOut animated").hide();}, 1000 );
				}
			});
			if (e.ctrlKey && e.shiftKey && (e.which === 68 || e.which === 49)) {discuss();hideSearch();e.preventDefault();}
			if (e.ctrlKey && e.shiftKey && (e.which === 65 || e.which === 50)) {agenda();hideSearch();e.preventDefault();}
			if (e.ctrlKey && e.shiftKey && (e.which === 80 || e.which === 51)) {people();hideSearch();e.preventDefault();}
			if (e.ctrlKey && e.shiftKey && e.which === 52) {support();hideSearch();e.preventDefault();}
			if (e.ctrlKey && e.shiftKey && e.which === 83) {
				$("#search-float").toggle(100);
				e.preventDefault();
				$("#nav-tabs").toggle(200);
				document.getElementById("search-master").value = "";
				document.getElementById("search-master").focus();
			}
			if (e.ctrlKey && e.which === 83) {
				e.preventDefault();
			}
		}
    });
	
	// jQuery for navigation bar elements
	var isEven=0;// Set for #todoScroll
	
	$("#search-float").hide();
	$("#personInfo-float").hide();
	$(".nav-float").hide();
	
	$("#nav-search").click(function() {
		$("#search-float").show(100);
		//$("#search-float").addClass("nav-current");
		$("#nav-tabs").hide(200);
		$("#search-master").focus();
	});
	function hideSearch() {
		$("#search-float").hide(100).removeClass("nav-current");
		$("#nav-tabs").show(200);
		document.getElementById("search-master").value = "";
	}
	$("#main-framework,#exit-ico, .nav-left, .nav-right").click(function() {hideSearch()});
	
	$("#nav-icon-settings").click(function() {
		$("#nav-settings").toggle(200);
		$("#nav-logout").hide(200);
		$("#personInfo-float").hide(200);
	});
	
	$("#nav-icon-logout").click(function() {
		$("#nav-logout").toggle(200);
		$("#nav-settings").hide(200);
		$("#personInfo-float").hide(200);
	});
	
	$("#no-btn").click(function() {
		$(".nav-float").hide(200);
		$("#nav-icons li").toggleClass("nav-icons-selected");
	});
	
	
	// jQuery for Page changing
	function loadMsg() {
		document.getElementById("sub-menu").innerHTML = "<b>Loading sub-menu . . .</b>";
		document.getElementById("ajaxShow").innerHTML = "<center><BR><BR><BR><div class='bubblingG'> <span id='bubblingG_1'> </span> <span id='bubblingG_2'> </span> <span id='bubblingG_3'> </span></div><h2 id='loadMsg'>Loading . . .</h2></center>";
		$("#loadMsg").addClass("animated infinite flipInY");
	}
    $("#nav-user").click(function() {usrPage()});
    function usrPage() {
        loadMsg();
        $("#discuss").hide(200);
        $("#agendar").hide(200);
        $("#people").hide(200);
        $("#support").hide(200);
        $("#nav-discuss").removeClass("nav-current animated flipInY");
        $("#nav-agenda").removeClass("nav-current animated flipInY");
        $("#nav-people").removeClass("nav-current animated flipInY");
        $("#nav-support").removeClass("nav-current animated flipInY");
    }
	$("#nav-discuss").click(function() {discuss()});
	function discuss() {
		loadMsg();
		$("#discuss").show(200);
		$("#agendar").hide(200);
		$("#people").hide(200);
		$("#support").hide(200);
		$("#nav-discuss").addClass("nav-current animated flipInY");
		$("#nav-agenda").removeClass("nav-current animated flipInY");
		$("#nav-people").removeClass("nav-current animated flipInY");
		$("#nav-support").removeClass("nav-current animated flipInY");
		ajaxLoader(1);
	}

	$("#nav-agenda").click(function() {agenda()});
	function agenda() {
		loadMsg();
		$("#discuss").hide(200);
		$("#agendar").show(200);
		$("#people").hide(200);
		$("#support").hide(200);
		$("#nav-discuss").removeClass("nav-current animated flipInY");
		$("#nav-agenda").addClass("nav-current animated flipInY");
		$("#nav-people").removeClass("nav-current animated flipInY");
		$("#nav-support").removeClass("nav-current animated flipInY");
		ajaxLoader(2);
	}
	
	$("#nav-people").click(function() {people()});
	function people() {
		loadMsg();
		$("#discuss").hide(200);
		$("#agendar").hide(200);
		$("#people").show(200);
		$("#support").hide(200);
		$("#nav-discuss").removeClass("nav-current animated flipInY");
		$("#nav-agenda").removeClass("nav-current animated flipInY");
		$("#nav-people").addClass("nav-current animated flipInY");
		$("#nav-support").removeClass("nav-current animated flipInY");
		ajaxLoader(3);
	}
	
	$("#nav-support").click(function() {support()});
	function support() {
		loadMsg();
		$("#discuss").hide(200);
		$("#agendar").hide(200);
		$("#people").hide(200);
		$("#support").show(200);
		$("#nav-discuss").removeClass("nav-current animated flipInY");
		$("#nav-agenda").removeClass("nav-current animated flipInY");
		$("#nav-people").removeClass("nav-current animated flipInY");
		$("#nav-support").addClass("nav-current animated flipInY");
		ajaxLoader(4);
	}
	
	// jQuery for Checklist
	$("#addTodo").keypress(function(event) {
        if(event.which === 13){
			var content = $("#addTodo").val();
            if (content=="" || content==" " || content.length < 2) {
                alert("Don't save a blank or too short check-list item!");
            } else {
                $.post("dependences/checklist.php?encrypt="+encrypt, {
                    add: content
                }, function(data) {
                    $("#todoScroll").html(data);
                    $("#addTodo").val('');
                });
            }
		}
    });
	$(".todoItem").click(function() {
		$(this).toggleClass("todoDone");
	});

    //  Calculator - Calendar Switch
    $("#calendar-home").hide();
	document.getElementById('home-widget-toggle').innerHTML = "<small style=\"font-size: x-small\">Calendar</small> <small>/</small> CALCULATOR";
    var i = 0;
    $("#home-widget-toggle").click(function () {
        if (i==0) {
            document.getElementById('home-widget-toggle').innerHTML = "CALENDAR <small>/</small> <small style=\"font-size: x-small\">Calculator</small>";
            i++;
            $("#calendar-home").show(150);
            $("#calculator-home").hide(150);
        } else {
            document.getElementById('home-widget-toggle').innerHTML = "<small style=\"font-size: x-small\">Calendar</small> <small>/</small> CALCULATOR";
            i--;
            $("#calendar-home").hide(150);
            $("#calculator-home").delay(150).show(150);
        }

    });

    // jQuery for Checklist
    $("#workEmpChoose").keypress(function(event) {
        //var chk = /^[0-9]+ | /;
        var content = $("#workEmpChoose").val();
        if(event.which === 13){
            /*if (!chk.test(content)) {
                alert("Please choose an employee from the list!\nIf the list not shown, there doesn't any employee match what you type.");
            } else {
                $("#empWorkChoose-popup").hide(100);
                document.getElementById("workEmp"+workItem).value = content;
            }*/
            $("#empWorkChoose-popup").hide(100);
            document.getElementById("workEmp"+workItem).value = document.getElementById("workEmpChoose-id").value+document.getElementById("workEmpChoose").value+" ("+document.getElementById("workEmpChoose-description").innerText+")";
        }
        if(event.which === 27){
            event.preventDefault();
            $("#empWorkChoose-popup").hide(100);
        }
    });

    // jQuery for Sending Comment
    $("#comment").css("height", screen.height * 0.06).keypress(function(event) {
        var comment = $("#comment").val();
        if (comment=="" || comment==" " || comment.length < 2 || comment.length > 255) {
            if (comment.length > 255) {
                $("#send-cmt-btn").val(' TOO LONG! ');
            }
            if (comment.length < 2) {
                $("#send-cmt-btn").val(' TOO SHORT! ');
            }
        } else {
            $("#send-cmt-btn").val(' Send ');
            if(event.which === 13){
                sendCmt();
            }
        }
    });


    // TASKS QUERIES -
    // send event
    $("#eventSubmit").click(function() {
        var title = $("#eventTitle").val();
        title = title.replace(/<h[1-9]*>|<[/]h[1-9]*>|<p(\s|[a-z]|[0-9]|['".=+-]){1,}>|<section(\s|[a-z]|[0-9]|['".=+-]){1,}>|<div(\s|[a-z]|[0-9]|['".=+-]){1,}>|<p>|<section>|<div>|<[/]p>|<[/]section>|<[/]div>/gi, ' ');
        var startDate = $("#startDate").val();
        var endDate = "";
        if (sameDay==1) {
            endDate = startDate;
        } else {
            endDate = $("#endDate").val();
        }
        var startTime = "";
        var endTime = "";
        if (allDay==1) {
            startTime = "00:00:01";
            endTime = "23:59:59";
        } else {
            startTime = $("#startTime").val()+":00";
            endTime = $("#endTime").val()+":00";
        }
        var content = $("#eventContent").val();
        content = content.replace(/\r\n|\n|\r/g, '<br />');
        content = content.replace(/<h[1-9]*>|<[/]h[1-9]*>|<p(\s|[a-z]|[0-9]|['".=+-]){1,}>|<section(\s|[a-z]|[0-9]|['".=+-]){1,}>|<div(\s|[a-z]|[0-9]|['".=+-]){1,}>|<p>|<section>|<div>|<[/]p>|<[/]section>|<[/]div>/gi, ' ');
        var datechk = /^[0-9]{1,2}-[0-9]{1,2}-[0-9]{4}$/;
        var timechk = /^[0-9]{1,2}:[0-9]{1,2}/;
        //:[0-9]{1,2}
        var error="";
        if (title=="" || title.length<5 || title.length>64) {
            error+=" - Title length should be between 05 and 64 characters\n";
        }
        if (startDate=="" || endDate=="" || !datechk.test(startDate) || !datechk.test(endDate)) {
            error+=" - Event date not valid\n";
        }
        if (allDay==0 && (startTime=="" || endTime=="" || !timechk.test(startTime) || !timechk.test(endTime)) ) {
            error+=" - Event time not valid\n";
        }
        if (content.length<10) {
            error+=" - Content should not blank\n";
        }
        if (error!="") {
            alert("There are some errors:\n\n"+error);
        } else {
            $.post("dependences/2.php?encrypt="+encrypt, {
                 isEvent: 1,
                 title: title,
                 start: startDate+" "+startTime,
                 end: endDate+" "+endTime,
                 content: content
                 }, function(data) {
                    $("#ajaxShow").html(data);
             });
            $("#eventTitle").val('');
            $("#startDate").val('');
            $("#endDate").val('');
            $("#startTime").val('');
            $("#endTime").val('');
            $("#eventContent").val('');
            $(".popup, #popup-bg").hide(200);
        }
    });
    // send work
    /*$("button#workSubmit").click(function() {
        var start = $("#workStart").val()+" 00:00:01";
        var end = $("#workEnd").val()+" 23:59:59";
        var title = $("#workTitle").val();
        title = title.replace(/<h[1-9]*>|<[/]h[1-9]*>|<p(\s|[a-z]|[0-9]|['".=+-]){1,}>|<section(\s|[a-z]|[0-9]|['".=+-]){1,}>|<div(\s|[a-z]|[0-9]|['".=+-]){1,}>|<p>|<section>|<div>|<[/]p>|<[/]section>|<[/]div>/gi, ' ');
        var error="";
        var timechk = /^[0-9]{1,2}-[0-9]{1,2}-[0-9]{4}/;
        if (timechk.test(start)==false || timechk.test(end)==false) {
            error+=" - Invalid date format\n";
        }
        if (title.length<5 || title.length>64) {
            error+=" - Title length should between 5 and 64 characters\n";
        }
        for (var count=1; count<=t; count++) {
            if (document.getElementById("workDetail"+count).value=="" || document.getElementById("workDetail"+count).value.length<5 || document.getElementById("workDetail"+count).value>64 || document.getElementById("workEmp"+count).value=="") {
                error+=" - Fill all fields in parts of work and employees assignment";
                break;
            }
        }
        if (error!="") {
            alert("There are some problems with your submission:\n\n"+error);
        } else {
            var content="^^div$$";
            var assign=";";
            var stat = ";";
            for (count=1; count<=t; count++) {
                document.getElementById("workDetail"+count).value = document.getElementById("workDetail"+count).value.replace(/<h[1-9]*>|<[/]h[1-9]*>|<p(\s|[a-z]|[0-9]|['".=+-]){1,}>|<section(\s|[a-z]|[0-9]|['".=+-]){1,}>|<div(\s|[a-z]|[0-9]|['".=+-]){1,}>|<p>|<section>|<div>|<[/]p>|<[/]section>|<[/]div>/gi, ' ');
                document.getElementById("workDetail"+count).value = document.getElementById("workDetail"+count).value.replace("^^div$$", "^-^div$-$");
                content+=document.getElementById("workDetail"+count).value+"^^div$$";
                assign+= document.getElementById("workEmp"+count).value+";";
                stat+="000;";
            }
            $.post("dependences/2.php?encrypt="+encrypt, {
                isEvent: 0,
                title: title,
                start: start,
                end: end,
                content: content,
                attend: assign,
                stat: stat
            }, function(data) {
                $("#ajaxShow").html(data);
            });
            $("#workStart,#workTitle,#workEnd").val('');
            for (count=1; count<=t; count++) {
                $("#workDetail"+count).val('');
                $("#workEmp"+count).val('');
                if (count!=1) {
                    delTaskDetail(count);
                }
            }
            $(".popup, #popup-bg").hide(200);
        }

    });*/
// TASKS QUERIES end
};
$(document).ready(global);

// Add Tasks type switch
function taskTypeSwitch() {
    if($("#taskType").val()==1) {
        $("#eventFrm").show(100);
        $("#workFrm").hide(100).html('<b>Initializing . . .');
    }
    else {
        $("#eventFrm").hide(100);
        $("#workFrm").show(100).load("dependences/workForm.php?encrypt="+encrypt);
    }
}

// Event handler
var allDay=0;
var sameDay=0;
function eventAllDay() {
    if (document.getElementById("allDay").innerHTML=="Set time for event") {
        document.getElementById("allDay").innerHTML = "All day event";
        $("#startTime, #endTime").show(50);
        $("#allDayCaption").html('-');
        allDay=0;
    } else {
        document.getElementById("allDay").innerHTML = "Set time for event";
        $("#allDayCaption").html('All day');
        $("#startTime").hide(50);
        $("#endTime").hide(50);
        document.getElementById("startTime").value = "00:01 AM";
        document.getElementById("endTime").value = "11:59 PM";
        allDay=1
    }
}
function eventSameDay() {
    if (document.getElementById("sameDay").innerHTML=="Another day") {
        document.getElementById("sameDay").innerHTML = "Same day";
        $("#endDate").show(50);
        allDay=0;
    } else {
        document.getElementById("sameDay").innerHTML = "Another day";
        $("#endDate").val($("#startDate").val()).hide(50);
        allDay=1;
    }
}

// Post content backup - Backup + set variable
var postBackup = "";
function getBackupPost() {
    postBackup = document.getElementById("discussPost").innerText;
}
function restorePost() {
    document.getElementById("discussPost").innerText = postBackup;
}
/*$(document).ready(function() {
    $("#discussPost").keypress(function () {
        postBackup = $("#discussPost").html();
    }).focus(function () {
        alert(postBackup);
        $("#discussPost").html(postBackup);
    });
});*/
