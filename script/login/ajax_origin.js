// JavaScript Document
ajaxLoader(1);
function ajaxLoader(i) {
	var loadFile = "dependences/"+i+".php?encrypt="+encrypt;
	$("#ajaxShow").load(loadFile);
	var loadSubMenu = "dependences/sub-menu.php?encrypt="+encrypt+"&part="+i;
	$("#sub-menu").load(loadSubMenu);
}

function ajaxSearch(p) {
	if(document.getElementById("search-master").value != '') {
		document.getElementById("search-result").innerHTML = 'Searching . . .';
		p = document.getElementById("search-master").value;
		// ajax something ...
		var loadResult = "dependences/search.php?encrypt="+encrypt+"?search="+p+"?people=0";
		$("#search-result").load(loadResult);
	} else {
		document.getElementById("search-result").innerHTML = 'What do you want to search?<p>You can use this to seach for people information or an event</p>';
	}
}