// JavaScript Document

//jQuery UI Showcase
$(function() {
    $( document ).tooltip({
        show: {
            effect: "slideDown",
            delay: 250
        },
        track: true
    });
});
$(function() {
    $("#startDate, #endDate, #workStart, #workEnd").datepicker({
        changeMonth: true,
        changeYear: true,
        minDate: "-7D",
        dateFormat: "dd-mm-yy",
        firstDay: 1
    });
    $("#empDOB").datepicker({
        changeMonth: true,
        changeYear: true,
        minDate: "-75Y",
        maxDate: "-16Y",
        dateFormat: "dd-mm-yy",
        firstDay: 1
    });
    $( ".popup:not(#empWorkChoose-popup)" ).draggable({cursor: "move", handle: "h3",containment: "document" });
    $('input[type="time"]').timepicker();
    $('.workTable .progress').slider();
    $('#calendar-home').datepicker({changeMonth: true, changeYear: true,firstDay: 1});
});

// CHECK_LIST jQuery
function todoStyle() {
    $("#todoScroll .todoItem:even").addClass("todoEven");
    $("#todoScroll .todoItem:odd").addClass("todoOdd");
}
todoStyle();
// CHECK_LIST end

// DISCUSS QUERIES -
function getComment(id) {
    var loadFile = "dependences/1.1.php?encrypt="+encrypt+"&postID="+id;
    $(".discuss-popup-scroll").html('Please wait...').load(loadFile);
    $('#comment').val('');
}
function editPost(id) {
    var loadFile = "dependences/1.2.php?encrypt="+encrypt+"&ID="+id;
    $(".discuss-edit-window").html('Please wait...').load(loadFile);
}
function delPost() {
    var c = confirm("The post will be completely deleted. This action cannot be undone.\nContinue?");
    if (c){
        var loadFile = "dependences/1.2.php?encrypt="+encrypt+"&del";
        $(".discuss-edit-window").load(loadFile);
        ajaxLoader(1);
    }
}
function delComment(id) {
    var c = confirm("You are about to delete this comment. This cannot be undone.\nContinue?");
    if (c){
        var loadFile = "dependences/1.1.php?encrypt="+encrypt+"&delCmtID="+id;
        $(".discuss-popup-scroll").load(loadFile);
    }
}
function changePost() {
    var post = $("#postContent").val();
    post = post.replace(/\r\n|\n|\r/g, '<br />');
    post = post.replace(/<h[1-9]*>|<[/]h[1-9]*>|<p(\s|[a-z]|[0-9]|['".=+-]){1,}>|<section(\s|[a-z]|[0-9]|['".=+-]){1,}>|<div(\s|[a-z]|[0-9]|['".=+-]){1,}>|<p>|<section>|<div>|<[/]p>|<[/]section>|<[/]div>/gi, ' ');
    if (document.getElementById('isPinned-change') == null) {
        var pin=0;
    }
    else {
        if (document.getElementById('isPinned-change').checked) {
            var pin = 1;
        } else {
            var pin = 0;
        }
    }
    if (post=="" || post.length < 10) {
        alert("Don't post a blank or too short discussions!\nAnd don't post an unchanged discussion, too.");
    } else {
        var loadFile = "dependences/1.2.php?encrypt="+encrypt+"&change";
        $.post("dependences/1.2.php?encrypt="+encrypt, {
            change: post,
            pin: pin
        }, function () {
            ajaxLoader(1);
        });
        $(".discuss-edit-window").html('Please wait...').load(loadFile);
    }
}
function sendCmt() {
    var comment = $("#comment").val();
    comment = comment.replace(/\r\n|\n|\r/g, ' . ');
    comment =comment.replace(/<h[1-9]*>|<[/]h[1-9]*>|<p(\s|[a-z]|[0-9]|['".=+-]){1,}>|<section(\s|[a-z]|[0-9]|['".=+-]){1,}>|<div(\s|[a-z]|[0-9]|['".=+-]){1,}>|<p>|<section>|<div>|<[/]p>|<[/]section>|<[/]div>/gi, ' ');
    if (comment=="" || comment==" " || comment.length < 2 || comment.length > 255) {
        alert("Don't send too short or too long comment!\n(2 - 255 characters)");
    } else {
        var loadFile = "dependences/1.1.php?encrypt="+encrypt+"&addCmt";
        $.post("dependences/1.1.php?encrypt="+encrypt, {
            addCmt: comment
        });
        $(".discuss-popup-scroll").load(loadFile);
        $('#comment').val('');
    }
}
function sendPost() {
    var post = $("#discussPost").val();
    post = post.replace(/\r\n|\n|\r/g, '<br />');
    post = post.replace(/<h[1-9]*>|<[/]h[1-9]*>|<p(\s|[a-z]|[0-9]|['".=+-]){1,}>|<section(\s|[a-z]|[0-9]|['".=+-]){1,}>|<div(\s|[a-z]|[0-9]|['".=+-]){1,}>|<p>|<section>|<div>|<[/]p>|<[/]section>|<[/]div>/gi, ' ');
    if (document.getElementById('pinnedChk') == null) {
        var pin=0;
    }
    else {
        if (document.getElementById('pinnedChk').checked) {
            var pin = 1;
        } else {
            var pin = 0;
        }
    }
    if (post=="" || post.length < 10) {
        alert("Don't post a blank or too short discussions!");
    } else {
        $("#post-btn").val("Please wait");
        document.getElementById("post-btn").disabled = true;
        $.post("dependences/1.3.php?encrypt="+encrypt, {
            post: post,
            pin: pin
        }, function() {
            ajaxLoader(1);
        });
        postBackup = "";
        ajaxLoader(1);
    }
}
function loadMorePost(pg,filter) {
    var loadPost = "dependences/1.php?encrypt="+encrypt+"&pg="+pg+"&"+filter;
    $("#ajaxShow").html("Loading...").load(loadPost);
}
function slMorePost(filter) {
    var pg = $("#pageSelect").val();
    var loadPost = "dependences/1.php?encrypt="+encrypt+"&pg="+pg+"&"+filter;
    $("#ajaxShow").html("Loading...").load(loadPost);
}
// DISCUSS QUERIES end

// TASKS QUERIES -
    // Sending function Available in GLOBAL.JS
function delItem(id) {
    c = confirm("You are about to remove this item, this can't be undone. Do you want to continue?");
    if (c) {
        $("#ajaxShow").html("Working...");
        $.post("dependences/2.php?encrypt="+encrypt, {
            delItem: 1,
            agdID: id
        }, function(data) {
            $("#ajaxShow").html(data);
        });
    }
}
function workUpdate(id,part,raw) {
    var chk = /^[0-9]{1,3}$/;
    var val = document.getElementById("workUpdate-frm-"+id+part).value;
    if (chk.test(val)==false || val<0 || val>100) {
        alert("Progress percentage must be a number between 0 and 100");
    } else {
        switch (val.length) {
            case 1:
                val = "00"+val;
                break;
            case 2:
                val = "0"+val;
        }
        var loadFile = "dependences/2.php?encrypt="+encrypt+"#"+id;
        $("#ajaxShow").html("Working...");
        $.post(loadFile, {
            updateWork: 1,
            agdID: id,
            stat: raw,
            part: part,
            value: val
        }, function(data) {
            $("#ajaxShow").html(data);
        });
        document.getElementById(id).focus();
    }
}
// TASKS QUERIES end

// PEOPLE QUERIES -
function getPeopleSearch(id) {
    var loadFile = "dependences/3.php?encrypt="+encrypt+"&empID="+id;
    $("#ajaxShow").load(loadFile);
}
function getPeople(id) {
    var loadFile = "dependences/3.1.php?encrypt="+encrypt+"&empID="+id+"&noSearch";
    $("#people-description").html("Loading...").load(loadFile);
}
function peopleSearchBox() {
    var search = $("#peopleSearch").val();
    $("#people-filter-byBranch").val(0);
    $("#people-filter-byDept").val(0);
    $("#people-filter-byPos").val(0);
    if (search!="") {
        $.post("dependences/3.2.php?encrypt="+encrypt, {
            q: search
        }, function(data) {
            $("#people-filter").html(data);
        });
    } else {
        var loadFile = "dependences/3.2.php?encrypt="+encrypt+"&all";
        $("#people-filter").load(loadFile);
    }
}
function peopleFilter() {
    $("#peopleSearch").val('');
    var posID = $("#people-filter-byPos").val();
    var deptID = $("#people-filter-byDept").val();
    var branchID = $("#people-filter-byBranch").val();
    var loadFile = "dependences/3.2.php?encrypt="+encrypt+"&posID="+posID+"&deptID="+deptID+"&branchID="+branchID;
    $("#people-filter").load(loadFile);
}
// PEOPLE QUERIES end

// SUPPORT QUERIES -
function sendRequest() {
    var error = "";
    var type = $("#reqType").val();
    var content = tinyMCE.get('reqContent').getContent();
    var stat = 1;
    if (document.getElementById("reqStatHigh").checked) {
        stat = 2;
    }
    if (document.getElementById("reqStatUrgent").checked) {
        stat = 3;
    }
    if (type == 0) {
        error += "\nChoose a request type";
    }
    if (content.length < 10) {
        error += "\nContent too short";
    }
    if (error!="") {
        alert("Error:"+error);
    } else {
        $("#ajaxShow").html("<h3>Sending...</h3>");
        $.post("dependences/4.php?encrypt="+encrypt, {
            reqTypeID: type,
            reqContent: content,
            reqStat: stat
        }, function(data) {
            $("#ajaxShow").html(data);
        });
    }
}
// SUPPORT QUERIES end

// USER PAGE QUERIES -
function changePasscode() {
    var error="";
    var regexPasscode = /^[0-9]{4,}$/;
    if (document.getElementById("empPasscode_old").value.length<4 || document.getElementById("empPasscode_old").value.length>8) {
        error += "Wrong Current passcode\n";
    }
    if (document.getElementById("empPasscode").value.length<4 || document.getElementById("empPasscode").value.length>8) {
        error += "Passcode must be between 4 and 8 digits\n";
    }
    if (document.getElementById("empPasscode").value != document.getElementById("empPasscode_re").value) {
        error += "New Passcode mismatch\n";
    }
    if (regexPasscode.test(document.getElementById("empPasscode").value)==false || regexPasscode.test(document.getElementById("empPasscode_re").value)==false || regexPasscode.test(document.getElementById("empPasscode_old").value)==false) {
        error += "Wrong passcode format\n";
    }
    if (document.getElementById("empPasscode_old").value == document.getElementById("empPasscode").value) {
        error += "New passcode must be different from old one\n";
    }
    if (error=="") {
        $("#ajaxShow").html('Working...');
        $.post("dependences/usr-page.php?encrypt="+encrypt+"&changePasscode", {
            empPasscode_old: document.getElementById("empPasscode_old").value,
            empPasscode: document.getElementById("empPasscode").value,
            empPasscode_re: document.getElementById("empPasscode_re").value
        }, function(data) {
            $("#ajaxShow").html(data);
        });
    } else {
        alert("Some errors occured while changing your passcode:\n\n"+error);
    }
}
function changePassword() {
    var c = confirm("You are going to change your password.\nDo you want to continue?");
    if (c) {
        $("#ajaxShow").html('Working...');
        $.post("dependences/usr-page.php?encrypt="+encrypt+"&changePswd", {
            changePswd: 1
        });
        ajaxLoader('usr-page');
    }
}
var imgUploadMode = 0;
$("#btnUploadURL").hide();
$("#empPicURL").hide();
function imgReset() {
    var c = confirm("Are you sure to change back your current picture to default picture?");
    if (c) {
        $("#ajaxShow").html('Working...');
        $.post("dependences/usr-page.php?encrypt="+encrypt, {
            removePic: 1
        }, function(data) {
            $("#ajaxShow").html(data);
        });
    }
}
// Mode = 1 -> Upload URL; Mode = 2 || 0 -> Upload image file
function uploadImgURL() {
    var urlRegex = /^(?:http|https|ftp|ftps|sftp):([0-9]|[a-z])*\/\/?(?:([^/?#]*))?([^?#]*.(?:jpg|gif|png))(?:\?([^#]*))?(?:#(.*))?/i;
    var url = document.getElementById("empPicURL").value;
    var error = "";
    if (url=="") {
        error += ' - URL should not be blank\n';
    }
    if (!urlRegex.test(url)) {
        error += ' - Wrong URL format (Accept protocols HTTP, HTTPS, FTP, FTPS, SFTP)\n';
    }
    if (error!="") {
        alert("There are some problem:\n\n"+error+"\nPlease check and try again!");
    } else {
        $("#ajaxShow").html('Working...');
        $.post("dependences/usr-page.php?encrypt="+encrypt, {
            changePic: 1,
            picURL: url
        }, function(data){
            $("#ajaxShow").html(data);
        });
    }
}
//$("#changeInfo").click(
function changeInfo() {
    var DOB = $("#empDOB").val();
    var IDcard = $("#empIDcard").val();
    var phone = $("#empPhone").val();
    var mob = $("#empMob").val();
    var skype = $("#empSkype").val();
    var email = $("#empBackupEmail").val();
    var mailchk = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    var phonechk = /^([0-9]|[-+. ]){8,}$/;
    var skypechk = /^(?:[a-zA-Z]|live:)([a-zA-Z0-9]|[_\-,.@]){5,31}$/;
    var error = "";
    var date = new Date();
    var today = date.getDate();
    if (DOB=="") {
        error+=" - Date of birth cannot be blank\n";
    }else {
        if (today-DOB>2365000000 || today-DOB<504576000) {
            error+=" - Your age must between 16 and 75\n";
        }
    }
    if (!document.getElementById("empGen1").checked && !document.getElementById("empGen2").checked && !document.getElementById("empGen3").checked) {
        error+=" - Please choose your gender\n";
    }
    var gen = 0;
    if (document.getElementById("empGen1").checked) gen=1;
    if (document.getElementById("empGen2").checked) gen=2;
    if (document.getElementById("empGen3").checked) gen=3;
    if (IDcard.length<8 && (IDcard.length!=0 || IDcard!="")) {
        error+=" - Please enter a valid ID card number\n";
    }
    if (!phonechk.test(phone) && (phone.length!=0 || phone!="")) {
        error+=" - Please enter a valid phone number\n";
    }
    if (!phonechk.test(mob) && (mob.length!=0 || mob!="")) {
        error+=" - Please enter a valid mobile number\n";
    }
    if (!skypechk.test(skype) && (skype.length!=0 || skype!="")) {
        error+=" - Please enter a valid Skype name\n";
    }
    if (!mailchk.test(email) && (email.length!=0 || email!="")) {
        error+=" - Please enter a valid email address";
    }
    if (error!="") {
        alert ("There are some errors:\n\n"+error);
    } else {
        $("#ajaxShow").html('Working...');
        $.post("dependences/usr-page.php?encrypt="+encrypt+"&editInfo=1", {
            empDOB: DOB,
            empGen: gen,
            empIDcard: IDcard,
            empPhone: phone,
            empMob: mob,
            empSkype: skype,
            empBackupEmail: email
        }, function(data) {
            $("#ajaxShow").html(data);
        });
    }
}
// USER PAGE QUERIES end

function loadUsr (part) {
    var loadFile = "dependences/usr-page.php?encrypt="+encrypt+"&part="+part;
    $("#ajaxShow").html('Loading...').load(loadFile);
}

// Main content
var main = function() {

	// Pop-ups controlling
	$(".comment-btn").click(function() {
		$("#discuss-popup-comment, #popup-bg").show(200);
	});
	$(".save-btn").click(function() {
		$("#discuss-popup-save, #popup-bg").show(200);
	});
	$(".close-popup, #popup-bg").click(function() {
		$(".popup, #popup-bg").hide(200);
        $("#workFrm").val('');
        $("#taskType").val(1);
        taskTypeSwitch();
	});
	
	$("#agenda-add-event").click(function() {
		$("#agenda-popup, #popup-bg").show(200);
	});

    // User Change password
    $(".changePassContent").hide();
    $("#changePassToggle").click(function() {
        $(".changePassContent").show(250);
        $("#changePassToggle").hide(250);
    });

	$("#scroll-area-discuss").height($(document).height()*0.65);
	$("#scroll-area-event").height($(document).height()*0.80);
	
	$(".close-popup").click(function() {
		$(".popup, #popup-bg").hide(200);
	});
	
	$(".col").css("height", screen.height * 0.71);

    $("#appinfo tr").hide();
    $("#appinfoBtn").show().click(function () {
        $("tr:not(#appinfoBtn)").show(150);
    });

    // Tasks table styling
    $(".workTable tr:even").css("background", "#FFA54C");
};
$(document).ready(main);

function loadPHPinfo() {
    var loadFile = "dependences/4.php?encrypt="+encrypt+"&phpinfo=1";
    $("#ajaxShow").load(loadFile);
}

// Tasks Filter
function taskFilter() {
    switch (document.getElementById("agenda-filter-type").value) {
        case "all":
            $(".taskWork, .taskEvent").show(50);
            break;
        case "event":
            $(".taskEvent").show(50);
            $(".taskWork").hide(50);
            break;
        case "work":
            $(".taskEvent").hide(50);
            $(".taskWork").show(50);
            break;
    }
}
