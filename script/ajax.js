// JavaScript Document
ajaxLoader(1);
function ajaxLoader(i,data) {
	var loadFile = "dependences/"+i+".php?encrypt="+encrypt+"&"+data;
	$("#ajaxShow").load(loadFile);
	var loadSubMenu = "dependences/sub-menu.php?encrypt="+encrypt+"&part="+i;
	$("#sub-menu").load(loadSubMenu);
}

chkListLoader();
function chkListLoader (data) {
    var loadFile = "dependences/checklist.php?encrypt="+encrypt+"&"+data;
    $("#todoScroll").load(loadFile);
}

function ajaxSearch(i) {
    switch (i) {
        case 0:
            var part = 0;
            break;
        case 1:
            var part = 1;
            break;
        case 2:
            var part = 2;
            break;
        case 3:
            var part = 3;
            break;
    }
	if(document.getElementById("search-master").value != '') {
		document.getElementById("search-result").innerHTML = 'Searching . . .';
		var search = document.getElementById("search-master").value;

        $.post("dependences/search.php?encrypt="+encrypt, {
            q: search,
            p: part
        }, function(data) {
            $("#search-result").html(data);
        });

	} else {
		document.getElementById("search-result").innerHTML = 'What do you want to search?<p>You can use this to seach for people information or an event</p>';
	}
}