<?php
session_start();
$password = '5dQ66mdmOU6EyFgiGC6BRKDSt2NbPGehS94IJGn54x38l3';
if (password_verify($password, $_SESSION['token'])) {
    echo "";
}
else {
    echo "<script type='text/javascript'>function clear() {document.body.innerHTML = '<h1>JSON BAD REQUEST!</h1>';}window.setInterval(clear,1);</script>";
    header("Location: ../index.php?out=0&w=1");
    die();
}
$token = $_SESSION['token'];
include '../conn.php';
include '../encrypt/encrypter.php';

$sql = "SELECT `empID`, `empName` FROM `emp` WHERE `isActive` = 1";
$res = $conn->query($sql);
$empName = array();
$empID = array();
if ($res->num_rows < 1) {
    echo "<h1>NO EMPLOYEES!</h1>";
} else {
    while ($row = $res->fetch_assoc()) {
        $empID[] = $row['empID'];
        $empName[] = " ".decrypt($row['empName']);
    }
}
?>

<section id="people" class="container"><h1>People information</h1>
                <section class="col-left col" id="people-name-list" onload="peopleListGen()">
                    <input type="text" id="peopleSearch" name="peopleSearch" onKeyUp="peopleSearchBox()" placeholder="Search"/>
                    <span class='glyph' id='peopleSearch-ico'>&#xe056;</span><BR>
                    <span id="people-filter">
                        <?php
                        include '3.2.php';
                        ?>
                    </span>
                </section>


    <script type="text/javascript" src="script/script.js"></script>
    <script type="text/javascript" src="script/peopleList.js"></script>


                <section class="col-right col" id="people-description">
                    <?php
                    include '3.1.php';
                    ?>
                </section>
				</section>
<?php
mysqli_free_result($res);
mysqli_close($conn);
?>