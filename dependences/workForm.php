<?php
session_start();
$password = '5dQ66mdmOU6EyFgiGC6BRKDSt2NbPGehS94IJGn54x38l3';
if (password_verify($password, $_SESSION['token'])) {
    echo "";
}
else {
    echo "<script type='text/javascript'>function clear() {document.body.innerHTML = '<h1>JSON BAD REQUEST!</h1>';}window.setInterval(clear,1);</script>";
    header("Location: ../index.php?out=0&w=1");
    die();
}
$token = $_SESSION['token'];
include '../conn.php';
include '../encrypt/encrypter.php';


?>

From: <input type="text" name="workStart" id="workStart" class="agdCalendar" placeholder="dd/mm/yyyy"> &nbsp;&nbsp;&nbsp;
To:   <input type="text" name="workEnd" id="workEnd" class="agdCalendar" placeholder="dd/mm/yyyy"><BR>
Title: &nbsp;<input type="text" name="workTitle" id="workTitle" style="width: 75%; margin-top: 3px;"><BR><BR>
Work content:<BR>
<section style="overflow: auto; height: 220px;">
    <table id="workContent" width="100%" border="0">
        <tr><th width="5%">No.</th><th width="60%">Parts of work</th><th width="30%">Assignment</th></tr>
        <tr>
            <th width="5%">1</th>
            <td width="60%">
                <!--input type="text" name="workDetail" id="workDetail1" placeholder="Details"-->
                Click "Add more" button to add a row
            </td>
            <td width="30%">
                &nbsp;<!--input type="text" name="workEmp" onclick="openEmpWorkPopup(1)" id="workEmp1" placeholder="Employee" readonly-->
            </td>
            <td><!--span class="glyph" onclick="delTaskDetail(1)">&#xe082;</span--> &nbsp;</td>
        </tr>
    </table>
</section>
<button onclick="addTaskDetail()"><span class="glyph">&#xe081;</span> Add more</button>
<button id="workSubmit" style="float: right"><span class="glyph">&#xe172;</span> Save</button>
<div class="clear"></div>

<script type="text/javascript">
    $(function() {
        var empList = [
            <?php
            $q = "SELECT emp.`empID`, `empName`, `posName`, `deptName`, `branchName` FROM `emp` JOIN `emppos` ON emp.empID = emppos.empID JOIN `position` ON emppos.posID = position.posID JOIN `department` ON emp.deptID = department.deptID JOIN `branch` ON emp.branchID = branch.branchID WHERE emp.`isActive` = 1 GROUP BY `empName`, `posName`, `deptName`, `branchName`";
            $r = $conn->query($q);
            if ($r->num_rows>0) {
                while($row = $r->fetch_assoc()) {
                    echo '{ value: "'.$row['empID'].' | '.'",';
                    echo 'label: "'.decrypt($row['empName']).'",';
                    echo 'desc: "'.decrypt($row['posName']).' in '.decrypt($row['deptName']).' at '.decrypt($row['branchName']).'" },';
                    //echo '"'.$row['empID'].' | '.decrypt($row['empName']).'  ['.decrypt($row['posName']).' in '.decrypt($row['deptName']).' at '.decrypt($row['branchName']).']",';
                }
            }
            ?>
            ".."
        ];

        $( "#workEmpChoose" ).autocomplete({
            minLength: 0,
            source: empList,
            focus: function( event, ui ) {
                $( "#project" ).val( ui.item.label );
                return false;
            },
            select: function( event, ui ) {
                $( "#workEmpChoose" ).val( ui.item.label );
                $( "#workEmpChoose-id" ).val( ui.item.value );
                $( "#workEmpChoose-description" ).html( ui.item.desc );
                return false;
            }
        }).autocomplete( "instance" )._renderItem = function( ul, item ) {
            return $( "<li>" )
                .append( "<a>" + item.label + "<br>" + item.desc + "</a>" )
                .appendTo( ul );
        };
    });

    // send work
    $("button#workSubmit").click(function() {
        var start = $("#workStart").val()+" 00:00:01";
        var end = $("#workEnd").val()+" 23:59:59";
        var title = $("#workTitle").val();
        title = title.replace(/<h[1-9]*>|<[/]h[1-9]*>|<p(\s|[a-z]|[0-9]|['".=+-]){1,}>|<section(\s|[a-z]|[0-9]|['".=+-]){1,}>|<div(\s|[a-z]|[0-9]|['".=+-]){1,}>|<p>|<section>|<div>|<[/]p>|<[/]section>|<[/]div>/gi, ' ');
        var error="";
        var timechk = /^[0-9]{1,2}-[0-9]{1,2}-[0-9]{4}/;
        if (timechk.test(start)==false || timechk.test(end)==false) {
            error+=" - Invalid date format\n";
        }
        if (title.length<5 || title.length>64) {
            error+=" - Title length should between 5 and 64 characters\n";
        }
        for (var count=1; count<=t; count++) {
            if (document.getElementById("workDetail"+count).value=="" || document.getElementById("workDetail"+count).value.length<5 || document.getElementById("workDetail"+count).value>64 || document.getElementById("workEmp"+count).value=="") {
                error+=" - Fill all fields in parts of work and employees assignment";
                break;
            }
        }
        if (error!="") {
            alert("There are some problems with your submission:\n\n"+error);
        } else {
            var content="^^div$$";
            var assign=";";
            var stat = ";";
            for (count=1; count<=t; count++) {
                document.getElementById("workDetail"+count).value = document.getElementById("workDetail"+count).value.replace(/<h[1-9]*>|<[/]h[1-9]*>|<p(\s|[a-z]|[0-9]|['".=+-]){1,}>|<section(\s|[a-z]|[0-9]|['".=+-]){1,}>|<div(\s|[a-z]|[0-9]|['".=+-]){1,}>|<p>|<section>|<div>|<[/]p>|<[/]section>|<[/]div>/gi, ' ');
                document.getElementById("workDetail"+count).value = document.getElementById("workDetail"+count).value.replace("^^div$$", "^-^div$-$");
                content+=document.getElementById("workDetail"+count).value+"^^div$$";
                assign+= document.getElementById("workEmp"+count).value+";";
                stat+="000;";
            }
            $.post("dependences/2.php?encrypt="+encrypt, {
                isEvent: 0,
                title: title,
                start: start,
                end: end,
                content: content,
                attend: assign,
                stat: stat
            }, function(data) {
                $("#ajaxShow").html(data);
            });
            $("#workStart,#workTitle,#workEnd").val('');
            for (count=1; count<=t; count++) {
                $("#workDetail"+count).val('');
                $("#workEmp"+count).val('');
                if (count!=1) {
                    delTaskDetail(count);
                }
            }
            $(".popup, #popup-bg").hide(200);
            $("#taskType").val(2);
            taskTypeSwitch();
        }

    });

    // Event handler
    var i=1;
    var t;
    function addTaskDetail() {
        i+=1;
        var t_workContent = '<tr><th width="5%">No.</th><th width="60%">Parts of work</th><th width="30%">Assignment</th><th width="5%">&nbsp;</th></tr>';
        for (t=1; t<i; t++) {
            if (i==1) {
                t_workContent += '<tr> <th width="5%">'+t+'</th> <td width="50%"> <input type="text" name="workDetail" id="workDetail'+t+'" value="'+document.getElementById("workDetail"+t).value+'" style="width: 95%" placeholder="Details"> </td> <td width="30%"> <input type="text" name="workEmp" onclick="openEmpWorkPopup('+t+')" id="workEmp'+t+'" value="'+document.getElementById("workEmp"+t).value+'" style="width: 95%" placeholder="Employee" readonly> </td> <td>&nbsp;</td> </tr>';
            } else {
                t_workContent += '<tr> <th width="5%">'+t+'</th> <td width="50%"> <input type="text" name="workDetail" id="workDetail'+t+'" value="'+document.getElementById("workDetail"+t).value+'" style="width: 95%" placeholder="Details"> </td> <td width="30%"> <input type="text" name="workEmp" onclick="openEmpWorkPopup('+t+')" id="workEmp'+t+'" value="'+document.getElementById("workEmp"+t).value+'" style="width: 95%" placeholder="Employee" readonly> </td> <td><span class="glyph" onclick="delTaskDetail('+t+')" title="Delete this row">&#xe082;</span> </td> </tr>';
            }
        }
        document.getElementById("workContent").innerHTML = t_workContent + '<tr> <th width="5%">'+i+'</th> <td width="50%"> <input type="text" name="workDetail" id="workDetail'+i+'" value="" style="width: 95%" placeholder="Details" autofocus="true"> </td> <td width="30%"> <input type="text" name="workEmp" onclick="openEmpWorkPopup('+i+')" id="workEmp'+i+'" value="" style="width: 95%" placeholder="Employee" readonly> </td> <td><span class="glyph" onclick="delTaskDetail(t)" title="Delete this row">&#xe082;</span> </td> </tr>';
        $("table tr:odd").css("background","#fff");
    }
    function delTaskDetail(num) {
        if (i!=1) {
            var t_workContent = '<tr><th width="5%">No.</th><th width="60%">Parts of work</th><th width="30%">Assignment</th><td width="5%">&nbsp;</td></tr>';
            for (t=1; t<num; t++) {
                if (i==1) {
                    t_workContent += '<tr> <th width="5%">'+t+'</th> <td width="60%"> <input type="text" name="workDetail" id="workDetail'+t+'" value="'+document.getElementById("workDetail"+t).value+'" style="width: 95%" placeholder="Details"> </td> <td width="30%"> <input type="text" name="workEmp" onclick="openEmpWorkPopup('+t+')" id="workEmp'+t+'" value="'+document.getElementById("workEmp"+t).value+'" style="width: 95%" placeholder="Employee"> </td> <td width="5%">&nbsp;</td> </tr>';
                } else {
                    t_workContent += '<tr> <th width="5%">'+t+'</th> <td width="60%"> <input type="text" name="workDetail" id="workDetail'+t+'" value="'+document.getElementById("workDetail"+t).value+'" style="width: 95%" placeholder="Details"> </td> <td width="30%"> <input type="text" name="workEmp" onclick="openEmpWorkPopup('+t+')" id="workEmp'+t+'" value="'+document.getElementById("workEmp"+t).value+'" style="width: 95%" placeholder="Employee"> </td> <td><span class="glyph" title="Delete this row" onclick="delTaskDetail('+t+')">&#xe082;</span> </td> </tr>';
                }
            }
            for (var t1=num; t1<i; t1++) {
                t++;
                t_workContent += '<tr> <th width="5%">'+t1+'</th> <td width="60%"> <input type="text" name="workDetail" id="workDetail'+t1+'" value="'+document.getElementById("workDetail"+t).value+'" style="width: 95%" placeholder="Details"> </td> <td width="30%"> <input type="text" name="workEmp" onclick="openEmpWorkPopup('+t1+')" id="workEmp'+t1+'" value="'+document.getElementById("workEmp"+t).value+'" style="width: 95%" placeholder="Employee"> </td> <td><span class="glyph" title="Delete this row" onclick="delTaskDetail('+t1+')">&#xe082;</span> </td> </tr>';
            }
            document.getElementById("workContent").innerHTML = t_workContent;
            $("table tr:odd").css("background","#fff");
            i-=1;
        }
    }
    var workItem = 0;
    function openEmpWorkPopup(i) {
        $("#empWorkChoose-popup").show(100);
        $("#workEmpChoose").val('').focus();
        //$("#popup-bg").show(100);
        workItem = i;
    }

</script>
				<script type="text/javascript" src="script/script.js"></script>
<?php
mysqli_free_result($r);
mysqli_close($conn);
?>