<?php
session_start();
$password = '5dQ66mdmOU6EyFgiGC6BRKDSt2NbPGehS94IJGn54x38l3';
if (password_verify($password, $_SESSION['token'])) {
    echo "";
}
else {
    echo "<script type='text/javascript'>function clear() {document.body.innerHTML = '<h1>JSON BAD REQUEST!</h1>';}window.setInterval(clear,1);</script>";
    session_destroy();
    header("Location: ../index.php?out=0&w=1");
    die();
}
$token = $_SESSION['token'];
include '../conn.php';
include '../encrypt/encrypter.php';

if ($_SERVER['REQUEST_METHOD']=='POST') {
    if (isset($_GET['changePasscode'])) {
        $empPasscode = decrypt($_SESSION['empPasscode']);
        if ($_POST['empPasscode']==$_POST['empPasscode_re'] && $_POST['empPasscode_old']==$empPasscode && $empPasscode!=$_POST['empPasscode']) {
            $sql = "UPDATE `emp` SET `empPasscode` = '".encrypt($_POST['empPasscode'])."' WHERE `empID` = ".$_SESSION['empID'];
            $res = $conn->query($sql);
            echo "<script type='text/javascript'>alert('Passcode changed successfully.')</script>";
            $_SESSION['empPasscode'] = encrypt($_POST['empPasscode']);
        } else {
            echo "<script type='text/javascript'>alert('Changing passcode Failed\\nWrong or mismatch Passcode!')</script>";
        }
    }
    elseif (isset($_GET['editInfo'])) {
        //if ($_POST['editInfo']==1) {
            $edit_sql = "UPDATE `emp` SET `empDOB` = '".date('Y-m-d', strtotime($_POST['empDOB']))."', `empGen` = '".$_POST['empGen']."'";
            if ($_POST['empIDcard']!="") $edit_sql  = $edit_sql .", `empIDcard` = '".encrypt($_POST['empIDcard'])."'";
            if ($_POST['empPhone']!="") $edit_sql  = $edit_sql .", `empPhone` = '".encrypt($_POST['empPhone'])."'";
            if ($_POST['empMob']!="") $edit_sql  = $edit_sql .", `empMob` = '".encrypt($_POST['empMob'])."'";
            if ($_POST['empSkype']!="") $edit_sql  = $edit_sql .", `empSkype` = '".encrypt($_POST['empSkype'])."'";
            if ($_POST['empBackupEmail']!="") $edit_sql  = $edit_sql .", `empBackupEmail` = '".encrypt($_POST['empBackupEmail'])."'";
            $edit_sql = $edit_sql ." WHERE `empID` = ".$_SESSION['empID'];
            $response = $conn->query($edit_sql);
        //echo "<script>alert('OK\\n'+".$edit_sql.");</script>";
        //}
    }
    elseif (isset($_POST['changePswd'])) {
        if ($_POST['changePswd']==1) {
            $sql = "UPDATE `emp` SET `setupStat` = '3' WHERE `empID` = ".$_SESSION['empID'];
            $res = $conn->query($sql);
            echo "<script type='text/javascript'>alert('Your password will be changed in the next login session.\\n\\nWhat to do?\\n1. Logut\\n2. Login with this account\\n3. Enter the new password')</script>";
        }
    }
    elseif (isset($_POST['changePic'])) {
        if (isset($_POST['picURL'])) {
            if (!empty($_POST['picURL']) || $_POST['picURL']!="") {
                $sql = "UPDATE `emp` SET `empPic` = '".$_POST['picURL']."' WHERE `emp`.`empID` = ".$_SESSION['empID'];
                $res = $conn->query($sql);
                $_SESSION['empPic'] = $_POST['picURL'];
                echo "<script type='text/javascript' src='script/ajax.js'></script> <script type='text/javascript'>ajaxLoader('usr-page');</script>";
            } else {
                echo "<script type='text/javascript'>alert('URL could not be blank.')</script>";
            }
        }
    }
    elseif (isset($_POST['removePic'])) {
        if ($_POST['removePic']==1) {
            $removePic_sql = "UPDATE `emp` SET `empPic` = '' WHERE `empID` = ".$_SESSION['empID'];
            $removePicEXEC = $conn->query($removePic_sql);
            $sql = "SELECT `value` FROM `setting` WHERE `prop` = 'empPic'";
            $exec = $conn->query($sql);
            $result_empPic = $exec->fetch_assoc();
            $_SESSION['empPic'] = $result_empPic['value'];
            echo "<script type='text/javascript' src='script/ajax.js'></script> <script type='text/javascript'>ajaxLoader('usr-page');</script>";
        }
    }
    else {
        echo "";
    }
}

$q = "SELECT empNo, empPswd, empPic, empGen, empDOB, empIDcard, empPhone, empMob, empSkype, setupStat, empBackupEmail, branchName FROM `emp` JOIN `branch` ON emp.branchID = branch.branchID WHERE empID = '{$_SESSION['empID']}'";
$result = $conn->query($q);
$row = $result->fetch_assoc();
$q2 = "SELECT posName FROM `emppos` JOIN `position` ON emppos.posID = position.posID WHERE empID = '{$_SESSION['empID']}'";
$result2 = $conn->query($q2);
$q3 = "SELECT deptName FROM `emp` JOIN `department` ON emp.deptID = department.deptID WHERE empID = '{$_SESSION['empID']}'";
$result3 = $conn->query($q3);
$row3 = $result3->fetch_assoc();

?>

<!-- User's personal page -->

<h1><?php echo decrypt($_SESSION['usrDetail']['empName'])."<i><span style='text-transform: none; font-size: smaller; font-weight: 100; padding:20px'>".$_SESSION['usr']."</span></i>"; ?></h1>

<section id="usr-pg">
    <section class="col" style="width: 50%">
        <form action="#" method="post" name="changePswd" id="changePswd">
            <table width="100%" border="0">
                <tr>
                    <td width="25%">Emp No.</td>
                    <td>
                        <?php echo $row['empNo']; ?>
                    </td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td>
                        <?php echo $_SESSION['usr'] ?>
                    </td>
                </tr>
                <tr>
                    <td>Branch</td>
                    <td>
                        <?php echo decrypt($row['branchName']); ?>
                    </td>
                </tr>
                <tr>
                    <td>Department</td>
                    <td>
                        <?php echo decrypt($row3['deptName']); ?>
                    </td>
                </tr>
                <tr>
                    <td>Position</td>
                    <td>
                        <?php
                        $count = 0;
                        while ($row2 = $result2->fetch_assoc()) {
                            echo decrypt($row2['posName'])."<BR>";
                            $count++;
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>Password</td>
                    <td>
                        <?php
                        if ($row['setupStat'] == 0) {
                            echo "<input type='button' onclick=\"changePassword()\" value=' Change Password '>";
                        } else {
                            echo "<span style='color: #843534; font-weight: bold;'><span class='glyph'>&#xe107;</span>Your password will be change.<BR>You will be asked to change your password in the next login.</span>";
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td><span style="text-decoration: underline;" title="A code that you use to exit the temporary lock mode. (4 to 8 digits)">Passcode</span></td>
                    <td><input type="button" name="changePass" id='changePassToggle' value=" Change Passcode ">
                        <input type="password" name="empPasscode" id="empPasscode_old" class="changePassContent" maxlength="8" placeholder="Current Passcode">
                        <input type="password" name="empPasscode" id="empPasscode" class="changePassContent" maxlength="8" placeholder="New Passcode">
                        <input type="password" name="empPasscode" id="empPasscode_re" class="changePassContent" maxlength="8" placeholder="Re-type Passcode">
                        <input type="button" name="changePass" onclick="changePasscode()" class="changePassContent" value=" Change Passcode ">
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>

                    </td>
                </tr>
            </table>
        </form>
        <HR>
            <table width="100%" border="0">
                <tr>
                    <td>Name</td>
                    <td>
                        <?php echo decrypt($_SESSION['empName']); ?>
                    </td>
                </tr>
                <tr>
                    <td>Gender <span style="font-weight: bold; color: #ac2925">*</span></td>
                    <td>
                        <label><input type="radio" name="empGen" id="empGen1" <?php if($row['empGen']==1) echo "checked"; ?>>Male</label>&nbsp;&nbsp;&nbsp;&nbsp;
                        <label><input type="radio" name="empGen" id="empGen2" <?php if($row['empGen']==2) echo "checked"; ?>>Female</label>&nbsp;&nbsp;&nbsp;&nbsp;
                        <label><input type="radio" name="empGen" id="empGen3" <?php if($row['empGen']==3) echo "checked"; ?>>Other</label>
                    </td>
                </tr>
                <tr>
                    <td>Date of birth <span style="font-weight: bold; color: #ac2925">*</span></td>
                    <td>
                        <input type="text" name="empDOB" id="empDOB" value="<?php echo date("d/m/Y",strtotime($row['empDOB'])); ?>" placeholder="dd-mm-yyyy" readonly>
                    </td>
                </tr>
                <tr>
                    <td>ID card</td>
                    <td>
                        <input type="text" name="empIDcard" id="empIDcard" value="<?php if (!empty($row['empIDcard'])) echo decrypt($row['empIDcard']) ?>">
                    </td>
                </tr>
                <tr>
                    <td>Phone</td>
                    <td>
                        <input type="text" name="empPhone" id="empPhone" value="<?php if (!empty($row['empPhone'])) echo decrypt($row['empPhone']) ?>">
                    </td>
                </tr>
                <tr>
                    <td>Mobile</td>
                    <td>
                        <input type="text" name="empMob" id="empMob" value="<?php if (!empty($row['empMob'])) echo decrypt($row['empMob']) ?>">
                    </td>
                </tr>
                <tr>
                    <td>Skype</td>
                    <td>
                        <input type="text" name="empSkype" id="empSkype" value="<?php if ($row['empSkype']!=NULL) echo decrypt($row['empSkype']) ?>">
                    </td>
                </tr>
                <tr>
                    <td>Alternate Email</td>
                    <td>
                        <input type="text" name="empBackupEmail" id="empBackupEmail" value="<?php if ($row['empBackupEmail']!=NULL) echo decrypt($row['empBackupEmail']) ?>">
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <input type="button" name="changeInfoBtn" id="changeInfoBtn" onclick="changeInfo()" value=" Save changes "><BR>
                        <span style="font-weight: bold; color: #ac2925">*</span> indicates required field
                    </td>
                </tr>
            </table>
    </section>
    <section class="col" style="width: 49%; padding-left: 5px">
        <!--table width="100%" border="0">
            <tr>
                <th width="20%">Your posts</th>
                <td><canvas id="chartPost" style="width:100%; height: 180px"></canvas></td>
            </tr>
        </table-->
        <p style="position: relative; width: 80%;">
            <span style="position: absolute; left:115px; top:90px;">Your posts</span>
            <canvas id="chartPost" style="width:150px; height: 100px"></canvas>
        </p>

        <p style="position: relative; width: 80%;">
            <span style="position: absolute; left:115px; top:80px; text-align: center">Your<BR>comments</span>
            <canvas id="chartCmt" style="width:150px; height: 100px"></canvas>
        </p>

        <p style="position: relative; width: 80%;">
            <span style="position: absolute; left:115px; top:80px; text-align: center">Your<BR>check-list</span>
            <canvas id="chartChk" style="width:150px; height: 100px"></canvas>
        </p>

            <?php
            $p = "SELECT `logoutTime` FROM emp WHERE empID = ".$_SESSION['empID'];
            $r = $conn->query($p);
            if ($r->num_rows > 0){
                $re = $r->fetch_assoc();
                if (!empty($re['logoutTime']) || $re['logoutTime']==null)
                    $lastUse = date($_SESSION['datetimeType'], strtotime($re['logoutTime']));
                else
                    $lastUse = "N/A";
            } else {
                $lastUse = "[ERROR]";
            }
            ?>

            <?php
            $p = "SELECT `postContent`, (SELECT `cmtContent` FROM comment WHERE empID = ".$_SESSION['empID']." ORDER BY `cmtID` DESC LIMIT 1) as cmtContent FROM post WHERE empID = ".$_SESSION['empID']." ORDER BY `postID` DESC LIMIT 1";
            $r = $conn->query($p);
            if ($r->num_rows > 0){
                $re = $r->fetch_assoc();
                $lastPost = decrypt($re['postContent']);
                $lastCmt  = decrypt($re['cmtContent']);
            } else {
                $lastPost = "N/A";
                $lastCmt = "N/A";
            }
            ?>
        <table width="100%" border="0" cellspacing="0px" cellpadding="5px">
            <tr>
                <th width="35%" style="vertical-align: top">Last login session</th>
                <td><?php echo $lastUse; ?></td>
            </tr>
            <tr>
                <th style="vertical-align: top">Recent post</th>
                <td><?php echo $lastPost; ?></td>
            </tr>
            <tr>
                <th style="vertical-align: top">Recent comment</th>
                <td><?php echo $lastCmt; ?></td>
            </tr>
        </table>
    </section>
    <div class="clear"></div>
</section>


<script type="text/javascript" src="script/script.js"></script>
<script type="text/javascript" src="script/Chart.min.js"></script>
<?php
$q = "SELECT COUNT(postID) as yourpost, (SELECT COUNT(postID) FROM post) as allpost FROM post WHERE empID = ".$_SESSION['empID'];
$r = $conn->query($q);
if ($r->num_rows>0) {
    $row = $r->fetch_assoc();
    $yourPost = $row['yourpost'];
    $allPost  = $row['allpost'];
}
else {
    $yourPost = 0;
    $allPost  = 0;
}
$q = "SELECT COUNT(cmtID) as yourcmt, (SELECT COUNT(cmtID) FROM comment) as allcmt FROM comment WHERE empID = ".$_SESSION['empID'];
$r = $conn->query($q);
if ($r->num_rows>0) {
    $row = $r->fetch_assoc();
    $yourCmt = $row['yourcmt'];
    $allCmt  = $row['allcmt'];
}
else {
    $yourCmt = 0;
    $allCmt  = 0;
}
$q = "SELECT COUNT(chkID) as yourchk, (SELECT COUNT(chkID) FROM chklist WHERE isActive=0 AND empID = ".$_SESSION['empID'].") as allchk FROM chklist WHERE isActive = 1 AND empID = ".$_SESSION['empID'];
$r = $conn->query($q);
if ($r->num_rows>0) {
    $row = $r->fetch_assoc();
    $yourChk = $row['yourchk'];
    $allChk  = $row['allchk'];
}
else {
    $yourChk = 0;
    $allChk  = 0;
}
?>
<script type="text/javascript">
    var doughnutData = [
        {
            value: <?php echo $yourPost ?>,
            color:"#F7464A",
            highlight: "#FF5A5E",
            label: "Your posts"
        },
        {
            value: <?php echo $allPost ?>,
            color: "#EB711A",
            highlight: "#F99147",
            label: "Total posts"
        }

    ];

    function chartPost(){
        var ctx = document.getElementById("chartPost").getContext("2d");
        window.myDoughnut = new Chart(ctx).Doughnut(doughnutData, {responsive : true});
    }

    var doughnutDataCmt = [
        {
            value: <?php echo $yourCmt ?>,
            color:"#089595",
            highlight: "#46BFBF",
            label: "Your comments"
        },
        {
            value: <?php echo $allCmt ?>,
            color: "#1948A6",
            highlight: "#577ECA",
            label: "Total comments"
        }

    ];

    function chartCmt(){
        var ctx = document.getElementById("chartCmt").getContext("2d");
        window.myDoughnut = new Chart(ctx).Doughnut(doughnutDataCmt, {responsive : true});
    }

    var doughnutDataChk = [
        {
            value: <?php echo $yourChk ?>,
            color: "#949FB1",
            highlight: "#A8B3C5",
            label: "Not finished"
        },
        {
            value: <?php echo $allChk ?>,
            color: "#4D5360",
            highlight: "#616774",
            label: "Finished"
        }

    ];

    function chartChk(){
        var ctx = document.getElementById("chartChk").getContext("2d");
        window.myDoughnut = new Chart(ctx).Doughnut(doughnutDataChk, {responsive : true});
    }

    chartPost(); chartCmt(); chartChk();
</script>
<?php
mysqli_free_result($r);
if (isset($res)) mysqli_free_result($res);
mysqli_free_result($result);
mysqli_free_result($result2);
mysqli_free_result($result3);
mysqli_close($conn);
?>