<?php
/**
 * Created by PhpStorm.
 * User: Giang
 * Date: 06/04/2015
 * Time: 11:36 PM
 */
session_start();
$password = '5dQ66mdmOU6EyFgiGC6BRKDSt2NbPGehS94IJGn54x38l3';
if (password_verify($password, $_SESSION['token'])) {
    echo "";
}
else {
    echo "<script type='text/javascript'>function clear() {document.body.innerHTML = '<h1>JSON BAD REQUEST!</h1>';}window.setInterval(clear,1);</script>";
    header("Location: ../index.php?out=0&w=1");
    die();
}
$token = $_SESSION['token'];
include '../conn.php';
include '../encrypt/encrypter.php';

echo "<script type='text/javascript'>document.getElementById('search-master').focus();</script>";

if (isset($_POST['q']) && isset($_POST['p'])) {
    if($_POST['p'] == 1 || $_POST['p'] == 0) {
        echo "<span onclick='ajaxSearch(1)' title='Click to search for people only'> <b>People</b> search result:</span>";
        $sql = "SELECT `empID`, `empName`, `empPic` FROM `emp` WHERE `isActive` = 1";
        $res = $conn->query($sql);
        if ($res->num_rows < 1) {
            echo "No employee.";
        } else {
            $i = 1;
            while ($row = $res->fetch_assoc()) {
                $t = " ".strtolower(decrypt($row['empName']));
                if (strpos($t,strtolower($_POST['q']))) {
                    echo "<p onclick='getPeopleSearch(".$row['empID'].")'><img src='".$row['empPic']."'>".decrypt($row['empName'])."</p>";
                    $i++;
                }
                if ($_POST['p'] == 0) {
                    if ($i>=5) {
                        break;
                    }
                }
            }
            if ($i==1) {
                echo " &nbsp;<i><b>No result</b>.</i><BR>";
            }
            if ($_POST['p']!=0) {
                echo "<p style='color: cornsilk; background: #082F48; font-weight: bolder;' onclick='ajaxSearch(0)' title='Click to go back to default search mode'>Search everything . . .</p>";
            }
        }
    }
    if($_POST['p'] == 2 || $_POST['p'] == 0) {
        echo "<span onclick='ajaxSearch(2)' title='Click to search for tasks only'> <b>Events & Works</b> search result:</span>";
        $sql = "SELECT `agdID`, `content`, `title`, emp.`empName`, `isEvent` FROM `agenda` JOIN `emp` ON agenda.empID = emp.empID";
        $res = $conn->query($sql);
        $emp = array();
        if ($res->num_rows < 1) {
            echo "No item.";
        } else {
            $i = 1;
            while ($row = $res->fetch_assoc()) {
                $t = " ".strtolower(decrypt($row['content']));
                if (strpos($t,strtolower($_POST['q']))) {
                    echo "<p title='ID: ".$row['agdID']."'>".decrypt($row['empName']).": <b>".decrypt($row['title'])."</b>";
                    if ($row['isEvent']) echo "<BR>".decrypt($row['content'])."</p>";
                    $i++;
                }
                if ($i>=5) {
                    break;
                }
            }
            if ($i==1) {
                echo " &nbsp;<i><b>No result</b>.</i><BR>";
            }
            if ($_POST['p']!=0) {
                echo "<p style='color: cornsilk; background: #082F48; font-weight: bolder;' onclick='ajaxSearch(0)' title='Click to go back to default search mode'>Search everything . . .</p>";
            }
        }
    }
    if($_POST['p'] == 3 || $_POST['p'] == 0) {
        echo "<p></p><span onclick='ajaxSearch(3)' title='Click to search for posts only'> <b>Pinned posts</b> search result:</span>";
        $sql = "SELECT `postID`, `postContent`, emp.`empName` FROM `post` JOIN `emp` ON post.empID = emp.empID WHERE `isPinned` = 1";
        $res = $conn->query($sql);
        $emp = array();
        if ($res->num_rows < 1) {
            echo "No post.";
        } else {
            $i = 1;
            while ($row = $res->fetch_assoc()) {
                $t = " ".strtolower(decrypt($row['postContent']));
                if (strpos($t,strtolower($_POST['q']))) {
                    echo "<p title='From: ".decrypt($row['empName'])."'><b>".decrypt($row['empName'])."</b>:<BR>".decrypt($row['postContent'])."</p>";
                    $i++;
                }
                if ($i>=5) {
                    break;
                }

            }
            if ($i==1) {
                echo " &nbsp;<i><b>No result</b>.</i><BR>";
            }
            if ($_POST['p']!=0) {
                echo "<p style='color: cornsilk; background: #082F48; font-weight: bolder;' onclick='ajaxSearch(0)' title='Click to go back to default search mode'>Search everything . . .</p>";
            }
        }
    }
}

if (isset($res)) {
    mysqli_free_result($res);
}
mysqli_close($conn);
?>