<?php
session_start();
$password = '5dQ66mdmOU6EyFgiGC6BRKDSt2NbPGehS94IJGn54x38l3';
if (password_verify($password, $_SESSION['token'])) {
    echo "";
}
else {
    echo "<script type='text/javascript'>function clear() {document.body.innerHTML = '<h1>JSON BAD REQUEST!</h1>';}window.setInterval(clear,1);</script>";
    header("Location: ../index.php?out=0&w=1");
    die();
}
$token = $_SESSION['token'];
include '../conn.php';
include '../encrypt/encrypter.php';

if (isset($_GET['changeID']) && isset($_GET['stat'])) {
    if ($_GET['stat']) {
        $stat = 0;
    } else {
        $stat = 1;
    }
    $q = "UPDATE `chklist` SET `isActive` = '".$stat."' WHERE `chkID` = ".$_GET['changeID'];
    $r = $conn->query($q);
}

if (isset($_POST['add'])) {
    if (!empty($_POST['add']) && strlen($_POST['add']) >=2 ) {
        $q = "INSERT INTO `chklist` (`chkID`, `empID`, `chkContent`, `isActive`) VALUES (NULL, '".$_SESSION['empID']."', '".encrypt($_POST['add'])."', '1')";
        $r = $conn->query($q);
    }
}
?>

<ol>
    <?php
    $sql1 = "SELECT `chkID` ,`chkContent` FROM `chklist` WHERE `empID` = ".$_SESSION['empID']." AND `isActive` = 1 ORDER BY `chkID` DESC";
    $result1 = $conn->query($sql1);
    while($row1 = $result1->fetch_assoc()) {
        echo "<li class='todoItem ";
        echo "' onclick='chkListLoader(\"changeID=".$row1['chkID']."&stat=1\")'> ".decrypt($row1['chkContent'])."</li>";
    }

    $sql2 = "SELECT `chkID` ,`chkContent` FROM `chklist` WHERE `empID` = ".$_SESSION['empID']." AND `isActive` = 0 ORDER BY `chkID` DESC LIMIT 0,10";
    $result2 = $conn->query($sql2);
    while($row2 = $result2->fetch_assoc()) {
        echo "<li class='todoItem ";
        if ($row2['isActive'] == 0) {
            echo "todoDone";
        }
        echo "' onclick='chkListLoader(\"changeID=".$row2['chkID']."&stat=0\")'> ".decrypt($row2['chkContent'])."</li>";
    }

    if ($result1->num_rows < 1 && $result2->num_rows < 1) {
        echo "<b title='... or connection error'>No item found.</b>";
    }
    ?>
</ol>
<script type="text/javascript" src="script/script.js"></script>
<?php
mysqli_free_result($result1);
mysqli_free_result($result2);
mysqli_close($conn);
?>