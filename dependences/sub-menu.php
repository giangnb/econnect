<?php
    session_start();
	$password = '5dQ66mdmOU6EyFgiGC6BRKDSt2NbPGehS94IJGn54x38l3';
    if (password_verify($password, $_SESSION['token'])) {
        $token = $_SESSION['token'];
        include '../conn.php';
        include '../encrypt/encrypter.php';
        if ($_GET['part']==1) {
			//Discuss
			echo '<ul>';
            echo '<li id="discuss-filter-all" onclick="ajaxLoader(1)">All posts</li>';
            echo '<li id="discuss-filter-your" onclick="ajaxLoader(1,\'only-me\')">My posts</li><li id="discuss-filter-commented" onclick="ajaxLoader(1,\'cmt-post\')">My commented posts</li><li id="discuss-filter-manager" onclick="ajaxLoader(1,\'all-pin\')">All pinned posts</li></ul>';
		}
		if ($_GET['part']==2) {
			//Agenda
            echo '<ul><li id="agenda-add-event">Add task</li>';
			//echo '<li id="agenda-filter-dept">My branch\'s agenda</li>';
            echo '<li>Filter &nbsp;&nbsp;<select onchange="taskFilter()" id="agenda-filter-type"><option value="all">All items</option><option value="event">Events only</option><option value="work">Works only</option></select></li></ul>';
		}
		if ($_GET['part']==3 || $_GET['part']==3.1 || $_GET['part']==3.2) {
			//People
            echo '<ul>';
            if(isset($_GET['posID']) || isset($_GET['deptID']) || isset($_GET['branchID']) || isset($_POST['q'])) {
                echo '<li id="people-filter-all">Show all people</li>';
            }
			//echo '<li id="people-filter-dept">People in my department</li><li id="people-filter-manager">Managers</li>';
            echo '<li>Filter<BR>';
            // Filter POSITION
            echo "<select id='people-filter-byPos' name='people-filter-byPos' onchange='peopleFilter()' style='min-width: 80%; margin: 3px; padding: 3px;'><option value='0'>All Position</option>";
            $q1 = "SELECT posName, posID FROM position";
            $r1 = $conn->query($q1);
            if ($r1->num_rows > 0) {
                while ($row1 = $r1->fetch_assoc()) {
                    echo "<option value='{$row1['posID']}'>".decrypt($row1['posName'])."</option>";
                }
            }
            echo "</select><BR>";
            // Filter DEPARTMENT
            echo "<select id='people-filter-byDept' name='people-filter-byDept' onchange='peopleFilter()' style='min-width: 80%; margin: 3px; padding: 3px;'><option value='0'>All Department</option>";
            $q2 = "SELECT deptName, deptID FROM department";
            $r2 = $conn->query($q2);
            if ($r2->num_rows > 0) {
                while ($row2 = $r2->fetch_assoc()) {
                    echo "<option value='{$row2['deptID']}'>".decrypt($row2['deptName'])."</option>";
                }
            }
            echo "</select><BR>";
            // Filter BRANCH
            echo "<select id='people-filter-byBranch' name='people-filter-byBranch' onchange='peopleFilter()' style='min-width: 80%; margin: 3px; padding: 3px;'><option value='0'>All Branch</option>";
            $q3 = "SELECT branchName, branchID FROM branch";
            $r3 = $conn->query($q3);
            if ($r3->num_rows > 0) {
                while ($row3 = $r3->fetch_assoc()) {
                    echo "<option value='{$row3['branchID']}'>".decrypt($row3['branchName'])."</option>";
                }
            }
            echo "</select>";
            echo "</li></ul>";
		}
		if ($_GET['part']==4) {
			// Support
			echo '<ul><!--li id="support-quick-guide">eConnect quick guide</li--><a href="docs/manual.pdf" target="_blank" style="color:#fff"><li id="support-view-documents">View documentary</li></a></ul>';
		}
        if ($_GET['part']=="usr-page") {
            // User page
            echo '<section style="position: relative;">';
            echo '<center><img src="'.$_SESSION['empPic'].'" style="width: 60%; text-align: center;"/><BR>';
            echo '<input type="text" name="empPic" id="empPicURL" style="width: 90%;" placeholder="Image URL"/> <form action="uploadImg.php" method="post" enctype="multipart/form-data"><input type="file" name="empPic" id="empPicUpload" accept="image/png,image/jpg,image/jpeg,image/gif,image/bmp" size="2MB" placeholder="Upload your image" required><BR>';
            echo '<input type="button" id="uploadImgMode" onclick="imgUploadSwitch()" value=" Image URL " title="Switch between File upload and URL mode"><button type="submit" onclick="uploadImgFile()" id="btnUploadFile"><span class="glyph">&#xe167;</span> Upload </button></button><button type="button" onclick="uploadImgURL()" id="btnUploadURL"><span class="glyph">&#xe172;</span> Save </button></form>';
            echo '<button class="glyph" onclick="imgReset(\''.$_SESSION['defaultPic'].'\')" title = "Change back to default photo" style="position: absolute; right: 5px; top: 5px;">&#xe020;</button></center>';
            echo '</section>';
        }
    }
    else {
        echo "<script type='text/javascript'>function clear() {document.body.innerHTML = '<h1>JSON BAD REQUEST!</h1>';}window.setInterval(clear,1);</script>";
        header("Location: ../index.php?out=0&w=1");
        die();
    }
?>
<script>
    function imgUploadSwitch() {
        if (imgUploadMode==1 || imgUploadMode==0) {
            $("#btnUploadFile").hide();
            $("#btnUploadURL").show();
            $("#empPicUpload").hide(100);
            $("#empPicURL").show(100);
            document.getElementById("uploadImgMode").value = "File";
            imgUploadMode = 2;
        } else {
            $("#btnUploadFile").show();
            $("#btnUploadURL").hide();
            $("#empPicUpload").show(100);
            $("#empPicURL").hide(100);
            document.getElementById("uploadImgMode").value = "URL";
            imgUploadMode = 1;
        }
    }
</script>
<script type="text/javascript" src="script/script.js"></script>
