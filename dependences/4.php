<?php
session_start();
$password = '5dQ66mdmOU6EyFgiGC6BRKDSt2NbPGehS94IJGn54x38l3';
if (password_verify($password, $_SESSION['token'])) {
    echo "";
}
else {
    echo "<script type='text/javascript'>function clear() {document.body.innerHTML = '<h1>JSON BAD REQUEST!</h1>';}window.setInterval(clear,1);</script>";
    header("Location: ../index.php?out=0&w=1");
    die();
}

$token = $_SESSION['token'];
include '../conn.php';
include '../encrypt/encrypter.php';

$return = 0;

if (isset($_POST['reqTypeID']) && isset($_POST['reqContent']) && isset($_POST['reqStat'])) {
    $sql = "INSERT INTO request (reqID, reqTypeID, empID, reqContent, reqStat) VALUES (NULL, '".$_POST['reqTypeID']."', '".$_SESSION['empID']."', '".encrypt($_POST['reqContent'])."', '".$_POST['reqStat']."');";
    $res = $conn->query($sql);
    echo "<span style='border:dashed thin #182236; padding:5px; font-size: large; color: #182236'>Request sent.</span>";
    if ($_POST['reqStat']==3) {
        $msg = "<html><head><title>Request received</title></head><body><p>Hello Administrator,<BR><BR>The user ID {$_SESSION['empID']} at ECONNECT system has sent an URGENT MESSAGE of RequestType {$_POST['reqTypeID']}</p><hr><BR>
            {$_POST['reqContent']}<BR><BR><HR><BR><p>This is an automated email.<BR>--End of email--</p></body></html>";
        $headers = 'MIME-Version: 1.0' . "\r\n" . 'Content-type: text/html' . "\r\n" . 'From: eConnect System <admin@giangnb.com>' . "\r\n" . 'Reply-To: '.$_SESSION['usr'] . "\r\n" . 'X-Mailer: PHP/' . phpversion();
        $msg = wordwrap($msg,70);
        mail("giangnb@giangnb.com","URGENT - Received an urgent message from user",$msg,$headers);
    }
}

?>

<section id="support" class="container"><h1>Request &amp; Support</h1>
    <table>
        <tr>
            <th>Request</th>
            <td>
                <select id="reqType" onchange="" style="padding: 3px; margin: 5px; font-size: 16px">
                    <option value="0">-- Please choose --</option>
                    <?php
                    $q = "SELECT reqTypeID, reqDesc FROM reqtype";
                    $r = $conn->query($q);
                    if ($r->num_rows > 0) {
                        while ($row = $r->fetch_assoc()) {
                            echo "<option value='".$row['reqTypeID']."'>".decrypt($row['reqDesc'])."</option>";
                        }
                    } else {
                        echo "<option value='0'>-- You cannot send request! --</option>";
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <th>Description</th>
            <td>
                <textarea id="reqContent" rows="10" cols="60" style="width: 100%; font-size: 16px; border-radius: 5px;"><?php if ($return) echo $_POST['reqContent']; ?></textarea>
            </td>
			<script type="text/javascript" src="script/tinymce/tinymce.min.js"></script>
			<script type="text/javascript">
				tinymce.init({
					height: 220,
					menubar : false,
					statusbar : false,
					event_root: "#reqContent",
					mode : "textareas",
					selector: "textarea#reqContent",
					plugins: [
						"autolink lists link image charmap preview",
						"visualblocks emoticons",
						"media paste"
					],
					toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist | emoticons link image",
					valid_elements : "a[href|target=_blank],strong/b,p[align],br"
				});
			</script>
        </tr>
        <tr>
            <th>Priority</th>
            <td>
                <label><input type="radio" name="priority" id="reqStatNormal" checked> Normal</label>&nbsp;&nbsp;&nbsp;
                <label><input type="radio" name="priority" id="reqStatHigh"> <span style="color:#d58512">High</span> </label>&nbsp;&nbsp;&nbsp;
                <label><input type="radio" name="priority" id="reqStatUrgent"> <span style="color:#ac2925">Urgent</span></label>
            </td>
        </tr>
        <tr>
            <th>&nbsp;</th>
            <td style="text-align: right">
                <BR>
                <input type="button" onclick="sendRequest()" value=" Send ">
            </td>
        </tr>
    </table>
    <!--table style="margin-top:120px; margin-left: 120px; background: #c0c0c0; border-radius: 5px;" id="appinfo" title="App information" cellspacing="10px">
        <?php /*
        $q = "SELECT (SELECT `value` FROM `setting` WHERE `prop`='appVer') as appVer, (SELECT `value` FROM `setting` WHERE `prop`='dbVer') as dbVer";
        $r = $conn->query($q);
        if ($r->num_rows>0) {
            $row = $r->fetch_assoc();
            $appVer = $row['appVer'];
            $dbVer = $row['dbVer'];
        }
        ?>
        <tr id="appinfoBtn">
            <td colspan="2">Application Information</td>
        </tr>
        <tr>
            <th style="text-align: right" width="60%">WebApp</th>
            <td>eConnect</td>
        </tr>
        <tr>
            <th style="text-align: right">Version</th>
            <td><?php echo $appVer; ?></td>
        </tr>
        <tr>
            <th style="text-align: right">Database Version</th>
            <td><?php echo $dbVer; ?></td>
        </tr>
        <tr>
            <th style="text-align: right">PHP version</th>
            <td><?php echo phpversion(); ?></td>
        </tr>
    </table> */?>
</section>
<script type="text/javascript" src="script/script.js"></script>
<?php
mysqli_free_result($r);
if (isset($res)) mysqli_free_result($res);
mysqli_close($conn);
?>