<?php
// Employee Ditails
if (isset($_GET['noSearch'])) {
    session_start();
    $password = '5dQ66mdmOU6EyFgiGC6BRKDSt2NbPGehS94IJGn54x38l3';
    if (password_verify($password, $_SESSION['token'])) {
        echo "";
    }
    else {
        echo "<script type='text/javascript'>function clear() {document.body.innerHTML = '<h1>JSON BAD REQUEST!</h1>';}window.setInterval(clear,1);</script>";
        header("Location: ../index.php?out=0&w=1");
        die();
    }
    $token = $_SESSION['token'];
    include '../conn.php';
    include '../encrypt/encrypter.php';
}

if (!isset($_GET['empID'])) {
    echo "<BR><BR><h3 style='text-align: center'>Please select a person from the list</h3>";
    die();
} else {
    $q = "SELECT empNo, empName, empEmail, empPic, empGen, empDOB, empPhone, empMob, empSkype, empPic, branchName, branchAdd FROM `emp` JOIN `branch` ON emp.branchID = branch.branchID WHERE empID = '{$_GET['empID']}'";
    $result = $conn->query($q);
    $row = $result->fetch_assoc();
    $q2 = "SELECT posName FROM `emppos` JOIN `position` ON emppos.posID = position.posID WHERE empID = '{$_GET['empID']}'";
    $result2 = $conn->query($q2);
    $q3 = "SELECT deptName FROM `emp` JOIN `department` ON emp.deptID = department.deptID WHERE empID = '{$_GET['empID']}'";
    $result3 = $conn->query($q3);
    $row3 = $result3->fetch_assoc();

    if ((!isset($row['empPic'])) || empty($row['empPic'])) {
        $empPic = $_SESSION['defaultPic'];
    } else {
        $empPic = $row['empPic'];
    }
}

?>

<div id="people-avatar">
    <img id="people-avatar-cover" src="<?php echo $empPic; ?>" height="165px"/>
</div>
<BR>
<table width="100%" border="0" id="people-desc">
    <tr>
        <th width="25%">Name</th>
        <td id="peopleName"><?php echo decrypt($row['empName']); ?></td>
    </tr>
    <tr>
        <th width="25%">Employee Number</th>
        <td id=""><?php echo $row['empNo']; ?></td>
    </tr>
    <tr>
        <th width="25%">Department</th>
        <td id="peopleDepartment"><?php echo decrypt($row3['deptName']); ?></td>
    </tr>
    <tr>
        <th width="25%">Position</th>
        <td id="peoplePosition">
            <?php
            while ($row2 = $result2->fetch_assoc()) {
                echo decrypt($row2['posName'])."; ";
            }
            ?>
        </td>
    </tr>
    <tr>
        <th width="25%">Gender</th>
        <td id="">
            <?php
            switch ($row['empGen']) {
                case 1:
                    echo "Male";
                    break;
                case 2:
                    echo "Female";
                    break;
                case 3:
                    echo "Other";
                    break;
            }
            ?>
        </td>
    </tr>
    <tr>
        <th width="25%" colspan="2"><BR><center><h2>Contact information</h2></center></th>
    </tr>
    <tr>
        <th width="25%">Branch</th>
        <td id="peopleBranch">
            <?php
            echo decrypt($row['branchName']);
            if (!empty($row['branchAdd'])) {
                echo "<BR>".decrypt($row['branchAdd']);
            }
            ?>
        </td>
    </tr>
    <tr>
        <th width="25%">Email</th>
        <td id="peopleEmail"><?php echo decrypt($row['empEmail']) ?></td>
    </tr>
    <tr>
        <th width="25%">Telephone</th>
        <td id="peopleTel"><?php if (!empty($row['empPhone'])) echo decrypt($row['empPhone']); else echo "&lt; No info &gt;"; ?></td>
    </tr>
    <tr>
        <th width="25%">Mobile</th>
        <td id="peopleMob"><?php if (!empty($row['empMob'])) echo decrypt($row['empMob']); else echo "&lt; No info &gt;"; ?></td>
    </tr>
    <tr>
        <th width="25%">Skype</th>
        <td id="peopleSkype"><?php if (!empty($row['empSkype'])) echo decrypt($row['empSkype']); else echo "&lt; No info &gt;"; ?></td>
    </tr>
</table>
<?php
mysqli_free_result($result);
mysqli_free_result($result2);
mysqli_free_result($result3);
mysqli_close($conn);
?>