<?php
session_start();
$password = '5dQ66mdmOU6EyFgiGC6BRKDSt2NbPGehS94IJGn54x38l3';
if (password_verify($password, $_SESSION['token'])) {
    echo "";
}
else {
    echo "<script type='text/javascript'>function clear() {document.body.innerHTML = '<h1>JSON BAD REQUEST!</h1>';}window.setInterval(clear,1);</script>";
    header("Location: ../index.php?out=0&w=1");
    die();
}
$token = $_SESSION['token'];
include '../conn.php';
include '../encrypt/encrypter.php';

$sql = "SELECT `branchID`, `deptID` FROM `emp` WHERE emp.empID = ".$_SESSION['empID'];
$res = $conn->query($sql);
$row = $res->fetch_assoc();
$empBranch = $row['branchID'];
$empDept = $row['deptID'];
mysqli_free_result($res);

if ($_SERVER['REQUEST_METHOD']=="POST") {
    if (isset($_POST['isEvent'])) {
        if ($_POST['isEvent'] == 1) {
            $sql = "INSERT INTO `agenda` (`agdID`, `branchID`, `deptID`, `empID`, `start`, `end`, `title`, `content`, `attend`, `isEvent`, `stat`, `isActive`) VALUES (NULL, '{$empBranch}', '{$empDept}', '" . $_SESSION['empID'] . "', '" . date("Y-m-d H:i:s", strtotime($_POST['start'])) . "', '" . date("Y-m-d H:i:s", strtotime($_POST['end'])) . "', '" . encrypt($_POST['title']) . "', '" . encrypt($_POST['content']) . "', NULL, '1', '', '1');";
            $res = $conn->query($sql);
        } else {
            $sql = "INSERT INTO `agenda` (`agdID`, `branchID`, `deptID`, `empID`, `start`, `end`, `title`, `content`, `attend`, `isEvent`, `stat`, `isActive`) VALUES (NULL, '{$empBranch}', '{$empDept}', '" . $_SESSION['empID'] . "', '" . date("Y-m-d H:i:s", strtotime($_POST['start'])) . "', '" . date("Y-m-d H:i:s", strtotime($_POST['end'])) . "', '" . encrypt($_POST['title']) . "', '" . encrypt($_POST['content']) . "', '".encrypt($_POST['attend'])."', '0', '".$_POST['stat']."', '1');";
            $res = $conn->query($sql);
        }
    }
    if (isset($_POST['delItem'])) {
        $sql = "SELECT `empID` FROM `agenda` WHERE `agdID` = ".$_POST['agdID'];
        $res = $conn->query($sql);
        if ($res->num_rows>0) {
            $row = $res->fetch_assoc();
            if ($row['empID']==$_SESSION['empID']) {
                $q = "UPDATE `agenda` SET `isActive` = '0' WHERE `agdID` = ".$_POST['agdID'];
                $r = $conn->query($q);
            }
        }
    }
    if (isset($_POST['updateWork'])) {
        $stat = explode(";", $_POST['stat']);
        $stat[$_POST['part']] = $_POST['value'];
        $raw = implode(";",$stat);
        $sql = "UPDATE `agenda` SET `stat` = '".$raw."' WHERE `agenda`.`agdID` = ".$_POST['agdID'];
        $res = $conn->query($sql);
    }
}

if (isset($_GET['dept-agd'])) {
} elseif (isset($_GET['filter'])) {
} else {
    $sql = "SELECT `agdID`, agenda.`deptID`, agenda.`empID` ,`empName`, `start`, `end`, `title`, `content`, `stat`, `attend`, `isEvent` FROM `agenda` JOIN `emp` ON agenda.empID = emp.empID WHERE agenda.`isActive`=1 AND (ABS(`end` - NOW()) <= 933120000) ORDER BY `end` DESC";
    $res = $conn->query($sql);
}
?>

<section id="agenda" class="container"><h1>Tasks</h1>
                <section class="scroll" id="scroll-area-event">
                    <?php
                    if ($res->num_rows>0) {
                        while ($row = $res->fetch_assoc()) {
                            if ($row['deptID']==$empDept || strpos($row['attend'],$_SESSION['empID'])) {
                                if ($row['isEvent']==0) {
                                    echo '<section class="left-event taskWork" style="background: cornsilk; position: relative;">';
                                    echo '<h3><span style="font-weight: normal; background: #005544;padding: 3px 5px; border-radius: 3px; color:#fff;"> Work </span> <span style="padding-right: 50px;">'.decrypt($row['title']).'</span>';
                                } else {
                                    echo '<section class="left-event taskEvent" style="position: relative;">';
                                    echo '<h3><span style="font-weight: normal; background: #082F48;padding: 3px 5px; border-radius: 3px; color:#fff;">Event</span> <span style="padding-right: 50px;">'.decrypt($row['title']).'</span>';
                                }
                                echo '<span style="font-weight: bold; font-size: smaller; float: right; margin-right: 65px; text-align: right;"><i>created by</i> '.decrypt($row['empName']).'</span></h3>';
                                echo "<p>";
                                echo "<span style='font-style: italic; font-size: small; padding-left: 55px;'>From <b>".date($_SESSION['datetimeType'], strtotime($row['start']))."</b> to <b>".date($_SESSION['datetimeType'], strtotime($row['end']))."</b></span><BR><BR>";
                                if ($row['isEvent']==0) {
                                    $content = explode("^^div$$",decrypt($row['content']));
                                    $attend = explode(";",decrypt($row['attend']));
                                    $stat = explode(";",$row['stat']);
                                    $l = count($attend);
                                    echo "<table width='95%' border='0' cellspacing='3px' class='workTable'>";
                                    echo "<tr><th colspan='5'>Work details <i>(divided into ".($l-2)." parts)</i></th></tr>";
                                    echo "<tr><th width='5%'>Part</th><th width='65%'>Content</th><th width='10%' title='Employee details. (Hover to view)'>Emp.</th><th width='15%' title='Estimated progress'>Progress</th><th width='5%'>&nbsp;</th></tr>";
                                    for ($i=1; $i<$l-1; $i++) {
                                        echo "<tr><td>{$i}.</td>";
                                        echo "<td>{$content[$i]}</td>";
                                        //echo "<td title='".substr($attend[$i],(strpos(' | ',$attend[$i])+3))."'>".explode(' | ',$attend[$i])[0]."</td>";
                                        echo "<td title='".substr($attend[$i],(strpos(' | ',$attend[$i])+3))."' style='text-align: center;'><span class='glyph'>&#xe008;</span></td>";
                                        if (explode(' | ',$attend[$i])[0] == $_SESSION['empID']) {
                                            echo "<td><input type='number' id='workUpdate-frm-{$row['agdID']}{$i}' value='{$stat[$i]}' min='000' max='100' step='1' title='Type and click the button on the right to update progress'> %</td>";
                                            echo "<td title='Update progress of this part'><button onclick='workUpdate(".$row['agdID'].", ".$i.", \"".$row['stat']."\")' class='glyph'>&#xe089;</button> </td>";
                                        } else {
                                            echo "<td>";
                                            if ($stat[$i] !=100) {
                                                if($stat[$i]<10) {
                                                    echo substr($stat[$i],1,2);
                                                } else {
                                                    echo substr($stat[$i],2,1);
                                                }
                                            } else {
                                                echo $stat[$i];
                                            }
                                            echo " %</td>";
                                            echo "<td>&nbsp;</td>";
                                        }
                                        echo "</tr>";
                                    }
                                    echo "</table>";
                                    //echo "<span style='font-size: large;'>".decrypt($row['content']).$row['attend']."</span>";
                                } else {
                                    echo "<span style='font-size: large;'>".decrypt($row['content'])."</span>";
                                }
                                echo "</p>";
                                if ($row['empID']==$_SESSION['empID']) {
                                    echo "<button onclick=\"delItem(".$row['agdID'].")\" style='position: absolute; top:5px; right: 5px;' class='glyph' title='Delete this item'>&#xe020;</button>";
                                }
                                echo "</section>";
                            }
                        }
                    } else {
                        echo "<p style='color:#ac2925; font-weight: bold;'>Nothing to show here</p>";
                    }
                    ?>
                    <!-- TEMPLATE:
                     <section class="left-event">
                        <h3>Event Name</h3>
                        <p>
                            Time<BR>
                            Lorem ipsum dolor sit amet consec...
                        </p>
                    </section>-->
                </section>
				</section>
				<script type="text/javascript" src="script/script.js"></script>
<?php
mysqli_free_result($res);
mysqli_close($conn);
?>