<?php
// Employee List
if (isset($_POST['q']) || isset($_GET['all']) || (isset($_GET['posID']) && isset($_GET['deptID']) && isset($_GET['branchID']))) {
    session_start();
    $password = '5dQ66mdmOU6EyFgiGC6BRKDSt2NbPGehS94IJGn54x38l3';
    if (password_verify($password, $_SESSION['token'])) {
        echo "";
    }
    else {
        echo "<script type='text/javascript'>function clear() {document.body.innerHTML = '<h1>JSON BAD REQUEST!</h1>';}window.setInterval(clear,1);</script>";
        header("Location: ../index.php?out=0&w=1");
        die();
    }
    $token = $_SESSION['token'];
    include '../conn.php';
    include '../encrypt/encrypter.php';
    if (isset($_GET['posID']) && isset($_GET['deptID']) && isset($_GET['branchID'])) {
        $sql = "SELECT emp.empID, empName, position.posID, posName, branch.branchID, branchName, department.deptID, deptName "
            . "FROM emp "
            . "JOIN emppos ON emp.empID = emppos.empID "
            . "JOIN position ON emppos.posID = position.posID "
            . "JOIN branch ON emp.branchID = branch.branchID "
            . "JOIN department ON emp.deptID = department.deptID "
            . "WHERE emp.isActive = 1";
        if ($_GET['posID'] != 0) {
            $sql = $sql." AND emppos.posID = ".$_GET['posID'];
        }
        if ($_GET['deptID'] != 0) {
            $sql = $sql." AND emp.deptID = ".$_GET['deptID'];
        }
        if ($_GET['branchID'] != 0) {
            $sql = $sql." AND emp.branchID = ".$_GET['branchID'];
        }
        $res = $conn->query($sql);
    } else {
        $sql = "SELECT `empID`, `empName` FROM `emp` WHERE `isActive` = 1";
        $res = $conn->query($sql);
    }

    $empName = array();
    $empID = array();
    if ($res->num_rows < 1) {
        echo "<h1>NO EMPLOYEES!</h1>";
    } else {
        while ($row = $res->fetch_assoc()) {
            $empID[] = $row['empID'];
            $empName[] = " ".decrypt($row['empName']);
        }
    }
}

$alphabet = array("a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","#");
$count = count($empID);
foreach ($alphabet as $abc) {
    if ($abc=="#") {
        echo "<h2 id='people-name-list-other'><span>".$abc."</span></h2><ul></ul>";
        continue;
    }
    echo "<h2 id='people-name-list-".$abc."'><span>".$abc."</span></h2><ul>";
    for ($i = 0; $i<$count; $i++) {
        if (isset($_POST['q'])){
            if (!empty($_POST['q'])) {
                if (strpos(strtolower($empName[$i]),$abc)==1 && strpos(strtolower($empName[$i]),strtolower($_POST['q'])) >= 1) {
                    echo "<li onclick='getPeople(".$empID[$i].")'>".$empName[$i]."</li>";
                }
            }
        } else {
            if (strpos(strtolower($empName[$i]),$abc)==1) {
                if ($i!=0 && $empID[$i]==$empID[$i-1]) {
                    continue;
                }
                echo "<li onclick='getPeople(".$empID[$i].")'>".$empName[$i]."</li>";
            }
        }
    }
    echo "</ul>";
}
mysqli_free_result($res);
mysqli_close($conn);
?>