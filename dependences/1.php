<?php
session_start();
$password = '5dQ66mdmOU6EyFgiGC6BRKDSt2NbPGehS94IJGn54x38l3';
if (password_verify($password, $_SESSION['token'])) {
    echo "";
}
else {
    echo "<script type='text/javascript'>function clear() {document.body.innerHTML = '<h1>JSON BAD REQUEST!</h1>';}window.setInterval(clear,1);</script>";
    header("Location: ../index.php?out=0&w=1");
    die();
}
$token = $_SESSION['token'];
include '../conn.php';
include '../encrypt/encrypter.php';
$q = "SELECT (SELECT `value` FROM `setting` WHERE `prop` = 'datetimeType') as datetimeType, (SELECT `value` FROM `setting` WHERE `prop` = 'pinPostLimit') as pinPostLimit, (SELECT `value` FROM `setting` WHERE `prop` = 'postPerPage') as postPerPage;";
$r = $conn->query($q);
while ($row = $r->fetch_assoc()) {
    $_SESSION['datetimeType'] = $row['datetimeType'];
    $_SESSION['pinPostLimit'] = $row['pinPostLimit'];
    $_SESSION['postPerPage'] = $row['postPerPage'];
}
$query_count = "SELECT COUNT(`postID`) as `count` FROM `post` JOIN `emp` ON emp.empID = post.empID WHERE post.isPinned = 0 AND emp.isActive=1";
$get_count = $conn->query($query_count);
$fetch_count = $get_count->fetch_assoc();
$post_count = $fetch_count['count'];
if (isset($_GET['pg'])) {
    $pg_count = $_GET['pg'];
    $pg_count = $_GET['pg'] - 1;
} else {
    $_GET['pg'] = 1;
    $pg_count = 0;
}
$filter = "";

// Load PINNED POSTS
//if (isset($_GET['pg']))
if (1) {
    $q1 = "SELECT `empName`, `empPic`, `postTime`, `postContent`, `postID`, emp.`empID` FROM `post` JOIN `emp` ON post.empID = emp.empID WHERE `isPinned` = 1 ";
    if (isset($_GET['only-me'])) {
        $q1 = $q1."AND emp.empID = ".$_SESSION['empID'];
        $query_count = "SELECT COUNT(`postID`) as `count` FROM `post` WHERE isPinned = 1 AND empID = ".$_SESSION['empID'];
        $get_count = $conn->query($query_count);
        $fetch_count = $get_count->fetch_assoc();
        $post_count = $fetch_count['count'];
        if (isset($_GET['pg'])) {
            $pg_count = $_GET['pg'];
            $pg_count = $_GET['pg'] - 1;
        } else {
            $_GET['pg'] = 1;
            $pg_count = 0;
        }
        $filter = "only-me";
    }
    if (isset($_GET['cmt-post'])) {
        $q1 = $q1."AND `postID` IN (SELECT `postID` FROM `comment` WHERE `empID` = ".$_SESSION['empID'].")";
        $query_count = "SELECT COUNT(`postID`) as `count` FROM `post` JOIN `emp` ON post.empID = emp.empID WHERE post.isPinned = 1 AND emp.isActive = 1 AND `postID` IN (SELECT `postID` FROM `comment` WHERE `empID` = ".$_SESSION['empID'].")";
        $get_count = $conn->query($query_count);
        $fetch_count = $get_count->fetch_assoc();
        $post_count = $fetch_count['count'];
        if (isset($_GET['pg'])) {
            $pg_count = $_GET['pg'];
            $pg_count = $_GET['pg'] - 1;
        } else {
            $_GET['pg'] = 1;
            $pg_count = 0;
        }
        $filter = "cmt-post";
    }
    if(isset($_GET['all-pin'])) {
        $query_count = "SELECT COUNT(`postID`) as `count` FROM `post` JOIN `emp` ON emp.empID = post.empID WHERE post.isPinned = 1 AND emp.isActive=1";
        $get_count = $conn->query($query_count);
        $fetch_count = $get_count->fetch_assoc();
        $post_count = $fetch_count['count'];
        if (isset($_GET['pg'])) {
            $pg_count = $_GET['pg'];
            $pg_count = $_GET['pg'] - 1;
        } else {
            $_GET['pg'] = 1;
            $pg_count = 0;
        }
        $filter = "all-pin";
        $q1 = $q1." ORDER BY `postTime` DESC LIMIT ".$pg_count*$_SESSION['postPerPage'].",".$_SESSION['postPerPage'];
    } else {
        if (!isset($_GET['only-me']) && !isset($_GET['cmt-post'])) {
            $q1 = $q1." ORDER BY `postTime` DESC LIMIT 0,".$_SESSION['pinPostLimit'];
        } else {
            $q1 = $q1." ORDER BY `postTime` DESC LIMIT ".$pg_count*$_SESSION['pinPostLimit'].",".$_SESSION['pinPostLimit'];
        }
    }
    $r1 = $conn->query($q1);
}

// Load REGULAR POSTS
if (!isset($_GET['all-pin'])) {
    $q2 = "SELECT `empName`, `empPic`, `postTime`, `postContent`, `postID`, emp.`empID` FROM `post` JOIN `emp` ON post.empID = emp.empID WHERE post.`isPinned` = 0 AND emp.`isActive`=1";
    if (isset($_GET['only-me'])) {
        echo "<p style='background: cornsilk; padding: 5px;'>Now showing: <B>Your posts</B></p>";
        $q2 = $q2." AND emp.empID = ".$_SESSION['empID'];
        $query_count = "SELECT COUNT(`postID`) as `count` FROM `post` WHERE isPinned = 1 AND empID = ".$_SESSION['empID'];
        $get_count = $conn->query($query_count);
        $fetch_count = $get_count->fetch_assoc();
        $post_count += $fetch_count['count'];
    }
    if (isset($_GET['cmt-post'])) {
        echo "<p style='background: cornsilk; padding: 5px;'>Now showing: <B>Your commented posts</B></p>";
        $q2 = $q2." AND `postID` IN (SELECT `postID` FROM `comment` WHERE `empID` = ".$_SESSION['empID'].")";
        $query_count = "SELECT COUNT(`postID`) as `count` FROM `post` JOIN `emp` ON post.empID = emp.empID WHERE post.isPinned = 1 AND emp.isActive = 1 AND `postID` IN (SELECT `postID` FROM `comment` WHERE `empID` = ".$_SESSION['empID'].")";
        $get_count = $conn->query($query_count);
        $fetch_count = $get_count->fetch_assoc();
        $post_count += $fetch_count['count'];
    }
    $q2 = $q2." ORDER BY `postTime` DESC LIMIT ".$pg_count*$_SESSION['postPerPage'].",".$_SESSION['postPerPage'];
    $r2 = $conn->query($q2);
} else {
    echo "<p style='background: cornsilk; padding: 5px;'>Now showing: <B>All pinned posts</B></p>";
}
?>

<!-- POSTS SHOWCASE -->
<section id="discuss" class="container">
    <?php
    if (!isset($_GET['only-me']) && !isset($_GET['load-more']) && !isset($_GET['cmt-post']) && !isset($_GET['all-pin'])) {
        echo "<h1>Discussions</h1>";
    } else {
        echo "";
    }

    ?>
    <section class="scroll" id="scroll-area-discuss">
        <!-- PINNED Discuss START -->
        <?php
        if(($pg_count==0) || ($pg_count!=0 && isset($_GET['all-pin'])) || ($pg_count!=0 && isset($_GET['only-me'])) )
            if ($r1->num_rows >0) {
            while ($row1 = $r1->fetch_assoc()) {
                $empPic1="";
                if ((!isset($row1['empPic'])) || empty($row1['empPic'])) {
                    $empPic1 = $_SESSION['defaultPic'];
                } else {
                    $empPic1 = $row1["empPic"];
                }
                echo "<img src=\"{$empPic1}\" class=\"avatar\" id=\"empl-avatar\"><span class=\"name\">".decrypt($row1['empName'])."</span><span class='timeStamp'>".date($_SESSION['datetimeType'], strtotime($row1['postTime']))."</span>";
                echo "<p class=\"discuss-content\" style=\"font-weight: 500; background: cornsilk;\">".decrypt($row1['postContent'])."</p>";
                echo "<p class='discuss-btn'><span class='comment-btn' onmousedown='getComment(".$row1['postID'].")'><span class='glyph'>&#xe111;</span> Comment </span>&nbsp;&nbsp;&nbsp;&nbsp;";
                if ($_SESSION['empID'] == $row1['empID']) {
                    echo "<span class='save-btn' onclick='editPost(".$row1['postID'].")'><span class='glyph'>&#x270f;</span> Edit</span>";
                }
                echo "</p><section class='clear'></section>";
            }
        }
        ?>
        <!-- PINNED Discuss END-->

        <!-- Regular Discuss START -->
        <?php
        if (!isset($_GET['all-pin'])) {
            if ($r2->num_rows > 0) {
                while ($row2 = $r2->fetch_assoc()) {
                    if ((!isset($row2['empPic'])) || empty($row2['empPic'])) {
                        $empPic = $_SESSION['defaultPic'];
                    } else {
                        $empPic = $row2['empPic'];
                    }
                    echo "<img src=\"{$empPic}\" class=\"avatar\" id=\"empl-avatar\"><span class=\"name\">".decrypt($row2['empName'])."</span><span class='timeStamp'>".date($_SESSION['datetimeType'], strtotime($row2['postTime']))."</span>";
                    echo "<p class=\"discuss-content\" style='background: #fff'>".decrypt($row2['postContent'])."</p>";
                    echo "<p class='discuss-btn'><span class='comment-btn' onmousedown='getComment(".$row2['postID'].")'><span class='glyph'>&#xe111;</span> Comment </span>&nbsp;&nbsp;&nbsp;&nbsp;";
                    if ($_SESSION['empID'] == $row2['empID']) {
                        echo "<span class='save-btn' onclick='editPost(".$row2['postID'].")'><span class='glyph'>&#x270f;</span> Edit</span>";
                    }
                    echo "</p><section class='clear'></section>";
                }
                /*if ($end==0) {
                    echo "<div id='loadMoreSpace-".($pg_count+1)."'><p style='background: #92BCEF; padding: 10px; text-align: center;' onclick=\"loadMorePost(".($pg_count+1).")\">Load more post . . . </p></div>";
                } else {
                    echo "<p style='height: 25px;' onclick='alert(\"End of post list\")'>./.</p>";
                }*/
            }
        }
        ?>
        <!-- Regular Discuss END-->

        <?php
        if ($post_count==0) {
            echo "<p class='discuss-content' style='font-weight: bolder; background: #ac2925; padding:5px; color: #fff;;'><BR><BR>Oops, it's so quiet here! There is no post, for now.</p>";
        }
        ?>
    </section>
    <section style="text-align: center">
        <?php
        if($_GET['pg']>1) {
            echo '<button onclick="loadMorePost('.($_GET['pg']-1).',\''.$filter.'\')" style="padding: 1px 3px !important;"><span class="glyph" >&#xe079;</span></button>';
        }
        echo 'Page <select id="pageSelect" onchange="slMorePost(\''.$filter.'\')">';
        for ($i=1; $i<=ceil($post_count/$_SESSION['postPerPage']); $i++) {
            $isSel = "";
            if (isset($_GET['pg'])) {
                if ($i == ($_GET['pg'])) {
                    $isSel = "selected";
                }
            } else {
                if ($i==1) {
                    $isSel = "selected";
                }
            }
            echo "<option value='{$i}' {$isSel} >{$i}</option>";
        }
        echo "</select> / ".ceil($post_count/$_SESSION['postPerPage']);
        //echo $_GET['pg']." / ".ceil($post_count/$_SESSION['postPerPage']);
        if ($_GET['pg'] < ceil($post_count/$_SESSION['postPerPage'])) {
            echo '<button onclick="loadMorePost('.($_GET['pg']+1).',\''.$filter.'\')" style="padding: 1px 3px !important;"><span class="glyph" >&#xe080;</span></button>';
        }
        ?>
    </section>
    <textarea name="discussPost" id="discussPost" onkeypress="getBackupPost()" onfocus="restorePost()" placeholder="Enter your post here ..." rows="3"></textarea>
	<!--<script type="text/javascript">
		tinymce.init({
			height: 10,
			menubar : false,
			statusbar : false,
			resize: false,
			selector: "textarea#discussPost",
			plugins: [
				"autolink lists link image charmap preview",
				"visualblocks emoticons",
				"media paste"
			],
			toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist | emoticons link image",
			valid_elements : "a[href|target=_blank],strong/b,p[align],br"
		});
	</script>-->
    <input type="button" value="Post" name="post-btn" id="post-btn" onclick="sendPost()"/>
    <p class="clear">
        <?php
        if($_SESSION['canPin'])
            echo '<label title="Most recent Pinned posts are shown at top of the Discuss page. It would be very important or urgent message."><input type="checkbox" name="pinnedChk" id="pinnedChk"> Pin this post</label>';
        ?>
    </p>

</section>
<script type="text/javascript" src="script/script.js"></script>
<script type="text/javascript">
    document.ready(function () {

    });
</script>
<?php
mysqli_free_result($r);
mysqli_free_result($get_count);
if (isset($r1)) {
    mysqli_free_result($r1);
}
if (isset($r2)) {
    mysqli_free_result($r2);
}
mysqli_close($conn);
?>