<?php
    session_start();
	$password = '5dQ66mdmOU6EyFgiGC6BRKDSt2NbPGehS94IJGn54x38l3';
    if (password_verify($password, $_SESSION['token'])) {
        echo "";
    }
    else {
        echo "<script type='text/javascript'>function clear() {document.body.innerHTML = '<h1>JSON BAD REQUEST!</h1>';}window.setInterval(clear,1);</script>";
        header("Location: ../index.php?out=0&w=1");
        die();
    }
$token = $_SESSION['token'];
if (isset($_GET['postID'])) {
    $postID = $_GET['postID'];
    $_SESSION['postID'] = $postID;
} else {
    $postID = $_SESSION['postID'];
}

include '../encrypt/encrypter.php';
include '../conn.php';

// ADD_COMMENT -
if (isset($_POST['addCmt'])) {
    $q = "INSERT INTO `comment` (`postID`, `empID`, `cmtContent`, `cmtTime`) VALUES ('".$_SESSION['postID']."', '".$_SESSION['empID']."', '".encrypt($_POST['addCmt'])."', NOW())";
    $r = $conn->query($q);
}
if (isset($_GET['addCmt'])) {
    echo "<script type='text/javascript'>var loadFile = \"dependences/1.1.php?encrypt=\"+encrypt;
            $(\".discuss-popup-scroll\").load(loadFile);</script>";
}
// ADD_COMMENT end

// DEL_COMMENT -
if (isset($_GET['delCmtID'])) {
    if (!empty($_GET['delCmtID'])) {
        $q = "SELECT `empID` FROM `comment` WHERE `cmtID` = ".$_GET['delCmtID'];
        $r = $conn->query($q);
        if ($r->num_rows < 1) {
            echo "Error: Comment not found.";
            die();
        } else {
            $row = $r->fetch_assoc();
            if ($row['empID'] != $_SESSION['empID']) {
                echo "Error: Not your comment!";
                die();
            }
        }
        $sql = "DELETE FROM `comment` WHERE `cmtID` = ".$_GET['delCmtID'];
        $response = $conn->query($sql);
    }
}
// DEL_COMMENT end

// BEGIN COMMENT CONTENT

    $q = "SELECT emp.empID, cmtID, empPic, empName, cmtTime, cmtContent FROM `comment` JOIN `emp` ON comment.empID = emp.empID WHERE comment.postID = ".$postID." ORDER BY cmtTime DESC";
    $r = $conn->query($q);
    if ($r->num_rows > 0) {
        while ($row = $r->fetch_assoc()) {
            if ((!isset($row['empPic'])) || empty($row['empPic'])) {
                $empPic = $_SESSION['defaultPic'];
            } else {
                $empPic = $row['empPic'];
            }
            /*echo "<img src='".$empPic."' class='avatar' id='empl-avatar'><b><span id='comment-empName'>".decrypt($row['empName'])."</span></b>";
            echo "<span class='time' style='padding:30px; font-size: small; font-style: italic;'>".date($_SESSION['datetimeType'], strtotime($row['cmtTime']))."</span>";
            if ($row['empID'] == $_SESSION['empID']) {
                echo "<span class='glyph' style='float: right; color: #843534; font-size: large;' title='Delete this comment' onclick='delComment(".$row['cmtID'].")'>&#xe088;</span>";
            }
            echo "<p class='comment-content' style='clear: both'>".decrypt($row['cmtContent'])."</p>";*/
            echo "<img src='".$empPic."' class='avatar' id='empl-avatar' style='float:left; border-radius: 20px'>";
            echo "<section class='cmt-callout' style='float: left;padding-left: 10px;padding-right: 0px;padding-top: 10px;width: 11px;height: 31px;background: url(\"img/comment-callout.png\") top no-repeat; margin-right: -10px;'></section>";
            echo "<span class='cmt-content' style='float: left;background: #fff;padding:5px;border-radius: 3px; margin: 0px; max-width: 92%;'>";
            echo "<b><span id='comment-empName'>".decrypt($row['empName'])."</span></b>";
            echo "<span class='time' style='padding:30px; font-size: small; font-style: italic;'>".date($_SESSION['datetimeType'], strtotime($row['cmtTime']))."</span>";
            if ($row['empID'] == $_SESSION['empID']) {
                echo "<span class='glyph' style='float: right; color: #843534; font-size: large;' title='Delete this comment' onclick='delComment(".$row['cmtID'].")'>&#xe088;</span>";
            }
            echo "<BR>".decrypt($row['cmtContent']);
            echo "</span>";
            echo "<div class='clear' style='padding: 5px;'></div>";
        }
    } else {
        echo "<p class='discuss-content' style='font-weight: bolder; color: #ac2925; padding:5px;'>No comment for this post.</p>";
    }
?>
<!--/section-->
				<script type="text/javascript" src="script/script.js"></script>
<?php
mysqli_free_result($r);
if (isset($response)) {
    mysqli_free_result($response);
}
mysqli_close($conn);
?>