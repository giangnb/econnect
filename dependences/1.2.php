<?php
    session_start();
	$password = '5dQ66mdmOU6EyFgiGC6BRKDSt2NbPGehS94IJGn54x38l3';
    if (password_verify($password, $_SESSION['token'])) {
        echo "";
    }
    else {
        echo "<script type='text/javascript'>function clear() {document.body.innerHTML = '<h1>JSON BAD REQUEST!</h1>';}window.setInterval(clear,1);</script>";
        header("Location: ../index.php?out=0&w=1");
        die();
    }
$token = $_SESSION['token'];
include '../encrypt/encrypter.php';
include '../conn.php';

if (!isset($_GET['ID']) && !isset($_SESSION['postID'])) {
    echo "<h1>Error!</h1>";
    die();
} else {
    if(isset($_GET['ID'])) {
        $_SESSION['postID'] = $_GET['ID'];
    }
}

if (isset($_POST['change'])) {
    if(!empty($_POST['change'])) {
        if (empty($_POST['pin']) || !isset($_POST['pin']) || $_POST['pin']==0) {
            $pin = 0;
        } else {
            $pin = 1;
        }
        $sql = "UPDATE `post` SET `postContent` = '".encrypt($_POST['change'])."', `isPinned` = ".$pin." WHERE `postID` = ".$_SESSION['postID'];
        $result = $conn->query($sql);
    } else {
        echo "Error";
        die();
    }
}

if (isset($_GET['del'])) {
    $sql = "SELECT `empID` FROM `post` WHERE `postID` = ".$_SESSION['postID'];
    $result = $conn->query($sql);
    $row = $result -> fetch_assoc();
    if ($row['empID'] == $_SESSION['empID']) {
        $sql = "DELETE FROM `comment` WHERE `postID` = ".$_SESSION['postID'];
        $result = $conn->query($sql);
        $sql = "DELETE FROM `post` WHERE `postID` = ".$_SESSION['postID'];
        $result = $conn->query($sql);
        echo "<h1>Post deleted.</h1>";
    } else {
        echo "<h1>You don't have permission to delete this post!<BR>Action terminated.</h1>";
    }
    unset($_SESSION['postID']);
    die();
} else {
    $sql = "SELECT `empID`, `postContent`, `isPinned` FROM `post` WHERE `postID` = " . $_SESSION['postID'];
    $result = $conn->query($sql);

    if(isset($_GET['change'])) {
        echo "<p style='background: #245269; color:#fff; padding: 10px; border-radius: 3px;'>Post updated.</p>";
    }

    if ($result->num_rows < 0) {
        echo "<b>Post not found!</b>";
    } else {
        while ($row = $result->fetch_assoc()) {
            if ($row['empID'] == $_SESSION['empID']) {
                //echo "<form name='editPost' action='#' method='post'>";
                echo "<textarea name='postContent' id='postContent' rows='8' style='width: 100%;'>" . decrypt($row['postContent']) . "</textarea><BR>";
                //echo "<input type='text' name='postContent' id='postContent' style='width: 100%;' value = '".decrypt($row['postContent'])."' ><BR>";
                if ($_SESSION['canPin'] == 1) {
                    echo "<label><input type='checkbox' name='isPinned-change' id='isPinned-change'";
                    if ($row['isPinned'] == 1) echo "checked";
                    echo " /> Pinned post</label>";
                }
                echo "<BR><input type='button' id='submitChangePost' name='submitChangePost' onclick='changePost()' value=' Change '/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='button' id='delPost' name='delPost' onclick='delPost()' value=' Delete post '/><div id='result'></div>";
                //echo "</form>";
            } else {
                echo "<b>You don't have permission to edit this post!</b>";
            }

        }
    }
}
?>
				<script type="text/javascript" src="script/script.js"></script>
<?php
mysqli_free_result($result);
mysqli_close($conn);
?>